create table if not exists gift_certificate (
	id bigint auto_increment primary key,
    description varchar(255),
    price double not null,
    duration int not null,
    create_date datetime,
    last_update_date datetime,
    name varchar(255) unique not null);
    
create table if not exists tag(
	id bigint auto_increment primary key,
    name varchar(255) unique not null
);

create table if not exists gift_certs_tags(
	gift_cert_id bigint,
    tag_id bigint,
    primary key(gift_cert_id, tag_id),
    foreign key(gift_cert_id) references gift_certificate(id) on delete cascade,
    foreign key(tag_id) references tag(id) on delete cascade,
    unique key(gift_cert_id, tag_id));
CREATE INDEX gift_cert_id_idx ON gift_certs_tags(gift_cert_id);
CREATE INDEX tag_id_idx ON gift_certs_tags(tag_id);

create table if not exists user(
    id bigint auto_increment primary key,
    full_name varchar(255) not null,
    email varchar(255) not null unique,
    password varchar(255) not null,
    created_date datetime,
    last_updated_date datetime
);

create table if not exists gift_certificate_order(
    id bigint primary key auto_increment,
    total_cost double not null,
    quantity int not null,
    status varchar(10) not null,
    ordered_date datetime not null,
    last_updated_date datetime,
    owner_id bigint,
    gift_certificate_id bigint,
    foreign key (owner_id) references user(id) on delete cascade,
    foreign key (gift_certificate_id) references gift_certificate(id) on delete cascade);
CREATE INDEX total_cost_idx ON gift_certificate_order(total_cost);
CREATE UNIQUE INDEX UNIQUE_GC_ID_AND_OWNER_ID ON gift_certificate_order (gift_certificate_id, owner_id);
--alter table gift_certificate_order add unique (`owner_id`,`gift_certificate_id`);