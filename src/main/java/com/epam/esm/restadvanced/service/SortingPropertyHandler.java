package com.epam.esm.restadvanced.service;
/**
 * This interface imposes converting on giving array of string to 
 * gift certificate field based parameters string description.
 * @author yusuf
 *
 */
public interface SortingPropertyHandler {
	/**
	 * Converts array of specific parameters to 
	 * gift certificate field based query description 
	 * 
	 * @param sortBy array of sorting property
	 * @return String based on gift certificate fields
	 */
	String convert(String[] sortBy);
}
