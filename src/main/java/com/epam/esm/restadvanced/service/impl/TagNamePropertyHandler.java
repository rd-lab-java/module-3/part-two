package com.epam.esm.restadvanced.service.impl;

import com.epam.esm.restadvanced.service.NamePropertyHandler;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class TagNamePropertyHandler implements NamePropertyHandler {

    @Override
    public String retrieveCompatibleNames(String[] names) {
        return Arrays.stream(names).map(s -> "'" + s + "'")
                .collect(Collectors.joining(","));
    }
}
