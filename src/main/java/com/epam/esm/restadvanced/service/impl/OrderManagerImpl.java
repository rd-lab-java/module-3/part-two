package com.epam.esm.restadvanced.service.impl;

import com.epam.esm.restadvanced.common.api.model.Page;
import com.epam.esm.restadvanced.common.api.model.PageRequest;
import com.epam.esm.restadvanced.common.api.model.Pageable;
import com.epam.esm.restadvanced.repository.OrderRepository;
import com.epam.esm.restadvanced.repository.entity.Order;
import com.epam.esm.restadvanced.service.CostCalculator;
import com.epam.esm.restadvanced.service.OrderManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class OrderManagerImpl implements OrderManager {
    private final OrderRepository orderRepository;
    private final CostCalculator costCalculator;

    @Autowired
    public OrderManagerImpl(OrderRepository orderRepository, CostCalculator costCalculator) {
        this.orderRepository = orderRepository;
        this.costCalculator = costCalculator;
    }

    @Transactional
    @Override
    public Order saveOrder(Order order) {
        order.setTotalCost(costCalculator.totalCost(order.getQuantity(), order.getUnitPrice()));
        return orderRepository.saveOrder(order);
    }

    @Transactional(readOnly = true)
    @Override
    public Page<Order> findOrdersByGiftCertificateId(long giftCertificateId, Pageable pageRequest) {
        return orderRepository.findOrdersByGiftCertificateId(giftCertificateId, pageRequest);
    }

    @Transactional(readOnly = true)
    @Override
    public Page<Order> findOrdersByOwnerId(long userId, Pageable pageRequest) {
        return orderRepository.findOrdersByOwnerId(userId, pageRequest);
    }

    @Transactional(readOnly = true)
    @Override
    public Order findOrderById(long orderId) {
        return orderRepository.findOrderById(orderId);
    }

    @Override
    @Transactional
    public Order deleteById(long orderId) {
        return orderRepository.deleteById(orderId);
    }

}
