package com.epam.esm.restadvanced.service.impl;

import com.epam.esm.restadvanced.service.CostCalculator;
import org.springframework.stereotype.Service;

@Service
public class CostCalculatorImpl implements CostCalculator {
    @Override
    public double totalCost(int quantity, double price) {
        return quantity * price;
    }
}
