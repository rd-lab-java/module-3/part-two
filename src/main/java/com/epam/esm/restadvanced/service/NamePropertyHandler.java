package com.epam.esm.restadvanced.service;

public interface NamePropertyHandler {
    String retrieveCompatibleNames(String[] names);
}
