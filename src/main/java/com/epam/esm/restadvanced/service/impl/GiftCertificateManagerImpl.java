package com.epam.esm.restadvanced.service.impl;

import com.epam.esm.restadvanced.common.api.model.Page;
import com.epam.esm.restadvanced.common.api.model.PageRequest;
import com.epam.esm.restadvanced.common.api.model.Pageable;
import com.epam.esm.restadvanced.repository.GiftCertificateRepository;
import com.epam.esm.restadvanced.repository.entity.GiftCertificate;
import com.epam.esm.restadvanced.service.GiftCertificateManager;
import com.epam.esm.restadvanced.service.NamePropertyHandler;
import com.epam.esm.restadvanced.service.SortingPropertyHandler;
import com.epam.esm.restadvanced.service.exception.GiftCertificateNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Gift Certificate Interface Implementations
 * that contains implementation of business logics. 
 * @author yusuf
 *
 */
@Service
@PropertySource(name="stringValues", value = "classpath:string-values.properties")
public class GiftCertificateManagerImpl implements GiftCertificateManager {
	private final SortingPropertyHandler giftCertificateSort;
	private final GiftCertificateRepository giftCertificateRepository;
	private final NamePropertyHandler tagNamePropertyHandler;
	private final Environment environment;
	
	@Autowired
	public GiftCertificateManagerImpl(GiftCertificateSort giftCertificateSort,
									  GiftCertificateRepository giftCertificateRepository,
									  TagNamePropertyHandler tagNamePropertyHandler, Environment environment) {
		this.giftCertificateSort = giftCertificateSort;
		this.giftCertificateRepository = giftCertificateRepository;
		this.tagNamePropertyHandler = tagNamePropertyHandler;
		this.environment = environment;
	}
	
	@Override
	@Transactional
	public GiftCertificate saveGiftCertificate(GiftCertificate giftCertificate) {
		return giftCertificateRepository.saveGiftCertificate(giftCertificate);
	}
	
	@Override
	@Transactional(readOnly = true)
	public Page<GiftCertificate> findAll(Pageable pageRequest) {
		return giftCertificateRepository.findAll(pageRequest);
	}

	@Override
	@Transactional(readOnly = true)
	public GiftCertificate findGiftCertificateById(long id) {
		return giftCertificateRepository.findGiftCertificateById(id);
	}

	@Override
	@Transactional
	public GiftCertificate updateGiftCertificateById(long id, GiftCertificate giftCertificate) {
		return giftCertificateRepository.updateGiftCertificateById(id, giftCertificate);
	}

	@Override
	@Transactional
	public GiftCertificate deleteGiftCertificateById(long id) {
		return giftCertificateRepository.deleteGiftCertificateById(id);
	}

	@Override
	@Transactional(readOnly = true)
	public Page<GiftCertificate> findGiftCertificatesByTagId(long tagId, Pageable pageRequest) {
		return giftCertificateRepository.findGiftCertificatesByTagId(tagId, pageRequest);
	}

	@Transactional(readOnly = true)
	@Override
	public Page<GiftCertificate> findByTagNames(Optional<String[]> tags,
												Optional<String[]> sortBy,
												Pageable pageable) {
		if(tags.isPresent()) {
			if(sortBy.isPresent()) {
				return giftCertificateRepository.findSortedGiftCertificatesAndTagsByTagNames(
						tagNamePropertyHandler.retrieveCompatibleNames(tags.get()), tags.get().length,
						giftCertificateSort.convert(sortBy.get()), pageable);
			}
			return giftCertificateRepository.findGiftCertificatesAndTagsByTagNames(
						tagNamePropertyHandler.retrieveCompatibleNames(tags.get()), tags.get().length, pageable);
		}
		throw new GiftCertificateNotFoundException(
				environment.getProperty("giftCertificate.message.searchNotFoundByTag"));
	}

	@Transactional(readOnly = true)
	@Override
	public Page<GiftCertificate> findByNameContaining(Optional<String> name, Optional<String[]> sortBy,
													  Pageable pageable) {
		if(name.isPresent()) {
			if(sortBy.isPresent()) {
				return giftCertificateRepository.findSortedGiftCertificatesAndTagsByPartOfName(
						name.get(), giftCertificateSort.convert(sortBy.get()), pageable);
			}
			return giftCertificateRepository.findGiftCertificatesAndTagsByPartOfName(name.get(), pageable);
		}
		throw new GiftCertificateNotFoundException(
				environment.getProperty("giftCertificate.message.searchNotFoundByName"));
	}

	@Override
	@Transactional(readOnly = true)
	public Page<GiftCertificate> findByDescriptionContaining(Optional<String> description, Optional<String[]> sortBy,
															 Pageable pageable) {
		if(description.isPresent()) {
			if(sortBy.isPresent()) {
				return giftCertificateRepository.findSortedGiftCertificatesAndTagsByPartOfDesc(
						description.get(), giftCertificateSort.convert(sortBy.get()), pageable);
			}
			return giftCertificateRepository.findGiftCertificatesAndTagsByPartOfDesc(description.get(), pageable);
		}
		throw new GiftCertificateNotFoundException(
				environment.getProperty("giftCertificate.message.searchNotFoundByDescription"));
	}
}
