package com.epam.esm.restadvanced.service.impl;

import com.epam.esm.restadvanced.common.api.model.Page;
import com.epam.esm.restadvanced.common.api.model.PageRequest;
import com.epam.esm.restadvanced.common.api.model.Pageable;
import com.epam.esm.restadvanced.repository.UserRepository;
import com.epam.esm.restadvanced.repository.entity.User;
import com.epam.esm.restadvanced.service.UserManager;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class UserManagerImpl implements UserManager {

    private final UserRepository userRepository;

    public UserManagerImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Transactional
    @Override
    public User saveUser(User user) {
        return userRepository.saveUser(user);
    }

    @Transactional(readOnly = true)
    @Override
    public Page<User> findAll(Pageable pageRequest) {
        return userRepository.findAll(pageRequest);
    }

    @Transactional(readOnly = true)
    @Override
    public User findUserById(long id) {
        return userRepository.findUserById(id);
    }

    @Transactional
    @Override
    public User deleteUserById(long userId) {
        return userRepository.deleteUserById(userId);
    }
}
