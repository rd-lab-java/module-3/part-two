package com.epam.esm.restadvanced.service.impl;

import com.epam.esm.restadvanced.common.api.model.Page;
import com.epam.esm.restadvanced.common.api.model.PageRequest;
import com.epam.esm.restadvanced.common.api.model.Pageable;
import com.epam.esm.restadvanced.repository.TagRepository;
import com.epam.esm.restadvanced.repository.entity.Tag;
import com.epam.esm.restadvanced.service.TagManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


/**
 * Tag Service interface implementation that contains 
 * implementation of CRUD opertations business logics
 * 
 * @author yusuf
 *
 */
@Service
public class TagManagerImpl implements TagManager {

	@Autowired
	private final TagRepository tagRepository;

	public TagManagerImpl(TagRepository tagRepository) {
		this.tagRepository = tagRepository;
	}

	@Override
	@Transactional
	public Tag saveTag(Tag tag) {
		return tagRepository.saveTag(tag);
	}

	@Override
	@Transactional(readOnly = true)
	public Page<Tag> findTagsByGiftCertificateId(long giftCertId, Pageable pageRequest) {
		return tagRepository.findTagsByGiftCertificateId(giftCertId, pageRequest);
	}

	@Override
	@Transactional(readOnly = true)
	public Page<Tag> findAll(Pageable pageRequest) {
		return tagRepository.findAll(pageRequest);
	}

	@Override
	@Transactional
	public Tag deleteTagById(long id) {
		return tagRepository.deleteTagById(id);
	}

	@Override
	@Transactional(readOnly = true)
	public Tag findTagById(long id) {
		return tagRepository.findTagById(id);
	}

	@Override
	@Transactional(readOnly = true)
	public Page<Tag> findMostUsedTags(Pageable pageRequest) {
		return tagRepository.findMostWidelyUsedTags(pageRequest);
	}

	@Override
	@Transactional(readOnly = true)
	public List<Tag> findMostWidelyUsedTagsByTopLimit(int top) {
		return tagRepository.findTopMostWidelyUsedTags(top);
	}

	@Override
	@Transactional(readOnly = true)
	public List<Tag> findTopMostUsedTags(int top) {
		return tagRepository.findTopMostWidelyUsedTags(top);
	}
}
