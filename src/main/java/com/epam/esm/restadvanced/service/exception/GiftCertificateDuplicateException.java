package com.epam.esm.restadvanced.service.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value=HttpStatus.INTERNAL_SERVER_ERROR, reason="Gift Certificate Duplicate Exception")
public class GiftCertificateDuplicateException extends RuntimeException {

	private static final long serialVersionUID = 6315049771712745727L;

	public GiftCertificateDuplicateException() {
		super();
	}

	public GiftCertificateDuplicateException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public GiftCertificateDuplicateException(String message, Throwable cause) {
		super(message, cause);
	}

	public GiftCertificateDuplicateException(String message) {
		super(message);
	}

	public GiftCertificateDuplicateException(Throwable cause) {
		super(cause);
	}
}
