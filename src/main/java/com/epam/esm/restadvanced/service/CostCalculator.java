package com.epam.esm.restadvanced.service;

public interface CostCalculator {
    double totalCost(int quantity, double price);
}
