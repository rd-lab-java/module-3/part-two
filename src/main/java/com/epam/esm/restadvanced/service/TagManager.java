package com.epam.esm.restadvanced.service;

import com.epam.esm.restadvanced.common.api.model.Page;
import com.epam.esm.restadvanced.common.api.model.PageRequest;
import com.epam.esm.restadvanced.common.api.model.Pageable;
import com.epam.esm.restadvanced.repository.entity.Tag;

import java.util.List;

/**
 * This interface imposes CRUD operations business logic on tag object
 * @author yusuf
 *
 */
public interface TagManager {
	Tag saveTag(Tag tag);
	Page<Tag> findTagsByGiftCertificateId(long giftCertId, Pageable pageRequest);
	Page<Tag> findAll(Pageable pageRequest);
	Tag deleteTagById(long id);
	Tag findTagById(long tagId);
    Page<Tag> findMostUsedTags(Pageable pageRequest);
	List<Tag> findMostWidelyUsedTagsByTopLimit(int top);

	List<Tag> findTopMostUsedTags(int top);
}
