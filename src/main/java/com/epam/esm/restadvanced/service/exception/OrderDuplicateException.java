package com.epam.esm.restadvanced.service.exception;

import org.springframework.dao.DuplicateKeyException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR, reason = "Order Duplicate Exception")
public class OrderDuplicateException extends RuntimeException {
    public OrderDuplicateException() {
    }

    public OrderDuplicateException(String message) {
        super(message);
    }

    public OrderDuplicateException(String message, Throwable cause) {
        super(message, cause);
    }

    public OrderDuplicateException(Throwable cause) {
        super(cause);
    }

    public OrderDuplicateException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
