package com.epam.esm.restadvanced.service.impl;

import com.epam.esm.restadvanced.service.SortingPropertyHandler;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

/**
 * The implementation of {@link SortingPropertyHandler} interface 
 * @author yusuf
 *
 */
@Service
public class GiftCertificateSort implements SortingPropertyHandler {

	@Override
	public String convert(String[] sortBy) {
		StringBuilder sort = new StringBuilder();
		for(String token: sortBy) {
			if(token.equals("date")) {
				if(sort.length() != 0) {
					sort.append(',');
				}
				sort.append("gift_certificate.create_date");
			}
			
			if(token.startsWith("name")){
				if(sort.length() != 0) {
					sort.append(',');
				}
				sort.append("gift_certificate.name ").append(token.split(";")[1]);
			}
		}
		return sort.toString();
	}
}
