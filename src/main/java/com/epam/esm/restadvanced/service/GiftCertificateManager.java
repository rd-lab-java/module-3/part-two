package com.epam.esm.restadvanced.service;

import com.epam.esm.restadvanced.common.api.model.Page;
import com.epam.esm.restadvanced.common.api.model.PageRequest;
import com.epam.esm.restadvanced.common.api.model.Pageable;
import com.epam.esm.restadvanced.repository.entity.GiftCertificate;
import org.springframework.stereotype.Component;

import java.util.Optional;


/**
 * This interface imposes business logic 
 * on gift certificate class. 
 * @author yusuf
 */
@Component
public interface GiftCertificateManager {
	GiftCertificate saveGiftCertificate(GiftCertificate giftCertificate);
	Page<GiftCertificate> findAll(Pageable pageRequest);
	GiftCertificate findGiftCertificateById(long id);
	GiftCertificate updateGiftCertificateById(long id, GiftCertificate giftCertificate);
	GiftCertificate deleteGiftCertificateById(long id);

    Page<GiftCertificate> findGiftCertificatesByTagId(long tagId, Pageable pageRequest);
	Page<GiftCertificate> findByTagNames(Optional<String[]> tags, Optional<String[]> sortBy, Pageable pageable);
	Page<GiftCertificate> findByNameContaining(Optional<String> name, Optional<String[]> sortBy, Pageable pageable);
	Page<GiftCertificate> findByDescriptionContaining(Optional<String> description, Optional<String[]> sortBy,
													  Pageable pageable);
}
