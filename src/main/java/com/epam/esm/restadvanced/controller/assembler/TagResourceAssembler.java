package com.epam.esm.restadvanced.controller.assembler;

import com.epam.esm.restadvanced.common.api.model.Pageable;
import com.epam.esm.restadvanced.controller.v1.TagController;
import com.epam.esm.restadvanced.controller.model.TagResource;
import com.epam.esm.restadvanced.repository.entity.Tag;
import org.springframework.stereotype.Component;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@Component
public class TagResourceAssembler {
    public TagResource toResource(Tag tag) {
        TagResource resource = new TagResource(tag.getId(), tag.getName());
        resource.add(linkTo(methodOn(TagController.class).findTag(tag.getId())).withSelfRel());
        resource.add(linkTo(methodOn(TagController.class).findGiftCertificatesByTagId(tag.getId(), Pageable.unpaged())).
                withRel("giftCertificates"));
        return resource;
    }
}
