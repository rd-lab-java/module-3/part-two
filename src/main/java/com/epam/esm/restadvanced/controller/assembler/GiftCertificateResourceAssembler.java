package com.epam.esm.restadvanced.controller.assembler;

import com.epam.esm.restadvanced.common.api.model.Pageable;
import com.epam.esm.restadvanced.controller.v1.GiftCertificateController;
import com.epam.esm.restadvanced.controller.model.GiftCertificateResource;
import com.epam.esm.restadvanced.controller.model.TagResource;
import com.epam.esm.restadvanced.repository.entity.GiftCertificate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@Component
public class GiftCertificateResourceAssembler {
    @Autowired
    private TagResourceAssembler tagResourceAssembler;

    public GiftCertificateResource toResource(GiftCertificate giftCertificate) {
        List<TagResource> tagResources = giftCertificate.getTags()
                .stream()
                .map(tag -> tagResourceAssembler.toResource(tag))
                .collect(Collectors.toList());

        GiftCertificateResource resource = new GiftCertificateResource(giftCertificate.getId(), giftCertificate.getName(),
                giftCertificate.getDescription(), giftCertificate.getPrice(), giftCertificate.getDuration(),
                giftCertificate.getCreatedDate(), giftCertificate.getLastUpdatedDate(), tagResources);

        resource.add(linkTo(methodOn(GiftCertificateController.class).findGiftCertificateById(giftCertificate.getId()))
                .withSelfRel());
        resource.add(linkTo(methodOn(GiftCertificateController.class).findGiftCertificateTags(giftCertificate.getId(),
                Pageable.unpaged())).withRel("tags"));
        resource.add(linkTo(methodOn(GiftCertificateController.class).findGiftCertificateOrders(giftCertificate.getId(),
                Pageable.unpaged())).withRel("orders"));
        
        return resource;
    }
}
