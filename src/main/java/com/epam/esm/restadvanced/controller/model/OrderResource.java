package com.epam.esm.restadvanced.controller.model;

import com.epam.esm.restadvanced.repository.entity.GiftCertificate;
import com.epam.esm.restadvanced.repository.entity.User;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.hateoas.RepresentationModel;
import org.springframework.stereotype.Component;

import java.util.Date;

public class OrderResource extends RepresentationModel<OrderResource> {
    public long id;
    public double totalCost;
    public int quantity;
    public String status;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss")
    public Date orderedDate;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss")
    public Date lastUpdatedDate;
    public UserResource owner;
    public GiftCertificateResource giftCertificate;

    public OrderResource(long id, double totalCost, int quantity, String status, Date orderedDate, Date lastUpdatedDate,
                         UserResource owner, GiftCertificateResource giftCertificate) {
        this.id = id;
        this.totalCost = totalCost;
        this.quantity = quantity;
        this.status = status;
        this.orderedDate = orderedDate;
        this.lastUpdatedDate = lastUpdatedDate;
        this.owner = owner;
        this.giftCertificate = giftCertificate;
    }
}
