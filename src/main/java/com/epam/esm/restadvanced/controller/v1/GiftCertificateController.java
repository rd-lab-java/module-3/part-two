package com.epam.esm.restadvanced.controller.v1;

import com.epam.esm.restadvanced.common.api.model.Page;
import com.epam.esm.restadvanced.common.api.model.PageRequest;
import com.epam.esm.restadvanced.common.api.model.Pageable;
import com.epam.esm.restadvanced.controller.assembler.*;
import com.epam.esm.restadvanced.controller.common.PatchHelper;
import com.epam.esm.restadvanced.controller.mapper.GiftCertificateMapper;
import com.epam.esm.restadvanced.controller.model.GiftCertificateListResource;
import com.epam.esm.restadvanced.controller.model.GiftCertificateResourceInput;
import com.epam.esm.restadvanced.controller.model.TagListResource;
import com.epam.esm.restadvanced.repository.entity.Order;
import com.epam.esm.restadvanced.repository.entity.Tag;
import com.epam.esm.restadvanced.repository.entity.User;
import com.epam.esm.restadvanced.service.OrderManager;
import com.epam.esm.restadvanced.service.UserManager;
import com.epam.esm.restadvanced.service.exception.GiftCertificateNotFoundException;
import com.epam.esm.restadvanced.service.exception.GiftCertificateNotModifiedException;
import com.epam.esm.restadvanced.service.exception.GiftCertificateDuplicateException;
import com.epam.esm.restadvanced.repository.entity.GiftCertificate;
import com.epam.esm.restadvanced.service.GiftCertificateManager;
import com.epam.esm.restadvanced.service.TagManager;
import com.epam.esm.restadvanced.util.ResponseUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;

import javax.json.JsonMergePatch;
import java.util.Optional;

/**
 * Rest controller api for managing 
 * gift certificate based requests and responses.
 * @author yusuf
 *
 */
@RestController
@RequestMapping("/v1/gift-certificates")
public class GiftCertificateController {
	/** Gift Certificate Service class variable */
	@Autowired
	private GiftCertificateManager giftCertificateManager;
	/** Tag Service class variable */
	@Autowired
	private TagManager tagManager;
	@Autowired
	private OrderManager orderManager;
	@Autowired
	private UserManager userManager;
	@Autowired
	private GiftCertificateMapper giftCertificateMapper;
	@Autowired
	private PatchHelper patchHelper;
	@Autowired
	private GiftCertificateListResourceAssembler giftCertificateListResourceAssembler;
	@Autowired
	private GiftCertificateResourceAssembler giftCertificateResourceAssembler;
	@Autowired
	private OrderListResourceAssembler orderListResourceAssembler;
	@Autowired
	private OrderResourceAssembler orderResourceAssembler;
	@Autowired
	private TagListResourceAssembler tagListResourceAssembler;

	/**
	 * Used to save new gift certificate, can contain zero or more tags
	 * 
	 * @param giftCertificate
	 * @return The common response {@link ResponseUtils} containing status code,
	 *  message and actual data ({@link GiftCertificate})
	 * @throws Exception {@link GiftCertificateDuplicateException}
	 *  when trying to insert duplicate gift certificate
	 */
	@PostMapping(consumes = "application/json", produces = "application/json")
	@ResponseStatus(value = HttpStatus.CREATED)
	public ResponseUtils saveGiftCertificate(@RequestBody GiftCertificate giftCertificate) {
		GiftCertificate savedGiftCertificate = giftCertificateManager.saveGiftCertificate(giftCertificate);

		return ResponseUtils.response(201, "Gift certificate created successfully",
				giftCertificateResourceAssembler.toResource(savedGiftCertificate));
	}
	
	/**
	 * Used to find all gift certificates
	 * 
	 * @return {@link ResponseUtils} containing status code,
	 *  message and actual data ({@link GiftCertificate}) without tags
	 * @throws Exception {@link GiftCertificateNotFoundException}
	 *  when no found any gift certificate 
	 */
	@GetMapping(produces = "application/json")
	public ResponseUtils findAll(Pageable pageRequest) {
		Page<GiftCertificate> giftCertificatePage = giftCertificateManager.findAll(pageRequest);
		GiftCertificateListResource giftCertificateListResource = giftCertificateListResourceAssembler.
				build(giftCertificatePage);
		return ResponseUtils.success(giftCertificateListResource);
	}
	
	/**
	 * Used to find gift certificate by gift certificate id
	 * 
	 * @param id
	 * @return The common response {@link ResponseUtils} containing status code,
	 *  message and actual data ({@link GiftCertificate}) without tags
	 * @throws Exception {@link GiftCertificateNotFoundException}
	 *  when no found any gift certificate based on id
	 */
	@GetMapping(path = "/{id:[0-9]+}", produces = "application/json")
	public ResponseUtils findGiftCertificateById(@PathVariable long id) {
		GiftCertificate giftCertificate = giftCertificateManager.findGiftCertificateById(id);
		return ResponseUtils.success(giftCertificateResourceAssembler.toResource(giftCertificate));
	}
	
	/**
	 * Used to update the only gift certificate's different fields,
	 * or tags that is new to the gift certificate
	 * 
	 * @param id of {@link GiftCertificate} that is to be updated
	 * @param {@link GiftCertificate} contains new values 
	 * @return The common response {@link ResponseUtils} containing status code,
	 *  message and actual data ({@link GiftCertificate})
	 * @throws Exception {@link GiftCertificateNotFoundException}
	 * when no found gift certificate based id 
	 * or not modified exception {@link GiftCertificateNotModifiedException}
	 * when try to update unmodified gift certificate
	 */
	@PutMapping(path = "/{id:[0-9]+}", produces = "application/json", consumes = "application/json")
	public ResponseUtils updateGetCertificate(@PathVariable long id, @RequestBody GiftCertificate giftCertificate) {
		GiftCertificate updatedGiftCertificate = giftCertificateManager.updateGiftCertificateById(id, giftCertificate);
		return ResponseUtils.success(giftCertificateResourceAssembler.toResource(updatedGiftCertificate));
	}
	
	/**
	 * Used to delete gift certificate 
	 * 
	 * @param {@link GiftCertificate} id 
	 * @return The common response {@link ResponseUtils} containing status code,
	 *  message and actual data ({@link GiftCertificate})
	 * @throws Exception {@link GiftCertificateNotFoundException}
	 * when no found any gift certificate based on id 
	 */
	@DeleteMapping(path="/{id:[0-9]+}", produces = "application/json")
	public ResponseUtils deleteGiftCertificate(@PathVariable long id) {
		GiftCertificate deletedGiftCertificate = giftCertificateManager.deleteGiftCertificateById(id);
		return ResponseUtils.response(200, "Gift Certificate deleted successfully",
				giftCertificateResourceAssembler.toResource(deletedGiftCertificate));
	}
	
	/**
	 * Used to find gift certificate tags
	 * 
	 * @param {@link GiftCertificate} id 
	 * @return The common response {@link ResponseUtils} containing status code,
	 *  message and actual data ({@link GiftCertificate})
	 */
	@GetMapping("/{giftCertId:[0-9]+}/tags")
	public ResponseUtils findGiftCertificateTags(@PathVariable long giftCertId,
												 Pageable pageRequest) {
		System.out.println(pageRequest.getPageNumber() + " , " + pageRequest.getPageSize());
		Page<Tag> tagPage = tagManager.findTagsByGiftCertificateId(giftCertId, pageRequest);
		TagListResource tagListResource = tagListResourceAssembler.build(tagPage);
		return ResponseUtils.success(tagListResource);
	}

	/**
	 * Used to update single field of gift certificate
	 * @param giftCertId
	 * @param mergePatch a single value to be updated
	 * @return The common response {@link ResponseUtils} containing status code,
	 * 	 *  message and actual data ({@link GiftCertificate})
	 */
	@PatchMapping(value = "/{giftCertId:[0-9]+}", consumes = "application/merge-patch+json")
	public ResponseUtils updateGiftCertificateField(@PathVariable long giftCertId,
													@RequestBody JsonMergePatch mergePatch) {
		GiftCertificate giftCertificate = giftCertificateManager.findGiftCertificateById(giftCertId);
		GiftCertificateResourceInput giftCertificateResource = giftCertificateMapper.
				asGiftCertificateResourceInput(giftCertificate);
		GiftCertificateResourceInput giftCertificateResourcePatched = patchHelper.
				mergePatch(mergePatch, giftCertificateResource, GiftCertificateResourceInput.class);
		giftCertificateMapper.update(giftCertificateResourcePatched, giftCertificate);
		GiftCertificate updatedGiftCertificate = giftCertificateManager.
				updateGiftCertificateById(giftCertId, giftCertificate);
		return ResponseUtils.success(giftCertificateResourceAssembler.toResource(updatedGiftCertificate));
	}

	@PostMapping("/{giftCertificateId:[0-9]+}/orders")
	@ResponseStatus(value = HttpStatus.CREATED)
	public ResponseUtils orderGiftCertificate(@PathVariable long giftCertificateId,
											  @RequestParam long userId,
											  @RequestParam(required = false, defaultValue = "1") int quantity) {
		GiftCertificate giftCertificate = giftCertificateManager.findGiftCertificateById(giftCertificateId);
		User owner = userManager.findUserById(userId);
		Order order = new Order(quantity,owner, giftCertificate, "unpaid");
		Order savedOrder = orderManager.saveOrder(order);
		return ResponseUtils.response(201, "Gift certificate order created successfully",
				orderResourceAssembler.toResource(savedOrder));
	}

	@GetMapping("/{giftCertificateId:[0-9]+}/orders")
	public ResponseUtils findGiftCertificateOrders(@PathVariable long giftCertificateId,
												   Pageable pageRequest) {
		Page<Order> giftCertificateOrdersPage = orderManager.findOrdersByGiftCertificateId(giftCertificateId,
				pageRequest);
		return ResponseUtils.success(orderListResourceAssembler.build(giftCertificateOrdersPage));
	}

	/**
	 * Search gift certificate by tag(s)
	 * @param tags name of tag(s)
	 * @param pageable page number, page size and sort
	 * @return The common response {@link ResponseUtils} containing status code,
	 * 	 * 	 *  message and actual all data ({@link GiftCertificate})
	 */
	@GetMapping("/search-by-tag")
	public ResponseUtils searchByTag(@RequestParam(value = "tag") Optional<String[]> tags,
									 @RequestParam(name = "sort", required = false) Optional<String[]> sortBy,
									 Pageable pageable) {
		return ResponseUtils.success(giftCertificateListResourceAssembler.
				build(giftCertificateManager.findByTagNames(tags, sortBy, pageable)));
	}

	/**
	 * Search gift certificates by part of gift certificate's name
	 * @param name	part of a name
	 * @param pageable page number, page size and sort
	 * @return The common response {@link ResponseUtils} containing status code,
	 * 	 * 	 * 	 *  message and actual all data ({@link GiftCertificate})
	 */
	@GetMapping("/search-by-name")
	public ResponseUtils searchByName(@RequestParam(value = "name") Optional<String> name,
									  @RequestParam(name="sort", required = false) Optional<String[]> sortBy,
									  Pageable pageable) {
		return ResponseUtils.success(giftCertificateListResourceAssembler.
				build(giftCertificateManager.findByNameContaining(name, sortBy, pageable)));
	}

	/**
	 * Search gift certificates by part of gift certificate's description
	 * @param description part of description
	 * @param pageable page number, page size and sort
	 * @return The common response {@link ResponseUtils} containing status code,
	 * 	 * 	 * 	 message and actual all data ({@link GiftCertificate})
	 */
	@GetMapping("/search-by-description")
	public ResponseUtils searchByDescription(@RequestParam(value = "description") Optional<String> description,
											 @RequestParam(name="sort", required = false) Optional<String[]> sortBy,
											 Pageable pageable) {
		return ResponseUtils.success(giftCertificateListResourceAssembler.
				build(giftCertificateManager.findByDescriptionContaining(description, sortBy, pageable)));
	}
}
