package com.epam.esm.restadvanced.controller.model;

import com.epam.esm.restadvanced.common.api.resource.AbstractListResource;
import com.epam.esm.restadvanced.repository.entity.Tag;

import java.util.Collection;

public class TagListResource extends AbstractListResource {
    private final Collection<TagResource> tags;

    public TagListResource(Collection<TagResource> tags, int pageNumber, int pageSize, int totalPages, long totalElements) {
        super(pageNumber, pageSize, totalPages, totalElements);
        this.tags = tags;
    }

    public Collection<TagResource> getTags() {
        return tags;
    }
}
