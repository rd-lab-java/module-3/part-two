package com.epam.esm.restadvanced.controller.assembler;

import com.epam.esm.restadvanced.common.api.model.Page;
import com.epam.esm.restadvanced.common.api.resource.PageLinks;
import com.epam.esm.restadvanced.controller.v1.OrderController;
import com.epam.esm.restadvanced.controller.model.OrderListResource;
import com.epam.esm.restadvanced.controller.model.OrderResource;
import com.epam.esm.restadvanced.repository.entity.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class OrderListResourceAssembler {
    @Autowired
    private OrderResourceAssembler orderResourceAssembler;

    @PageLinks(OrderController.class)
    public OrderListResource build(Page<Order> page) {
        List<OrderResource> orderResources = page.getContent()
                .stream()
                .map(order -> orderResourceAssembler.toResource(order))
                .collect(Collectors.toList());

        return new OrderListResource(orderResources, page.getNumber(), page.getPageSize(),
                page.getTotalPages(), page.getTotalElements());
    }
}
