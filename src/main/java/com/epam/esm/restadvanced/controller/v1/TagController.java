package com.epam.esm.restadvanced.controller.v1;

import com.epam.esm.restadvanced.common.api.model.Page;
import com.epam.esm.restadvanced.common.api.model.PageRequest;
import com.epam.esm.restadvanced.common.api.model.Pageable;
import com.epam.esm.restadvanced.controller.assembler.GiftCertificateListResourceAssembler;
import com.epam.esm.restadvanced.controller.assembler.TagListResourceAssembler;
import com.epam.esm.restadvanced.controller.assembler.TagResourceAssembler;
import com.epam.esm.restadvanced.controller.model.GiftCertificateListResource;
import com.epam.esm.restadvanced.controller.model.TagListResource;
import com.epam.esm.restadvanced.controller.model.TagResource;
import com.epam.esm.restadvanced.repository.entity.GiftCertificate;
import com.epam.esm.restadvanced.repository.entity.Tag;
import com.epam.esm.restadvanced.service.GiftCertificateManager;
import com.epam.esm.restadvanced.service.TagManager;
import com.epam.esm.restadvanced.service.exception.TagDuplicateException;
import com.epam.esm.restadvanced.util.ResponseUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Rest controller api for managing 
 * tag based requests and responses.
 * 
 * @author yusuf
 *
 */
@RestController
@RequestMapping("/v1/tags")
public class TagController {
	/** Tag manager service class variable */
	@Autowired
	private TagManager tagManager;
	@Autowired
	private GiftCertificateManager giftCertificateManager;
	@Autowired
	private GiftCertificateListResourceAssembler giftCertificateListResourceAssembler;
	@Autowired
	private TagListResourceAssembler tagListResourceAssembler;
	@Autowired
	private TagResourceAssembler tagResourceAssembler;

	/**
	 * Used to find all tags 
	 * 
	 * @return The common response {@link ResponseUtils} containing status code,
	 *  message and actual data ({@link Tag})
	 * @throws Exception {@link com.epam.esm.restadvanced.service.exception.TagNotFoundException}
	 * when no any data
	 */
	@GetMapping(produces = "application/json")
	public ResponseUtils findAll(Pageable pageable) {
		Page<Tag> tagPage = tagManager.findAll(pageable);
		TagListResource tagListResource = tagListResourceAssembler.build(tagPage);
		return ResponseUtils.success(tagListResource);
	}
	
	/**
	 * Used to save tag 
	 * 
	 * @param tag {@link Tag} to save
	 * @return The common response {@link ResponseUtils} containing status code,
	 *  message and actual data ({@link Tag})
	 * @throws Exception {@link TagDuplicateException} when providing duplicate data
	 */
	@PostMapping(consumes = "application/json", produces = "application/json")
	public ResponseUtils saveTag(@RequestBody Tag tag) {
		Tag savedTag = tagManager.saveTag(tag);
		return ResponseUtils.response(201, "Tag created successfully", tagResourceAssembler.
				toResource(savedTag));
	}
	
	/**
	 * Used to find tag by tag id
	 * 
	 * @param tagId 
	 * @return The common response {@link ResponseUtils} containing status code,
	 *  message and actual data ({@link Tag})
	 * @throws Exception {@link com.epam.esm.restadvanced.service.exception.TagNotFoundException}
	 *  when no any data based on tag id
	 */
	@GetMapping("/{tagId:[0-9]+}")
	public ResponseUtils findTag(@PathVariable long tagId) {
		Tag tag = tagManager.findTagById(tagId);
		return ResponseUtils.success(tagResourceAssembler.toResource(tag));
	}
	
	/**
	 * Used to delete tag by tag id
	 * 
	 * @param tagId
	 * @return The common response {@link ResponseUtils} containing status code,
	 *  message and actual data ({@link Tag})
	 * @throws Exception {@link com.epam.esm.restadvanced.service.exception.TagNotFoundException}
	 *  when no any data based on tag id
	 */
	@DeleteMapping("/{tagId:[0-9]+}")
	public ResponseUtils deleteTag(@PathVariable long tagId) {
		Tag deletedTag = tagManager.deleteTagById(tagId);
		return ResponseUtils.response(200, "Tag deleted successfully",
				tagResourceAssembler.toResource(deletedTag));
	}

	/**
	 * Get all gift certificates that belong to the specific tag
	 *
	 * @param tagId id of tag that contains gift certificates
	 * @param pageable page number and page size
	 * @return The common response {@link ResponseUtils} containing status code,
	 * 	 *  message and actual all data ({@link GiftCertificate})
	 */
	@GetMapping("/{tagId:[0-9]+}/gift-certificates")
	public ResponseUtils findGiftCertificatesByTagId(@PathVariable long tagId,
													 Pageable pageable) {
		Page<GiftCertificate> giftCertificatePage = giftCertificateManager.
				findGiftCertificatesByTagId(tagId, pageable);
		GiftCertificateListResource giftCertificateListResource = giftCertificateListResourceAssembler.
				build(giftCertificatePage);
		return ResponseUtils.success(giftCertificateListResource);
	}

	/**
	 * Get most used tags
	 *
	 * @param top limit that is to determine to get top tags
	 * @param pageable page number ,page size and sort
	 * @return The common response {@link ResponseUtils} containing status code,
	 * 	 * 	 *  message and actual all data ({@link Tag})
	 */
	@GetMapping("/most-used")
	public ResponseUtils getMostWidelyUsedTags(@RequestParam(value = "top") Optional<Integer> top,
											   Pageable pageable) {

		if(top.isPresent()) {
			return ResponseUtils.success(getTagResources(tagManager.findTopMostUsedTags(top.get())));
		}
		Page<Tag> mostWidelyUsedTags = tagManager.findMostUsedTags(pageable);
		TagListResource tagListResource = tagListResourceAssembler.build(mostWidelyUsedTags);
		return ResponseUtils.success(tagListResource);
	}

	/**
	 * Convert tags to tag resources
	 *
	 * @param tags list of tags
	 * @return returns list of tag resources {@link TagResource}
	 */
	private List<TagResource> getTagResources(List<Tag> tags) {
		return tags.stream()
				.map(tag -> tagResourceAssembler.toResource(tag))
				.collect(Collectors.toList());
	}
}
