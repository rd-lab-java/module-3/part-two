package com.epam.esm.restadvanced.controller.assembler;

import com.epam.esm.restadvanced.common.api.model.Page;
import com.epam.esm.restadvanced.common.api.resource.PageLinks;
import com.epam.esm.restadvanced.controller.v1.UserController;
import com.epam.esm.restadvanced.controller.model.UserListResource;
import com.epam.esm.restadvanced.controller.model.UserResource;
import com.epam.esm.restadvanced.repository.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class UserListResourceAssembler {
    @Autowired
    private UserResourceAssembler userResourceAssembler;

    @PageLinks(UserController.class)
    public UserListResource build(Page<User> page) {

        List<UserResource> userResources = page.getContent().stream()
                .map(user -> userResourceAssembler.toResource(user))
                .collect(Collectors.toList());

        return new UserListResource(userResources, page.getNumber(), page.getPageSize(),
                page.getTotalPages(), page.getTotalElements());
    }
}
