package com.epam.esm.restadvanced.controller.assembler;

import com.epam.esm.restadvanced.common.api.model.Page;
import com.epam.esm.restadvanced.common.api.resource.PageLinks;
import com.epam.esm.restadvanced.controller.v1.TagController;
import com.epam.esm.restadvanced.controller.model.TagListResource;
import com.epam.esm.restadvanced.controller.model.TagResource;
import com.epam.esm.restadvanced.repository.entity.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class TagListResourceAssembler {
    @Autowired
    private TagResourceAssembler tagResourceAssembler;

    @PageLinks(TagController.class)
    public TagListResource build(Page<Tag> page) {
        List<TagResource> tags = page.getContent()
                .stream()
                .map(tag -> tagResourceAssembler.toResource(tag))
                .collect(Collectors.toList());
        return new TagListResource(tags, page.getNumber(), page.getPageSize(), page.getTotalPages(),
                page.getTotalElements());
    }
}
