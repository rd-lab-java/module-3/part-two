package com.epam.esm.restadvanced.controller.mapper;

import com.epam.esm.restadvanced.controller.model.GiftCertificateResourceInput;
import com.epam.esm.restadvanced.repository.entity.GiftCertificate;

public interface GiftCertificateMapper {
    GiftCertificateResourceInput asGiftCertificateResourceInput(GiftCertificate giftCertificate);
    void update(GiftCertificateResourceInput giftCertificateResource, GiftCertificate giftCertificate);
}
