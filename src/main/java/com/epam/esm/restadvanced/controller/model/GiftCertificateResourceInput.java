package com.epam.esm.restadvanced.controller.model;

import com.epam.esm.restadvanced.repository.entity.Tag;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GiftCertificateResourceInput {
    private long id;
    private String name;
    private String description;
    private double price;
    private int duration;
}
