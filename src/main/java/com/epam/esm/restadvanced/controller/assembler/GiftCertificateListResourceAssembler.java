package com.epam.esm.restadvanced.controller.assembler;

import com.epam.esm.restadvanced.common.api.model.Page;
import com.epam.esm.restadvanced.common.api.resource.PageLinks;
import com.epam.esm.restadvanced.controller.v1.GiftCertificateController;
import com.epam.esm.restadvanced.controller.model.GiftCertificateListResource;
import com.epam.esm.restadvanced.controller.model.GiftCertificateResource;
import com.epam.esm.restadvanced.repository.entity.GiftCertificate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class GiftCertificateListResourceAssembler {

    @Autowired
    private GiftCertificateResourceAssembler giftCertificateResourceAssembler;

    @PageLinks(GiftCertificateController.class)
    public GiftCertificateListResource build(Page<GiftCertificate> page) {
        List<GiftCertificateResource> giftCertificates = page.getContent()
                .stream()
                .map(giftCertificate -> giftCertificateResourceAssembler.toResource(giftCertificate))
                .collect(Collectors.toList());

        return new GiftCertificateListResource(giftCertificates, page.getNumber(), page.getPageSize(),
                page.getTotalPages(), page.getTotalElements());
    }
}
