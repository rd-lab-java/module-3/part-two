package com.epam.esm.restadvanced.controller.model;

import com.epam.esm.restadvanced.common.api.resource.AbstractListResource;

import java.util.Collection;

public class OrderListResource extends AbstractListResource {
    private final Collection<OrderResource> orderResources;

    public OrderListResource(Collection<OrderResource> orderResources, int pageNumber, int pageSize,
                             int totalPages, long totalElements) {
        super(pageNumber, pageSize, totalPages, totalElements);
        this.orderResources = orderResources;
    }

    public Collection<OrderResource> getOrderResources() {
        return orderResources;
    }
}
