package com.epam.esm.restadvanced.controller.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.RepresentationModel;

import java.util.Date;

public class UserResource extends RepresentationModel<UserResource> {
    public long id;
    public String fullName;
    public String email;
    public String password;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss")
    public Date createdDate;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss")
    public Date lastUpdatedDate;

    public UserResource(long id, String fullName, String email, String password, Date createdDate, Date lastUpdatedDate) {
        this.id = id;
        this.fullName = fullName;
        this.email = email;
        this.password = password;
        this.createdDate = createdDate;
        this.lastUpdatedDate = lastUpdatedDate;
    }

    public UserResource(Link initialLink, long id, String fullName, String email, String password, Date createdDate, Date lastUpdatedDate) {
        super(initialLink);
        this.id = id;
        this.fullName = fullName;
        this.email = email;
        this.password = password;
        this.createdDate = createdDate;
        this.lastUpdatedDate = lastUpdatedDate;
    }

    public UserResource(Iterable<Link> initialLinks, long id, String fullName, String email, String password, Date createdDate, Date lastUpdatedDate) {
        super(initialLinks);
        this.id = id;
        this.fullName = fullName;
        this.email = email;
        this.password = password;
        this.createdDate = createdDate;
        this.lastUpdatedDate = lastUpdatedDate;
    }
}
