package com.epam.esm.restadvanced.controller.assembler;

import com.epam.esm.restadvanced.common.api.model.Pageable;
import com.epam.esm.restadvanced.controller.v1.UserController;
import com.epam.esm.restadvanced.controller.model.UserResource;
import com.epam.esm.restadvanced.repository.entity.User;
import org.springframework.stereotype.Component;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@Component
public class UserResourceAssembler {
    public UserResource toResource(User user) {
        UserResource userResource = new UserResource(user.getId(), user.getFullName(), user.getEmail(),
                user.getPassword(), user.getCreatedDate(), user.getLastUpdatedDate());
        userResource.add(linkTo(methodOn(UserController.class).findUserById(user.getId())).withSelfRel());
        userResource.add(linkTo(methodOn(UserController.class).
                findOrders(user.getId(), Pageable.unpaged())).withRel("orders"));
        return userResource;
    }
}
