package com.epam.esm.restadvanced.common.api.model;

public enum Unpaged implements Pageable {
    INSTANCE;

    @Override
    public Pageable next() {
        return this;
    }

    @Override
    public Pageable previous() {
        return this;
    }

    @Override
    public int getPageNumber() {
        throw new UnsupportedOperationException();
    }

    @Override
    public int getPageSize() {
        throw new UnsupportedOperationException();
    }
}
