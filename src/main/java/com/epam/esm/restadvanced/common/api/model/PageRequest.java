package com.epam.esm.restadvanced.common.api.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
public class PageRequest implements Pageable{
    private final int page;
    private final int size;

    public PageRequest() {
        page = 1;
        size = 5;
    }

    @Override
    public PageRequest next() {
        return new PageRequest(page + 1, size);
    }

    @Override
    public PageRequest previous() {
        return new PageRequest(page - 1, size);
    }

    @Override
    public int getPageNumber() {
        return page;
    }

    @Override
    public int getPageSize() {
        return size;
    }
}
