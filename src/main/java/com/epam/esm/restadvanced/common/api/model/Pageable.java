package com.epam.esm.restadvanced.common.api.model;

public interface Pageable {
    Pageable next();
    Pageable previous();
    int getPageNumber();
    int getPageSize();
    static Pageable unpaged() {
        return Unpaged.INSTANCE;
    }
}
