package com.epam.esm.restadvanced.common.api.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.NoArgsConstructor;

import java.util.List;

@NoArgsConstructor
public class Page<T> {
    /** the page content as List */
    private List<T> content;
    /** the total amount of elements. */
    private int totalElements;
    /** the number of total pages */
    private int totalPages;
    /** the number of the current page */
    private int number;
    private int pageSize;
    @JsonIgnore
    private Pageable nextPageable;
    @JsonIgnore
    private Pageable previousPageable;

    public List<T> getContent() {
        return content;
    }

    public int getTotalElements() {
        return totalElements;
    }

    public int getTotalPages() {
        return totalPages;
    }

    public int getNumberOfElements() {
        return content.size();
    }

    public int getNumber() {
        return number;
    }

    public void setContent(List<T> content) {
        this.content = content;
    }

    public void setTotalElements(int totalElements) {
        this.totalElements = totalElements;
    }

    public void setTotalPages(int totalPages) {
        this.totalPages = totalPages;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public boolean isFirst() {
        return number == 1;
    }

    public boolean isLast() {
        return number == totalPages;
    }

    public boolean isEmpty() {
        return totalElements == 0;
    }

    public boolean hasNext() {
        return number < totalPages;
    }

    public boolean hasPrevious() {
        return number > 1 && number <= totalPages;
    }

    public boolean hasContent() {
        return content != null && content.size() > 0;
    }

    public Pageable getNextPageable() {
        return nextPageable;
    }

    public void setNextPageable(Pageable nextPageable) {
        this.nextPageable = nextPageable;
    }

    public Pageable getPreviousPageable() {
        return previousPageable;
    }

    public void setPreviousPageable(Pageable previousPageable) {
        this.previousPageable = previousPageable;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }
}
