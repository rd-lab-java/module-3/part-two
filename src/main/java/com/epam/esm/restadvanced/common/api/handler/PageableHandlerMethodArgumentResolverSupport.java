package com.epam.esm.restadvanced.common.api.handler;

import com.epam.esm.restadvanced.common.api.model.PageRequest;
import com.epam.esm.restadvanced.common.api.model.Pageable;
import org.springframework.core.MethodParameter;
import org.springframework.util.StringUtils;

import java.util.Optional;

public class PageableHandlerMethodArgumentResolverSupport {
    protected static final String DEFAULT_PAGE_PARAMETER = "page";
    protected static final String DEFAULT_SIZE_PARAMETER = "size";
    private static final int DEFAULT_MAX_PAGE_SIZE = 2000;
    static final Pageable DEFAULT_PAGE_REQUEST = new PageRequest(1, 20);
    private boolean oneIndexedParameters = false;

    protected Pageable getPageable(MethodParameter parameter, String pageString, String pageSizeString) {

        Pageable defaultPageable = DEFAULT_PAGE_REQUEST;

        Optional<Integer> page = parseAndApplyBoundaries(pageString, Integer.MAX_VALUE, true);
        Optional<Integer> pageSize = parseAndApplyBoundaries(pageSizeString, DEFAULT_MAX_PAGE_SIZE, false);

        int p = page
                .orElseGet(defaultPageable::getPageNumber);
        int ps = pageSize
                .orElseGet(defaultPageable::getPageSize);

        // Limit lower bound
        ps = ps < 1 ? defaultPageable.getPageSize() : ps;
        // Limit upper bound
        ps = ps > DEFAULT_MAX_PAGE_SIZE ? DEFAULT_MAX_PAGE_SIZE : ps;

        return new PageRequest(p, ps);
    }

    private Optional<Integer> parseAndApplyBoundaries(String parameter, int upper, boolean shiftIndex) {

        if (!StringUtils.hasText(parameter)) {
            return Optional.empty();
        }

        try {
            int parsed = Integer.parseInt(parameter) - (oneIndexedParameters && shiftIndex ? 1 : 0);
            return Optional.of(parsed < 0 ? 0 : parsed > upper ? upper : parsed);
        } catch (NumberFormatException e) {
            return Optional.of(0);
        }
    }
}
