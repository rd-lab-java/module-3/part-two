package com.epam.esm.restadvanced.common.api.handler;

import com.epam.esm.restadvanced.common.api.model.Pageable;
import org.springframework.core.MethodParameter;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;


public class PageableHandleMethodArgumentResolver extends PageableHandlerMethodArgumentResolverSupport
        implements HandlerMethodArgumentResolver {
    @Override
    public boolean supportsParameter(MethodParameter parameter) {
        return Pageable.class.equals(parameter.getParameterType());
    }

    @Override
    public Object resolveArgument(MethodParameter parameter, ModelAndViewContainer mavContainer,
                                  NativeWebRequest webRequest, WebDataBinderFactory binderFactory) throws Exception {
        String page = webRequest.getParameter(DEFAULT_PAGE_PARAMETER);
        String size = webRequest.getParameter(DEFAULT_SIZE_PARAMETER);
        return getPageable(parameter, page, size);
    }
}
