package com.epam.esm.restadvanced.config;

import org.apache.commons.dbcp2.BasicDataSource;
import org.springframework.context.annotation.*;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;

import javax.sql.DataSource;

@Configuration
@PropertySources({
        @PropertySource(name="db_prod.properties", value = "classpath:application-dev.properties")
})
public class DevConfig {
    @Bean(name = "devDataSource")
    @Profile("dev")
    public DataSource getDataSource(Environment environment) {
        BasicDataSource basicDataSource = new BasicDataSource();
        basicDataSource.setDriverClassName(environment.getProperty("spring.datasource.driverClassName"));
        basicDataSource.setUrl(environment.getProperty("spring.datasource.url"));
        basicDataSource.setMaxIdle(environment.getProperty("spring.datasource.maxIdle", Integer.class));
        basicDataSource.setMaxWaitMillis(environment.getProperty("spring.datasource.maxWait", Long.class));
        basicDataSource.setMaxTotal(environment.getProperty("spring.datasource.maxActive", Integer.class));
        basicDataSource.setUsername(environment.getProperty("spring.datasource.user"));
        basicDataSource.setPassword(environment.getProperty("spring.datasource.password"));
        return basicDataSource;
    }

    @Bean(name = "devTxManager")
    @Profile("dev")
    public PlatformTransactionManager txManager(DataSource dataSource) {
        return new DataSourceTransactionManager(dataSource);
    }

    @Bean(name = "devJdbcTemplate")
    @Profile("dev")
    public JdbcTemplate getJdbcTemplate(DataSource dataSource) {
        return new JdbcTemplate(dataSource);
    }
}
