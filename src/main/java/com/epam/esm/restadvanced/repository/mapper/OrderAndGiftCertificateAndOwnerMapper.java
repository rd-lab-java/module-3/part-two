package com.epam.esm.restadvanced.repository.mapper;

import com.epam.esm.restadvanced.repository.GiftCertificateRepository;
import com.epam.esm.restadvanced.repository.UserRepository;
import com.epam.esm.restadvanced.repository.entity.GiftCertificate;
import com.epam.esm.restadvanced.repository.entity.Order;
import com.epam.esm.restadvanced.repository.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class OrderAndGiftCertificateAndOwnerMapper implements RowMapper<Order> {
    private final GiftCertificateRepository giftCertificateRepository;
    private final UserRepository userRepository;

    @Autowired
    public OrderAndGiftCertificateAndOwnerMapper(GiftCertificateRepository giftCertificateRepository,
                                                 UserRepository userRepository) {
        this.giftCertificateRepository = giftCertificateRepository;
        this.userRepository = userRepository;
    }

    @Override
    public Order mapRow(ResultSet rs, int rowNum) throws SQLException {
        Order order = new Order();
        order.setId(rs.getLong("id"));
        order.setQuantity(rs.getInt("quantity"));
        order.setTotalCost(rs.getDouble("total_cost"));
        order.setOrderedDate(rs.getTimestamp("ordered_date"));
        order.setLastUpdatedDate(rs.getTimestamp("last_updated_date"));
        order.setStatus(rs.getString("status"));
        User owner = userRepository.findUserById(rs.getLong("owner_id"));
        order.setOwner(owner);
        GiftCertificate giftCertificate = giftCertificateRepository.findGiftCertificateById(
                rs.getLong("gift_certificate_id"));
        order.setGiftCertificate(giftCertificate);
        return order;
    }
}
