package com.epam.esm.restadvanced.repository.impl;

import com.epam.esm.restadvanced.common.api.model.Page;
import com.epam.esm.restadvanced.common.api.model.PageRequest;
import com.epam.esm.restadvanced.common.api.model.Pageable;
import com.epam.esm.restadvanced.repository.PageRepository;
import com.epam.esm.restadvanced.repository.TagRepository;
import com.epam.esm.restadvanced.repository.entity.Tag;
import com.epam.esm.restadvanced.repository.mapper.TagMapper;
import com.epam.esm.restadvanced.service.exception.TagDuplicateException;
import com.epam.esm.restadvanced.service.exception.TagNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.core.env.Environment;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.Statement;
import java.util.List;

/**
 * Tag repository implementation contains
 * implementations with data source 
 * and has jdbc template {@link JdbcTemplate} dependency
 * 
 * @author yusuf
 *
 */
@Repository
@PropertySources({
	@PropertySource(name="sqlStatements", value = "classpath:sql-statements.properties"),
	@PropertySource(name="stringValues", value = "classpath:string-values.properties")
})
public class TagRepositoryImpl implements TagRepository {
	
	private final JdbcTemplate jdbcTemplate;
	private final Environment environment;
	private final PageRepository<Tag> tagPageRepository;
	
	@Autowired
	public TagRepositoryImpl(JdbcTemplate jdbcTemplate, Environment environment, PageRepository<Tag> tagPageRepository) {
		super();
		this.jdbcTemplate = jdbcTemplate;
		this.environment = environment;
		this.tagPageRepository = tagPageRepository;
	}

	@Override
	public Tag saveTag(Tag tag) {
		try {
			KeyHolder keyHolder = new GeneratedKeyHolder();
			jdbcTemplate.update(connection -> {
				PreparedStatement ps = connection.prepareStatement(environment.getProperty("tag.insert"), Statement.RETURN_GENERATED_KEYS);
				ps.setString(1, tag.getName());
				return ps;
			}, keyHolder);
			tag.setId(keyHolder.getKey().longValue());
			return tag;
		} catch(DuplicateKeyException ex) {
			throw new TagDuplicateException(String.format(environment.getProperty("tag.message.duplicate"), tag.getName()), ex);
		}
	}

	@Override
	public Page<Tag> findTagsByGiftCertificateId(long giftCertId, Pageable pageRequest) {
		Page<Tag> tagPage = tagPageRepository.fetchPage(
				String.format(environment.getProperty("tag.find.giftCertificateTagsPage"), giftCertId),
				String.format(environment.getProperty("tag.count.giftCertificateTags"), giftCertId),
				new TagMapper(), pageRequest);
		if(tagPage.getTotalElements() == 0) {
			throw new TagNotFoundException(
					String.format(environment.getProperty("tag.message.notFoundByGiftCertificateId"), giftCertId));
		}
		return tagPage;
	}

	@Override
	public List<Tag> findTagsByGiftCertificateId(long giftCertId) {
		return jdbcTemplate.query(environment.getProperty("tag.find.giftCertificateTags"), new TagMapper(),
				new Object[] {giftCertId});
	}

	@Override
	public Tag findTagByName(String name) {
		try {
			return jdbcTemplate.queryForObject(environment.getProperty("tag.find.byName"), new TagMapper(), name);
		} catch(EmptyResultDataAccessException ex) {
			throw new TagNotFoundException(
					String.format(environment.getProperty("tag.message.notFoundByName"), name), ex);
		}
	}

	@Override
	public Page<Tag> findAll(Pageable pageRequest) {
		Page<Tag> tagPage = tagPageRepository.fetchPage(environment.getProperty("tag.find.all"),
				environment.getProperty("tag.count.all"), new TagMapper(), pageRequest);
		if(tagPage.getTotalElements() == 0) {
			throw new TagNotFoundException(environment.getProperty("tag.message.allNotFound"));
		}
		return tagPage;
	}

	@Override
	public Tag deleteTagById(long id) {
		try {
			Tag existedTag = jdbcTemplate.queryForObject(environment.getProperty("tag.find.byId"), new TagMapper(), id);
			jdbcTemplate.update(environment.getProperty("tag.delete"), id);
			return existedTag;
		} catch(EmptyResultDataAccessException ex) {
			throw new TagNotFoundException(String.format(environment.getProperty("tag.message.notFoundById"), id), ex);
		}
	}

	@Override
	public Tag findTagById(long id) {
		try {
			return jdbcTemplate.queryForObject(environment.getProperty("tag.find.byId"), new TagMapper(), id);
		} catch(EmptyResultDataAccessException ex) {
			throw new TagNotFoundException(String.format(environment.getProperty("tag.message.notFoundById"), id), ex);
		}
	}

	@Override
	public Page<Tag> findMostWidelyUsedTags(Pageable pageRequest) {
		Page<Tag> tagPage = tagPageRepository.fetchPage(environment.getProperty("tag.find.mostWidelyUsed"),
				environment.getProperty("tag.count.mostWidelyUsed"),
				new TagMapper(), pageRequest);
		if(tagPage.getTotalElements() == 0)
			throw new TagNotFoundException(environment.getProperty("tag.message.allNotFound"));
		return tagPage;
	}

	@Override
	public List<Tag> findTopMostWidelyUsedTags(int top) {
		List<Tag> tags = jdbcTemplate.query(environment.getProperty("tag.find.topMostWidelyUsed"), new TagMapper(), top);
		if(tags.size() == 0)
			throw new TagNotFoundException(environment.getProperty("tag.message.allNotFound"));
		return tags;
	}

	@Override
	public void deleteAll() {
		jdbcTemplate.update(environment.getProperty("tag.delete.all"));
		
		/** unset when using h2 embedded database */
		jdbcTemplate.update(environment.getProperty("tag.reset.autoincrement"));
	}
}
