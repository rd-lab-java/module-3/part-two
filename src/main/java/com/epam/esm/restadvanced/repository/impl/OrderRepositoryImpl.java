package com.epam.esm.restadvanced.repository.impl;

import com.epam.esm.restadvanced.common.api.model.Page;
import com.epam.esm.restadvanced.common.api.model.PageRequest;
import com.epam.esm.restadvanced.common.api.model.Pageable;
import com.epam.esm.restadvanced.repository.GiftCertificateRepository;
import com.epam.esm.restadvanced.repository.OrderRepository;
import com.epam.esm.restadvanced.repository.PageRepository;
import com.epam.esm.restadvanced.repository.UserRepository;
import com.epam.esm.restadvanced.repository.entity.Order;
import com.epam.esm.restadvanced.repository.entity.Tag;
import com.epam.esm.restadvanced.repository.mapper.OrderAndGiftCertificateAndOwnerMapper;
import com.epam.esm.restadvanced.repository.mapper.OrderExtractor;
import com.epam.esm.restadvanced.repository.mapper.TagMapper;
import com.epam.esm.restadvanced.service.exception.OrderDuplicateException;
import com.epam.esm.restadvanced.service.exception.OrderNotFoundException;
import com.epam.esm.restadvanced.service.exception.TagNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.core.env.Environment;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.Statement;
import java.sql.Timestamp;
import java.time.LocalDateTime;

@Repository
@PropertySources({
        @PropertySource(name="sqlStatements", value = "classpath:sql-statements.properties"),
        @PropertySource(name="stringValues", value = "classpath:string-values.properties")
})
public class OrderRepositoryImpl implements OrderRepository {
    private final JdbcTemplate jdbcTemplate;
    private final Environment environment;
    private final UserRepository userRepository;
    private final GiftCertificateRepository giftCertificateRepository;
    private final PageRepository<Order> orderPageRepository;

    @Autowired
    public OrderRepositoryImpl(JdbcTemplate jdbcTemplate, Environment environment, UserRepository userRepository,
                               GiftCertificateRepository giftCertificateRepository,
                               PageRepository<Order> orderPageRepository) {
        this.jdbcTemplate = jdbcTemplate;
        this.environment = environment;
        this.userRepository = userRepository;
        this.giftCertificateRepository = giftCertificateRepository;
        this.orderPageRepository = orderPageRepository;
    }

    @Override
    public Order saveOrder(Order order) {
        try {
            KeyHolder keyHolder = new GeneratedKeyHolder();
            jdbcTemplate.update(connection -> {
                PreparedStatement ps = connection.prepareStatement(environment.getProperty("order.insert"),
                        Statement.RETURN_GENERATED_KEYS);
                ps.setDouble(1, order.getTotalCost());
                ps.setInt(2, order.getQuantity());
                ps.setString(3, order.getStatus());
                ps.setObject(4, Timestamp.valueOf(LocalDateTime.now()));
                ps.setObject(5, order.getLastUpdatedDate());
                ps.setLong(6, order.getOwner().getId());
                ps.setLong(7, order.getGiftCertificate().getId());
                return ps;
            }, keyHolder);
            order.setId(keyHolder.getKey().longValue());
            return order;
        } catch(DuplicateKeyException ex) {
            throw new OrderDuplicateException(String.format(environment.getProperty("order.message.duplicate"),
                    order.getOwner().getId(), order.getGiftCertificate().getId()), ex);
        }
    }

    @Override
    public Page<Order> findOrdersByGiftCertificateId(long giftCertificateId, Pageable pageRequest) {
        Page<Order> orderPage = orderPageRepository.fetchPage(
                String.format(environment.getProperty("order.find.byGiftCertificateId"), giftCertificateId) ,
                String.format(environment.getProperty("order.count.byGiftCertificateId"), giftCertificateId),
                new OrderExtractor(userRepository, giftCertificateRepository, environment), pageRequest);
        if(orderPage.getTotalElements() == 0) {
            throw new OrderNotFoundException(String.format(
                    environment.getProperty("order.message.notFoundByGiftCertificateId"), giftCertificateId));
        }
        return orderPage;
    }

    @Override
    public Page<Order> findOrdersByOwnerId(long userId, Pageable pageRequest) {
        Page<Order> orderPage = orderPageRepository.fetchPage(
                String.format(environment.getProperty("order.find.byOwnerId"), userId),
                String.format(environment.getProperty("order.count.byOwnerId"), userId),
                new OrderExtractor(userRepository, giftCertificateRepository, environment), pageRequest);
        if(orderPage.getTotalElements() == 0) {
            throw new OrderNotFoundException(String.format(
                    environment.getProperty("order.message.notFoundByOwnerId"), userId));
        }
        return orderPage;
    }

    @Override
    public Order findOrderById(long orderId) {
        try {
            return jdbcTemplate.queryForObject(environment.getProperty("order.find.byId"),
                    new OrderAndGiftCertificateAndOwnerMapper(giftCertificateRepository, userRepository),
                    orderId);
        } catch(EmptyResultDataAccessException ex) {
            throw new OrderNotFoundException(
                    String.format(environment.getProperty("order.message.notFoundById"), orderId), ex);
        }
    }

    @Override
    public Order deleteById(long id) {
        try {
            Order existedOrder = jdbcTemplate.queryForObject(environment.getProperty("order.find.byId"),
                    new OrderAndGiftCertificateAndOwnerMapper(giftCertificateRepository, userRepository), id);
            jdbcTemplate.update(environment.getProperty("order.delete.byId"), id);
            return existedOrder;
        } catch(EmptyResultDataAccessException ex) {
            throw new OrderNotFoundException(
                    String.format(environment.getProperty("order.message.notFoundById"), id), ex);
        }
    }

    @Override
    public void deleteAll() {
        jdbcTemplate.update(environment.getProperty("order.delete.all"));

        /** unset when using h2 embedded database */
        jdbcTemplate.update(environment.getProperty("order.reset.autoincrement"));
    }
}
