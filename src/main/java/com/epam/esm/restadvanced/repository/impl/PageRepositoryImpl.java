package com.epam.esm.restadvanced.repository.impl;

import com.epam.esm.restadvanced.common.api.model.Page;
import com.epam.esm.restadvanced.common.api.model.PageRequest;
import com.epam.esm.restadvanced.common.api.model.Pageable;
import com.epam.esm.restadvanced.repository.PageRepository;
import com.sun.webkit.PageCache;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class PageRepositoryImpl<T> implements PageRepository<T> {
    private final JdbcTemplate jdbcTemplate;
    @Autowired
    public PageRepositoryImpl(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public Page<T> fetchPage(String sqlQuery, String countQuery, RowMapper<T> rowMapper, Pageable pageRequest) {
        Integer rowCount = jdbcTemplate.queryForObject(countQuery, Integer.class);
        System.out.println(pageRequest.getPageNumber() + ", " + pageRequest.getPageSize());
        int pageCount = rowCount / pageRequest.getPageSize();
        if(rowCount > pageRequest.getPageSize() * pageCount) {
            pageCount++;
        }
        int startRow = (pageRequest.getPageNumber() - 1) * pageRequest.getPageSize();
        List<T> contents = jdbcTemplate.query(
                sqlQuery, rowMapper, pageRequest.getPageSize(), startRow);
        return getPageFromList(rowCount, pageCount, pageRequest, contents);
    }



    @Override
    public Page<T> fetchPage(String sqlQuery, String countQuery, ResultSetExtractor<List<T>> extractor,
                             Pageable pageRequest) {
        Integer rowCount = jdbcTemplate.queryForObject(countQuery, Integer.class);
        int pageCount = rowCount / pageRequest.getPageSize();
        if(rowCount > pageRequest.getPageSize() * pageCount) {
            pageCount++;
        }
        int startRow = (pageRequest.getPageNumber() - 1) * pageRequest.getPageSize();
        List<T> contents = jdbcTemplate.query(
                sqlQuery, extractor, pageRequest.getPageSize(), startRow);
        return getPageFromList(rowCount, pageCount, pageRequest, contents);
    }

    private Page<T> getPageFromList(int totalElementsCount, int totalPageCount,
                                    Pageable pageRequest, List<T> contents) {
        Page<T> page = new Page<>();
        page.setContent(contents);
        page.setNumber(pageRequest.getPageNumber());
        page.setTotalPages(totalPageCount);
        page.setTotalElements(totalElementsCount);
        page.setNextPageable(pageRequest.next());
        page.setPreviousPageable(pageRequest.previous());
        page.setPageSize(pageRequest.getPageSize());
        return page;
    }
}
