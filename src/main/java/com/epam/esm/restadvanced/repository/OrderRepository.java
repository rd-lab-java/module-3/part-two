package com.epam.esm.restadvanced.repository;

import com.epam.esm.restadvanced.common.api.model.Page;
import com.epam.esm.restadvanced.common.api.model.PageRequest;
import com.epam.esm.restadvanced.common.api.model.Pageable;
import com.epam.esm.restadvanced.repository.entity.Order;

public interface OrderRepository {
    Order saveOrder(Order order);
    Page<Order> findOrdersByGiftCertificateId(long giftCertificateId, Pageable pageRequest);
    Page<Order> findOrdersByOwnerId(long userId, Pageable pageRequest);
    Order findOrderById(long orderId);
    Order deleteById(long orderId);
    void deleteAll();
}
