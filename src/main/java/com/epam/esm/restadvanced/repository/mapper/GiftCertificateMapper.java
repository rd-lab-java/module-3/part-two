package com.epam.esm.restadvanced.repository.mapper;

import com.epam.esm.restadvanced.repository.entity.GiftCertificate;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class GiftCertificateMapper implements RowMapper<GiftCertificate> {

	@Override
	public GiftCertificate mapRow(ResultSet rs, int rowNum) throws SQLException {
		GiftCertificate giftCertificate = new GiftCertificate();
		giftCertificate.setId(rs.getLong("id"));
		giftCertificate.setName(rs.getString("name"));
		giftCertificate.setDescription(rs.getString("description"));
		giftCertificate.setPrice(rs.getDouble("price"));
		giftCertificate.setDuration(rs.getInt("duration"));
		giftCertificate.setCreatedDate(rs.getTimestamp("create_date"));
		giftCertificate.setLastUpdatedDate(rs.getTimestamp("last_update_date"));
		return giftCertificate;
	}

}
