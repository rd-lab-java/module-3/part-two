package com.epam.esm.restadvanced.repository.entity;


import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Data
public class Order {
    private long id;
    private double totalCost;
    private double unitPrice;
    private int quantity;
    private String status;
    private Date orderedDate;
    private Date lastUpdatedDate;
    private User owner;
    private GiftCertificate giftCertificate;

    public Order() {
        this.orderedDate = new Date();
    }

    public Order(int quantity, User owner, GiftCertificate giftCertificate, String status) {
        this.quantity = quantity;
        this.orderedDate = new Date();
        this.owner = owner;
        this.giftCertificate = giftCertificate;
        this.status = status;
        this.unitPrice = giftCertificate.getPrice();
    }
}
