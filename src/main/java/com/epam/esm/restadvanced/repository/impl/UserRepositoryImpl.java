package com.epam.esm.restadvanced.repository.impl;

import com.epam.esm.restadvanced.common.api.model.Page;
import com.epam.esm.restadvanced.common.api.model.PageRequest;
import com.epam.esm.restadvanced.common.api.model.Pageable;
import com.epam.esm.restadvanced.repository.PageRepository;
import com.epam.esm.restadvanced.repository.UserRepository;
import com.epam.esm.restadvanced.repository.entity.User;
import com.epam.esm.restadvanced.repository.mapper.UserMapper;
import com.epam.esm.restadvanced.service.exception.UserDuplicateException;
import com.epam.esm.restadvanced.service.exception.UserNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.core.env.Environment;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.Statement;
import java.sql.Timestamp;
import java.time.LocalDateTime;

@Repository
@PropertySources({
        @PropertySource(name="sqlStatements", value = "classpath:sql-statements.properties"),
        @PropertySource(name="stringValues", value = "classpath:string-values.properties")
})
public class UserRepositoryImpl implements UserRepository {
    private final JdbcTemplate jdbcTemplate;
    private final Environment environment;
    private final PageRepository<User> userPageRepository;

    @Autowired
    public UserRepositoryImpl(JdbcTemplate jdbcTemplate, Environment environment,
                              PageRepository<User> userPageRepository) {
        this.jdbcTemplate = jdbcTemplate;
        this.environment = environment;
        this.userPageRepository = userPageRepository;
    }

    @Override
    public User saveUser(User user) {
        try {
            KeyHolder keyHolder = new GeneratedKeyHolder();
            jdbcTemplate.update(connection -> {
                PreparedStatement ps = connection.prepareStatement(environment.getProperty("user.insert"),
                        Statement.RETURN_GENERATED_KEYS);
                ps.setString(1, user.getFullName());
                ps.setString(2, user.getEmail());
                ps.setString(3, user.getPassword());
                ps.setObject(4, Timestamp.valueOf(LocalDateTime.now()));
                ps.setObject(5, user.getLastUpdatedDate());
                return ps;
            }, keyHolder);
            user.setId(keyHolder.getKey().longValue());
            return user;
        } catch(DuplicateKeyException ex) {
            throw new UserDuplicateException(String.format(environment.getProperty("user.message.duplicate"),
                    user.getFullName()), ex);
        }
    }

    @Override
    public Page<User> findAll(Pageable pageRequest) {
        Page<User> userPage = userPageRepository.fetchPage(environment.getProperty("user.find.all"),
                environment.getProperty("user.count.all"), new UserMapper(), pageRequest);
        if(userPage.getTotalElements() == 0) {
            throw new UserNotFoundException(environment.getProperty("user.message.allNotFound"));
        }
        return userPage;
    }

    @Override
    public User findUserById(long id) {
        try {
            return jdbcTemplate.queryForObject(environment.getProperty("user.find.byId"), new UserMapper(), id);
        } catch(EmptyResultDataAccessException ex) {
            throw new UserNotFoundException(String.format(environment.getProperty("user.message.notFound"), id), ex);
        }
    }

    @Override
    public User deleteUserById(long userId) {
        try {
            User existedGiftCertificate = findUserById(userId);
            jdbcTemplate.update(environment.getProperty("user.delete.byId"), userId);
            return existedGiftCertificate;
        } catch(EmptyResultDataAccessException ex) {
            throw new UserNotFoundException(String.format(environment.getProperty("user.message.notFound"),
                    userId), ex);
        }
    }

    @Override
    public void deleteAll() {
        jdbcTemplate.update(environment.getProperty("user.delete.all"));

        /** unset when using h2 embedded database */
        jdbcTemplate.update(environment.getProperty("user.reset.autoincrement"));
    }
}
