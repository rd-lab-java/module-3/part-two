package com.epam.esm.restadvanced.repository.mapper;

import com.epam.esm.restadvanced.repository.TagRepository;
import com.epam.esm.restadvanced.repository.entity.GiftCertificate;
import com.epam.esm.restadvanced.repository.entity.Tag;
import com.epam.esm.restadvanced.service.exception.GiftCertificateNotFoundException;
import org.springframework.core.env.Environment;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class GiftCertificateAndTagsExtractor implements ResultSetExtractor<List<GiftCertificate>> {
	private final TagRepository tagRepository;
	private final Environment environment;
	
	public GiftCertificateAndTagsExtractor(TagRepository tagRepository, Environment environment) {
		super();
		this.tagRepository = tagRepository;
		this.environment = environment;
	}
	
	@Override
	public List<GiftCertificate> extractData(ResultSet rs) throws SQLException, DataAccessException {
		List<GiftCertificate> giftCertificates = null;
		while(rs.next()) {
			GiftCertificate giftCertificate = new GiftCertificate();
			giftCertificate.setId(rs.getLong("id"));
			giftCertificate.setName(rs.getString("name"));
			giftCertificate.setDescription(rs.getString("description"));
			giftCertificate.setPrice(rs.getDouble("price"));
			giftCertificate.setDuration(rs.getInt("duration"));
			giftCertificate.setCreatedDate(rs.getTimestamp("create_date"));
			giftCertificate.setLastUpdatedDate(rs.getTimestamp("last_update_date"));

			List<Tag> tags = tagRepository.findTagsByGiftCertificateId(giftCertificate.getId());
			giftCertificate.setTags(tags);

			if(giftCertificates == null)
				giftCertificates = new ArrayList<>();
			
			giftCertificates.add(giftCertificate);
		}

		if(giftCertificates == null)
			throw new GiftCertificateNotFoundException(
					environment.getProperty("giftCertificate.message.searchNotFound"));
		return giftCertificates;
	}
}
