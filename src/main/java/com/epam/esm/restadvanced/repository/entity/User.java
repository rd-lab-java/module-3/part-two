package com.epam.esm.restadvanced.repository.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
public class User {
    private long id;
    private String fullName;
    private String email;
    private String password;
    private Date createdDate;
    private Date lastUpdatedDate;

    public User() {
        createdDate = new Date();
    }

    public User(long id, String fullName, String email, String password) {
        this.id = id;
        this.fullName = fullName;
        this.email = email;
        this.password = password;
        createdDate = new Date();
    }
}
