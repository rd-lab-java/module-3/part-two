package com.epam.esm.restadvanced.repository;

import com.epam.esm.restadvanced.common.api.model.Page;
import com.epam.esm.restadvanced.common.api.model.PageRequest;
import com.epam.esm.restadvanced.common.api.model.Pageable;
import com.epam.esm.restadvanced.repository.entity.GiftCertificate;
import com.epam.esm.restadvanced.repository.entity.Tag;

/**
 * Gift Certificate repository interface 
 * imposes basic operations that working with data source.
 * @author yusuf
 *
 */
public interface GiftCertificateRepository {
	GiftCertificate saveGiftCertificate(GiftCertificate giftCertificate);
	Page<GiftCertificate> findAll(Pageable pageRequest);
	GiftCertificate findGiftCertificateById(long id);
	GiftCertificate updateGiftCertificateById(long id, GiftCertificate giftCertificate);
	GiftCertificate deleteGiftCertificateById(long id);
	boolean saveGiftCertificateAndTag(GiftCertificate giftCertificate, Tag tag);
	Page<GiftCertificate> findSortedGiftCertificatesAndTagsByTagNames(String tagNames, int tagNamesCount,
																	  String sort, Pageable pageRequest);
	Page<GiftCertificate> findGiftCertificatesAndTagsByTagNames(String tagNames, int tagNamesCount,
																Pageable pageRequest);
	Page<GiftCertificate> findSortedGiftCertificatesAndTagsByPartOfName(String partOfName, String sort,
																		Pageable pageRequest);
	Page<GiftCertificate> findGiftCertificatesAndTagsByPartOfName(String partOfName, Pageable pageRequest);
	Page<GiftCertificate> findSortedGiftCertificatesAndTagsByPartOfDesc(String partOfDesc, String sort,
																		Pageable pageRequest);
	Page<GiftCertificate> findGiftCertificatesAndTagsByPartOfDesc(String partOfDesc, Pageable pageRequest);
	Page<GiftCertificate> findGiftCertificatesByTagId(long tagId, Pageable pageRequest);
	/* tear down process*/
	void deleteAll();
}
