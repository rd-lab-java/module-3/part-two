package com.epam.esm.restadvanced.repository.entity;


import com.epam.esm.restadvanced.service.exception.GiftCertificateNotModifiedException;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

@Data
public class GiftCertificate {
    private long id;
    private String name;
    private String description;
    private double price;
    private int duration;
    private Date createdDate;
    private Date lastUpdatedDate;
	
	private Collection<Tag> tags = new ArrayList<>();

	public GiftCertificate(String name, String description, double price, int duration) {
		this.name = name;
		this.description = description;
		this.price = price;
		this.duration = duration;
		createdDate = new Date();
	}
	
	public GiftCertificate() {
		createdDate = new Date();
	}
}
