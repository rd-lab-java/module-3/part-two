package com.epam.esm.restadvanced.repository.mapper;

import com.epam.esm.restadvanced.repository.GiftCertificateRepository;
import com.epam.esm.restadvanced.repository.UserRepository;
import com.epam.esm.restadvanced.repository.entity.GiftCertificate;
import com.epam.esm.restadvanced.repository.entity.Order;
import com.epam.esm.restadvanced.repository.entity.User;
import com.epam.esm.restadvanced.service.exception.OrderNotFoundException;
import org.springframework.core.env.Environment;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.PreparedStatementSetter;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowCallbackHandler;

import java.security.acl.Owner;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class OrderExtractor implements ResultSetExtractor<List<Order>> {
    private final UserRepository userRepository;
    private final GiftCertificateRepository giftCertificateRepository;
    private final Environment environment;

    public OrderExtractor(UserRepository userRepository, GiftCertificateRepository giftCertificateRepository,
                          Environment environment) {
        this.userRepository = userRepository;
        this.giftCertificateRepository = giftCertificateRepository;
        this.environment = environment;
    }

    @Override
    public List<Order> extractData(ResultSet rs) throws SQLException, DataAccessException {
        List<Order> orders = null;
        Map<Long, User> owners = new HashMap<>();
        Map<Long, GiftCertificate> giftCertificates = new HashMap<>();
        while(rs.next()) {
            Order order = new Order();
            order.setId(rs.getLong("id"));
            order.setTotalCost(rs.getDouble("total_cost"));
            order.setQuantity(rs.getInt("quantity"));
            order.setOrderedDate(rs.getTimestamp("ordered_date"));
            order.setLastUpdatedDate(rs.getTimestamp("last_updated_date"));
            order.setStatus(rs.getString("status"));

            long ownerId = rs.getLong("owner_id");
            if(!owners.containsKey(ownerId)) {
                User owner = userRepository.findUserById(ownerId);
                owners.put(ownerId, owner);
            }
            order.setOwner(owners.get(ownerId));

            long giftCertificateId = rs.getLong("gift_certificate_id");
            if(!giftCertificates.containsKey(giftCertificateId)) {
                GiftCertificate giftCertificate = giftCertificateRepository.findGiftCertificateById(giftCertificateId);
                giftCertificates.put(giftCertificateId, giftCertificate);
            }
            order.setGiftCertificate(giftCertificates.get(giftCertificateId));

            if(orders == null)
                orders = new ArrayList<>();
            orders.add(order);
        }
        return orders;
    }
}
