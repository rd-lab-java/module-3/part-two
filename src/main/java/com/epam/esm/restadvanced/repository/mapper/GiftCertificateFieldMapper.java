package com.epam.esm.restadvanced.repository.mapper;

import com.epam.esm.restadvanced.repository.entity.GiftCertificate;

public interface GiftCertificateFieldMapper {
    String retrieveChangedFields(GiftCertificate original, GiftCertificate another);
}
