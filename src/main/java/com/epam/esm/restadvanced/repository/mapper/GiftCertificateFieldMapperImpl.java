package com.epam.esm.restadvanced.repository.mapper;

import com.epam.esm.restadvanced.repository.entity.GiftCertificate;
import org.springframework.stereotype.Component;

@Component
public class GiftCertificateFieldMapperImpl implements GiftCertificateFieldMapper {
    @Override
    public String retrieveChangedFields(GiftCertificate original, GiftCertificate another) {
        StringBuilder part = new StringBuilder();

        if(!original.getName().equals(another.getName())) {
            part.append(String.format("name='%s',", another.getName()));
        }

        if(original.getDescription() != null && !original.getDescription().equals(another.getDescription())) {
            part.append(String.format("description='%s',", another.getDescription()));
        }

        if(original.getPrice() != another.getPrice()) {
            part.append(String.format("price='%s'," , another.getPrice()));
        }

        if(original.getDuration() != another.getDuration()) {
            part.append(String.format("duration='%s',", another.getDuration()));
        }

        return part.toString();
    }
}
