package com.epam.esm.restadvanced.repository.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.Collection;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(value = {"giftCertificates"}, allowSetters = true)
public class Tag {
    private long id;
    private String name;
    private Collection<GiftCertificate> giftCertificates = new ArrayList<>();
}
