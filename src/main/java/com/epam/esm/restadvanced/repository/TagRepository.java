package com.epam.esm.restadvanced.repository;

import com.epam.esm.restadvanced.common.api.model.Page;
import com.epam.esm.restadvanced.common.api.model.PageRequest;
import com.epam.esm.restadvanced.common.api.model.Pageable;
import com.epam.esm.restadvanced.repository.entity.Tag;

import java.util.List;

/**
 * Tag repository interface
 * imposes basic CRUD operations 
 * that working directly with data source
 * 
 * @author yusuf
 *
 */
public interface TagRepository {
	Tag saveTag(Tag tag);
	Tag findTagByName(String name);
	Page<Tag> findTagsByGiftCertificateId(long giftCertId, Pageable pageRequest);
	List<Tag> findTagsByGiftCertificateId(long giftCertId);
	Page<Tag> findAll(Pageable pageRequest);
	Tag deleteTagById(long id);
	Tag findTagById(long id);
	Page<Tag> findMostWidelyUsedTags(Pageable pageRequest);
	List<Tag> findTopMostWidelyUsedTags(int top);

	/** tear down process */
	void deleteAll();
}
