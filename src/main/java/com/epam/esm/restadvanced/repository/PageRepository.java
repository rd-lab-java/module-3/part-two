package com.epam.esm.restadvanced.repository;

import com.epam.esm.restadvanced.common.api.model.Page;
import com.epam.esm.restadvanced.common.api.model.PageRequest;
import com.epam.esm.restadvanced.common.api.model.Pageable;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;

import java.util.List;

public interface PageRepository<T> {
    Page<T> fetchPage(String sqlQuery, String countQuery, RowMapper<T> rowMapper, Pageable pageRequest);
    Page<T> fetchPage(String sqlQuery, String countQuery, ResultSetExtractor<List<T>> extractor, Pageable pageRequest);
}
