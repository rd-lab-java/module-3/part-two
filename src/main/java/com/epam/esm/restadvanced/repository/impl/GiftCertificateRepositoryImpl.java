package com.epam.esm.restadvanced.repository.impl;


import com.epam.esm.restadvanced.common.api.model.Page;
import com.epam.esm.restadvanced.common.api.model.Pageable;
import com.epam.esm.restadvanced.repository.GiftCertificateRepository;
import com.epam.esm.restadvanced.repository.PageRepository;
import com.epam.esm.restadvanced.repository.TagRepository;
import com.epam.esm.restadvanced.repository.entity.GiftCertificate;
import com.epam.esm.restadvanced.repository.entity.Tag;
import com.epam.esm.restadvanced.repository.mapper.GiftCertificateAndTagsExtractor;
import com.epam.esm.restadvanced.repository.mapper.GiftCertificateFieldMapper;
import com.epam.esm.restadvanced.repository.mapper.GiftCertificateMapper;
import com.epam.esm.restadvanced.service.exception.GiftCertificateDuplicateException;
import com.epam.esm.restadvanced.service.exception.GiftCertificateNotFoundException;
import com.epam.esm.restadvanced.service.exception.GiftCertificateNotModifiedException;
import com.epam.esm.restadvanced.service.exception.TagDuplicateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.core.env.Environment;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.Statement;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.function.Predicate;

/**
 * Gift Certificate repository implementation
 * contains implementation with data source 
 * and has jdbc template {@link JdbcTemplate} dependency and
 * tag repository {@link TagRepository} dependency
 * 
 * @author yusuf
 *
 */
@Repository
@PropertySources({
		@PropertySource(name="sqlStatements", value = "classpath:sql-statements.properties"),
		@PropertySource(name="stringValues", value = "classpath:string-values.properties")
})
public class GiftCertificateRepositoryImpl implements GiftCertificateRepository {

	private final Environment environment;
	private final JdbcTemplate jdbcTemplate;
	private final TagRepository tagRepository;
	private final PageRepository<GiftCertificate> pageRepository;
	private final GiftCertificateFieldMapper giftCertificateFieldMapper;
	
	@Autowired
	public GiftCertificateRepositoryImpl(Environment environment, JdbcTemplate jdbcTemplate,
										 TagRepository tagRepository, PageRepository<GiftCertificate> pageRepository,
										 GiftCertificateFieldMapper giftCertificateFieldMapper) {
		super();
		this.environment = environment;
		this.jdbcTemplate = jdbcTemplate;
		this.tagRepository = tagRepository;
		this.pageRepository = pageRepository;
		this.giftCertificateFieldMapper = giftCertificateFieldMapper;
	}


	@Override
	public GiftCertificate saveGiftCertificate(GiftCertificate giftCertificate) {
		try {
			KeyHolder keyHolder = new GeneratedKeyHolder();
			jdbcTemplate.update(connection -> {
				PreparedStatement ps = connection.prepareStatement(environment.getProperty("giftCertificate.insert"),
						Statement.RETURN_GENERATED_KEYS);
				
				ps.setString(1, giftCertificate.getName());
				ps.setString(2, giftCertificate.getDescription());
				ps.setDouble(3, giftCertificate.getPrice());
				ps.setInt(4, giftCertificate.getDuration());
				ps.setObject(5, Timestamp.valueOf(LocalDateTime.now()));
				ps.setObject(6, giftCertificate.getLastUpdatedDate());
				return ps;
			}, keyHolder);
			giftCertificate.setId(keyHolder.getKey().longValue());
			saveGiftCertificateAndTags(giftCertificate);
			return giftCertificate;
		} catch(DuplicateKeyException ex) {
			throw new GiftCertificateDuplicateException(
					String.format(environment.getProperty("giftCertificate.message.duplicate"),
					giftCertificate.getName()), ex);
		}
	}
	
	
	/**
	 * save if the there are tags with certificate
	 * save only new tags for the certificate.
	 * 
	 * @param giftCertificate
	 * @return true if successfully save 
	 */
	private boolean saveGiftCertificateAndTags(GiftCertificate giftCertificate) {
		long savedItemCount = giftCertificate.getTags().stream().filter(new Predicate<Tag>() {
			@Override
			public boolean test(Tag tag) {
				tag.setId(saveAndGetTag(tag).getId());
				return saveGiftCertificateAndTag(giftCertificate, tag);
			}
		}).count();
		return savedItemCount > 0;
	}
	
	/**
	 * save new tag or get existing one if occurs overwriting 
	 * 
	 * @param tag
	 * @return saved tag, or existing tag
	 */
	private Tag saveAndGetTag(Tag tag) {
		try {
			return tagRepository.saveTag(tag);
		} catch(TagDuplicateException ex) {
			return tagRepository.findTagByName(tag.getName());
		}
	}

	@Override
	public Page<GiftCertificate> findAll(Pageable pageRequest) {
		Page<GiftCertificate> giftCertificatePage = pageRepository.fetchPage(
				environment.getProperty("giftCertificate.find.all"),
				environment.getProperty("giftCertificate.count.all"),
				new GiftCertificateMapper(), pageRequest);
		if(giftCertificatePage.getTotalElements() == 0) {
			throw new GiftCertificateNotFoundException(environment.getProperty("giftCertificate.message.allNotFound"));
		}
		return giftCertificatePage;
	}

	@Override
	public GiftCertificate findGiftCertificateById(long id) {
		try {
			GiftCertificate giftCertificate = jdbcTemplate.queryForObject(
					environment.getProperty("giftCertificate.find.byId"), new GiftCertificateMapper(), id);
			return giftCertificate;
		} catch(EmptyResultDataAccessException ex) {
			throw new GiftCertificateNotFoundException(
					String.format(environment.getProperty("giftCertificate.message.notFound"), id), ex);
		}
	}

	@Override
	public GiftCertificate updateGiftCertificateById(long id, GiftCertificate giftCertificate) {
		try {
			GiftCertificate existedGiftCertificate = jdbcTemplate.queryForObject(
					environment.getProperty("giftCertificate.find.byId"), new GiftCertificateMapper(), id);
			
			giftCertificate.setId(id);
			giftCertificate.setLastUpdatedDate(new Date());
			giftCertificate.setCreatedDate(existedGiftCertificate.getCreatedDate());
			
			String updatingFields = giftCertificateFieldMapper.retrieveChangedFields(
					existedGiftCertificate, giftCertificate);
			boolean isNewTagAdded = saveGiftCertificateAndTags(giftCertificate);
			
			if(updatingFields.length() == 0 && !isNewTagAdded) {
				throw new GiftCertificateNotModifiedException(
						String.format(environment.getProperty("giftCertificate.message.notModified"), id));
			}
			
			updateFieldsById(id, updatingFields);
			return giftCertificate;
		} catch(EmptyResultDataAccessException ex) {
			throw new GiftCertificateNotFoundException(
					String.format(environment.getProperty("giftCertificate.message.notFound"), id), ex);
		}
	}

	private void updateFieldsById(long id, String fields) {
		jdbcTemplate.update(String.format(environment.getProperty("giftCertificate.update.byId"), fields),
				Timestamp.valueOf(LocalDateTime.now()), id);
	}

	@Override
	public GiftCertificate deleteGiftCertificateById(long id) {
		try {
			GiftCertificate existedGiftCertificate = jdbcTemplate.queryForObject(
					environment.getProperty("giftCertificate.find.byId"), new GiftCertificateMapper(), id);
			jdbcTemplate.update(environment.getProperty("giftCertificate.delete.byId"), id);
			return existedGiftCertificate;
		} catch(EmptyResultDataAccessException ex) {
			throw new GiftCertificateNotFoundException(
					String.format(environment.getProperty("giftCertificate.message.notFound"), id), ex);
		}
	}

	@Override
	public boolean saveGiftCertificateAndTag(GiftCertificate giftCertificate, Tag tag) {
		try {
			return jdbcTemplate.update(environment.getProperty("giftCertificate.insert.giftCertAndTag"),
					giftCertificate.getId(), tag.getId()) > 0;
		} catch (DuplicateKeyException exception) {
			return false;
		}
	}

	@Override
	public Page<GiftCertificate> findSortedGiftCertificatesAndTagsByTagNames(String tagNames, int tagNamesCount,
																			 String sort, Pageable pageRequest) {
		return pageRepository.fetchPage(
				String.format(environment.getProperty("giftCertificate.find.sortedGiftCertificatesAndTagsByTagNames"),
						tagNames, tagNamesCount, sort),
				String.format(environment.getProperty("giftCertificate.count.giftCertificatesAndTagsByTagNames"),
						tagNames, tagNamesCount),
				new GiftCertificateAndTagsExtractor(tagRepository, environment), pageRequest);
	}

	@Override
	public Page<GiftCertificate> findGiftCertificatesAndTagsByTagNames(String tagNames, int tagNamesCount,
																	   Pageable pageRequest) {
		return pageRepository.fetchPage(
				String.format(environment.getProperty("giftCertificate.find.giftCertificatesAndTagsByTagNames"),
						tagNames, tagNamesCount),
				String.format(environment.getProperty("giftCertificate.count.giftCertificatesAndTagsByTagNames"),
						tagNames, tagNamesCount),
				new GiftCertificateAndTagsExtractor(tagRepository, environment), pageRequest);
	}

	@Override
	public Page<GiftCertificate> findSortedGiftCertificatesAndTagsByPartOfName(String partOfName, String sort,
																			   Pageable pageRequest) {
		String arg = "%" + partOfName + "%";
		return pageRepository.fetchPage(
				String.format(environment.getProperty("giftCertificate.find.sortedGiftCertificatesAndTagsByPartOfName"),
						arg, sort),
				String.format(environment.getProperty("giftCertificate.count.giftCertificatesAndTagsByPartOfName"), arg),
				new GiftCertificateAndTagsExtractor(tagRepository, environment), pageRequest);
	}

	@Override
	public Page<GiftCertificate> findGiftCertificatesAndTagsByPartOfName(String partOfName, Pageable pageRequest) {
		String arg = "%" + partOfName + "%";
		return pageRepository.fetchPage(
				String.format(environment.getProperty("giftCertificate.find.giftCertificatesAndTagsByPartOfName"), arg),
				String.format(environment.getProperty("giftCertificate.count.giftCertificatesAndTagsByPartOfName"), arg),
				new GiftCertificateAndTagsExtractor(tagRepository, environment), pageRequest);
	}

	@Override
	public Page<GiftCertificate> findSortedGiftCertificatesAndTagsByPartOfDesc(String partOfDesc, String sort,
																			   Pageable pageRequest) {
		String arg = "%" + partOfDesc + "%";
		return pageRepository.fetchPage(
				String.format(environment.getProperty("giftCertificate.find.sortedGiftCertificatesAndTagsByPartOfDesc"),
						arg, sort),
				String.format(environment.getProperty("giftCertificate.count.giftCertificatesAndTagsByPartOfDesc"), arg),
				new GiftCertificateAndTagsExtractor(tagRepository, environment), pageRequest);
	}

	@Override
	public Page<GiftCertificate> findGiftCertificatesAndTagsByPartOfDesc(String partOfDesc, Pageable pageRequest) {
		String arg = "%" +partOfDesc + "%";
		return pageRepository.fetchPage(
				String.format(environment.getProperty("giftCertificate.find.giftCertificatesAndTagsByPartOfDesc"),arg),
				String.format(environment.getProperty("giftCertificate.count.giftCertificatesAndTagsByPartOfDesc"), arg),
				new GiftCertificateAndTagsExtractor(tagRepository, environment), pageRequest);
	}

	@Override
	public Page<GiftCertificate> findGiftCertificatesByTagId(long tagId, Pageable pageRequest) {
		Page<GiftCertificate> giftCertificatePage = pageRepository.fetchPage(
				String.format(environment.getProperty("giftCertificate.find.tagGiftCertificates"), tagId),
				String.format(environment.getProperty("giftCertificate.count.tagGiftCertificates"), tagId),
				new GiftCertificateMapper(), pageRequest);
		if(giftCertificatePage.getTotalElements() == 0) {
			throw new GiftCertificateNotFoundException(
					String.format(environment.getProperty("giftCertificate.message.notFoundByTagId"), tagId));
		}
		return giftCertificatePage;
	}


	@Override
	public void deleteAll() {
		jdbcTemplate.update(environment.getProperty("giftCertificate.delete.all"));

		/** unset when using embedded h2 database */
		jdbcTemplate.update(environment.getProperty("giftCertificate.reset.autoincrement"));
		tagRepository.deleteAll();
	}
}
