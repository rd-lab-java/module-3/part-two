package com.epam.esm.restadvanced.repository;

import com.epam.esm.restadvanced.common.api.model.Page;
import com.epam.esm.restadvanced.common.api.model.PageRequest;
import com.epam.esm.restadvanced.common.api.model.Pageable;
import com.epam.esm.restadvanced.repository.entity.User;

public interface UserRepository {
    User saveUser(User user);
    Page<User> findAll(Pageable pageRequest);
    User findUserById(long id);
    User deleteUserById(long userId);

    void deleteAll();
}
