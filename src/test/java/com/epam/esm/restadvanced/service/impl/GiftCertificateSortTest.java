package com.epam.esm.restadvanced.service.impl;

import com.epam.esm.restadvanced.service.SortingPropertyHandler;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(SpringExtension.class)
class GiftCertificateSortTest {
    SortingPropertyHandler sortingPropertyHandler;

    @BeforeEach
    public void init() {
        sortingPropertyHandler = new GiftCertificateSort();
    }

    @Test
    @DisplayName("Get sorting date property handler")
    public void shouldGetSortingPropertyHandler() {
        String[] sortBy = {"date"};
        String sortingProperty = sortingPropertyHandler.convert(sortBy);
        assertEquals("gift_certificate.create_date", sortingProperty);
    }

    @Test
    @DisplayName("Get sorting name ASC property handler")
    public void shouldGetSortingNameASCPropertyHandler() {
        String[] sortBy = {"name;ASC"};
        String sortingProperty = sortingPropertyHandler.convert(sortBy);
        assertEquals("gift_certificate.name ASC", sortingProperty);
    }

    @Test
    @DisplayName("Get sorting name DESC property handler")
    public void shouldGetSortingNameDESCPropertyHandler() {
        String[] sortBy = {"name;DESC"};
        String sortingProperty = sortingPropertyHandler.convert(sortBy);
        assertEquals("gift_certificate.name DESC", sortingProperty);
    }

    @Test
    @DisplayName("Get sorting name ASC and date property handler")
    public void shouldGetSortingNameASCAndDatePropertyHandler() {
        String[] sortBy = {"name;ASC", "date"};
        String sortingProperty = sortingPropertyHandler.convert(sortBy);
        assertEquals("gift_certificate.name ASC,gift_certificate.create_date", sortingProperty);
    }

    @Test
    @DisplayName("Get sorting name DESC and date property handler")
    public void shouldGetSortingNameDescAndDatePropertyHandler() {
        String[] sortBy = {"name;DESC", "date"};
        String sortingProperty = sortingPropertyHandler.convert(sortBy);
        assertEquals("gift_certificate.name DESC,gift_certificate.create_date", sortingProperty);
    }

    @Test
    @DisplayName("Get sorting empty property handler")
    public void shouldGetSortingEmptyPropertyHandler() {
        String[] sortBy = {};
        String sortingProperty = sortingPropertyHandler.convert(sortBy);
        assertEquals("", sortingProperty);
    }
}