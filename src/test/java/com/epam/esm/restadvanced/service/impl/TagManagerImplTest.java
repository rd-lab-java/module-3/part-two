package com.epam.esm.restadvanced.service.impl;

import com.epam.esm.restadvanced.common.api.model.Page;
import com.epam.esm.restadvanced.common.api.model.PageRequest;
import com.epam.esm.restadvanced.config.TestH2Config;
import com.epam.esm.restadvanced.repository.GiftCertificateRepository;
import com.epam.esm.restadvanced.repository.OrderRepository;
import com.epam.esm.restadvanced.repository.TagRepository;
import com.epam.esm.restadvanced.repository.UserRepository;
import com.epam.esm.restadvanced.repository.entity.GiftCertificate;
import com.epam.esm.restadvanced.repository.entity.Order;
import com.epam.esm.restadvanced.repository.entity.Tag;
import com.epam.esm.restadvanced.repository.entity.User;
import com.epam.esm.restadvanced.service.*;
import com.epam.esm.restadvanced.service.exception.TagDuplicateException;
import com.epam.esm.restadvanced.service.exception.TagNotFoundException;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
@ActiveProfiles("test-h2")
@ContextConfiguration(classes =  {TestH2Config.class})
@PropertySource(name="stringValues", value = "classpath:string-values.properties")
class TagManagerImplTest {
    private final static int DEFAULT_PAGE_NUMBER = 1;
    private final static int DEFAULT_PAGE_SIZE = 3;

    TagManager tagManager;
    @Autowired
    TagRepository tagRepository;
    @Autowired
    Environment environment;


    @BeforeEach
    public void init() {
        tagManager = new TagManagerImpl(tagRepository);
        Tag tag1 = new Tag();
        tag1.setName("new");
        tagManager.saveTag(tag1);

        Tag tag2 = new Tag();
        tag2.setName("old");
        tagManager.saveTag(tag2);

        Tag tag3 = new Tag();
        tag3.setName("lovely");
        tagManager.saveTag(tag3);

        Tag tag4 = new Tag();
        tag4.setName("annoying");
        tagManager.saveTag(tag4);

        Tag tag5 = new Tag();
        tag5.setName("disgusting");
        tagManager.saveTag(tag5);
    }

    @AfterEach
    public void tearDown() {
        tagRepository.deleteAll();
    }

    @Test
    public void shouldSaveTag() {
        Tag tag = new Tag();
        tag.setName("tashkent");
        Tag saveTag = tagManager.saveTag(tag);
        assertNotNull(saveTag);
        assertEquals(tag.getName(), saveTag.getName());
    }

    @Test
    public void shouldThrowExceptionWhenSaveTag() {
        Tag tag = new Tag();
        tag.setName("new");
        String errorMessage = String.format(environment.getProperty("tag.message.duplicate"), tag.getName());
        TagDuplicateException exception = assertThrows(TagDuplicateException.class, () -> {
            tagManager.saveTag(tag);
        });
        assertEquals(errorMessage, exception.getMessage());
    }

    @Test
    public void shouldFindTagById() {
        long tagId = 2L;
        Tag tag = tagManager.findTagById(tagId);
        assertNotNull(tag);
        assertEquals(tagId, tag.getId());
    }

    @Test
    public void shouldThrowExceptionWhenFindTag() {
        long tagId = 6L;
        String errorMessage = String.format(environment.getProperty("tag.message.notFoundById"), tagId);
        TagNotFoundException exception = assertThrows(TagNotFoundException.class, ()-> {
            tagManager.findTagById(tagId);
        });
        assertEquals(errorMessage, exception.getMessage());
    }

    @Test
    public void shouldDeleteTag() {
        long tagId = 2L;
        Tag deletedTag = tagManager.deleteTagById(tagId);
        assertEquals(tagId, deletedTag.getId());
        String errorMessage =String.format(environment.getProperty("tag.message.notFoundById"), tagId);
        TagNotFoundException ex = assertThrows(TagNotFoundException.class, ()-> {
            tagManager.deleteTagById(tagId);
        });
        assertEquals(errorMessage, ex.getMessage());
    }

    @Test
    public void shouldThrowExceptionWhenDeleteTag() {
        long tagId = 6L;
        String errorMessage =String.format(environment.getProperty("tag.message.notFoundById"), tagId);
        TagNotFoundException ex = assertThrows(TagNotFoundException.class, ()-> {
            tagManager.deleteTagById(tagId);
        });
        assertEquals(errorMessage, ex.getMessage());
    }

    @Test
    public void shouldFindAll() {
        Page<Tag> tagPage = tagManager.findAll(new PageRequest(DEFAULT_PAGE_NUMBER, DEFAULT_PAGE_SIZE));
        assertNotNull(tagPage);
        assertEquals(5, tagPage.getTotalElements());
        assertEquals(2, tagPage.getTotalPages());
        assertEquals(1, tagPage.getNumber());
        assertEquals(3, tagPage.getNumberOfElements());

        assertTrue(tagPage.hasContent());
        assertTrue(tagPage.hasNext());
        assertFalse(tagPage.hasPrevious());

        Page<Tag> tagPage2 = tagManager.findAll(tagPage.getNextPageable());
        assertEquals(5, tagPage2.getTotalElements());
        assertEquals(2, tagPage2.getTotalPages());
        assertEquals(2, tagPage2.getNumber());
        assertEquals(2, tagPage2.getNumberOfElements());

        assertTrue(tagPage2.hasContent());
        assertFalse(tagPage2.hasNext());
        assertTrue(tagPage2.hasPrevious());
    }

    @Test
    public void shouldThrowWhenFindAllGiftCertificates() {
        tagRepository.deleteAll();
        String errorMessage = environment.getProperty("tag.message.allNotFound");
        TagNotFoundException ex = assertThrows(TagNotFoundException.class, () -> {
            tagManager.findAll(new PageRequest(DEFAULT_PAGE_NUMBER, DEFAULT_PAGE_SIZE));
        });
        assertEquals(errorMessage, ex.getMessage());
    }

    @Nested
    public class TagAndGiftCertificate {
        @Autowired
        GiftCertificateRepository giftCertificateRepository;
        @Mock
        GiftCertificateSort giftCertificateSort;
        @Mock
        TagNamePropertyHandler tagNamePropertyHandler;
        GiftCertificateManager giftCertificateManager;

        @BeforeEach
        public void init() {
            giftCertificateManager = new GiftCertificateManagerImpl(
                    giftCertificateSort, giftCertificateRepository, tagNamePropertyHandler, environment);

            GiftCertificate giftCertificate = new GiftCertificate();
            giftCertificate.setName("GC-one");
            giftCertificate.setDescription("gift certificate one");
            giftCertificate.setPrice(10.5);
            giftCertificate.setDuration(12);
            Tag tag1 = new Tag();
            tag1.setName("new");

            Tag tag2 = new Tag();
            tag2.setName("old");

            Tag tag3 = new Tag();
            tag3.setName("lonely");
            List<Tag> tags = Arrays.asList(tag1, tag2, tag3);
            giftCertificate.setTags(tags);

            giftCertificateManager.saveGiftCertificate(giftCertificate);
        }

        @AfterEach
        public void tearDown() {
            giftCertificateRepository.deleteAll();
        }

        @Test
        public void shouldFindTagsByGiftCertificateId() {
            long giftCertificateId = 1L;
            Page<Tag> tagPage = tagManager.findTagsByGiftCertificateId(
                    giftCertificateId, new PageRequest(DEFAULT_PAGE_NUMBER, DEFAULT_PAGE_SIZE));
            assertNotNull(tagPage);
            assertEquals(3, tagPage.getTotalElements());
            assertEquals(1, tagPage.getTotalPages());
            assertEquals(3, tagPage.getTotalElements());
            assertEquals(1, tagPage.getNumber());
        }

        @Test
        public void shouldThrowExceptionWhenFindTags() {
            long giftCertificateId = 5L;
            String errorMessage = String.format(
                    environment.getProperty("tag.message.notFoundByGiftCertificateId"), giftCertificateId);
            TagNotFoundException tagNotFoundException = assertThrows(TagNotFoundException.class, ()-> {
                tagManager.findTagsByGiftCertificateId(
                        giftCertificateId, new PageRequest(DEFAULT_PAGE_NUMBER, DEFAULT_PAGE_SIZE));
            });
            assertEquals(errorMessage, tagNotFoundException.getMessage());
        }
    }

    @Nested
    public class TagAndGiftCertificateAndOrderAndUser {
        @Autowired
        GiftCertificateRepository giftCertificateRepository;
        @Mock
        GiftCertificateSort giftCertificateSort;
        @Mock
        TagNamePropertyHandler tagNamePropertyHandler;
        GiftCertificateManager giftCertificateManager;
        @Autowired
        OrderRepository orderRepository;
        OrderManager orderManager;
        @Autowired
        UserRepository userRepository;
        UserManager userManager;
        @Mock
        CostCalculator costCalculator;

        @BeforeEach
        public void init() {
            giftCertificateManager = new GiftCertificateManagerImpl(
                    giftCertificateSort, giftCertificateRepository, tagNamePropertyHandler, environment);
            orderManager = new OrderManagerImpl(orderRepository, costCalculator);
            userManager = new UserManagerImpl(userRepository);

            User user1 = new User();
            user1.setFullName("User one");
            user1.setEmail("user_one@email.com");
            user1.setPassword("user_one");
            userManager.saveUser(user1);

            User user2 = new User();
            user2.setFullName("User two");
            user2.setPassword("user_two");
            user2.setEmail("user_two@email.com");
            userManager.saveUser(user2);

            GiftCertificate giftCertificate1 = new GiftCertificate();
            giftCertificate1.setName("GC-one");
            giftCertificate1.setDescription("gift certificate one");
            giftCertificate1.setPrice(10.5);
            giftCertificate1.setDuration(12);
            Tag tag1 = new Tag();
            tag1.setName("new");

            Tag tag2 = new Tag();
            tag2.setName("old");

            List<Tag> tags = Arrays.asList(tag1, tag2);
            giftCertificate1.setTags(tags);

            giftCertificateManager.saveGiftCertificate(giftCertificate1);

            GiftCertificate giftCertificate2 = new GiftCertificate();
            giftCertificate2.setName("GC-two");
            giftCertificate2.setDescription("gift certificate two");
            giftCertificate2.setPrice(12.5);
            giftCertificate2.setDuration(9);
            Tag tag4 = new Tag();
            tag4.setName("lonely");

            Tag tag5 = new Tag();
            tag5.setName("annoying");
            giftCertificate2.setTags(Arrays.asList(tag4, tag5));

            giftCertificateManager.saveGiftCertificate(giftCertificate2);

            GiftCertificate giftCertificate3 = new GiftCertificate();
            giftCertificate3.setName("GC-three");
            giftCertificate3.setDescription("gift certificate three");
            giftCertificate3.setPrice(25.5);
            giftCertificate3.setDuration(5);
            Tag tag6 = new Tag();
            tag6.setName("disgusting");

            giftCertificate3.setTags(Collections.singletonList(tag6));

            giftCertificateManager.saveGiftCertificate(giftCertificate3);

            when(costCalculator.totalCost(3, giftCertificate1.getPrice())).thenReturn(3 * giftCertificate1.getPrice());
            com.epam.esm.restadvanced.repository.entity.Order order1 = new Order(3, user1, giftCertificate1, "unpaid");
            orderManager.saveOrder(order1);

            when(costCalculator.totalCost(4, giftCertificate2.getPrice())).thenReturn(4 * giftCertificate2.getPrice());
            com.epam.esm.restadvanced.repository.entity.Order order2 = new Order(4, user2, giftCertificate2, "unpaid");
            orderManager.saveOrder(order2);

            when(costCalculator.totalCost(5, giftCertificate3.getPrice())).thenReturn(5 * giftCertificate3.getPrice());
            com.epam.esm.restadvanced.repository.entity.Order order3 = new Order(5, user1, giftCertificate3, "unpaid");
            orderManager.saveOrder(order3);
        }

        @AfterEach
        public void tearDown() {
            giftCertificateRepository.deleteAll();
            userRepository.deleteAll();
            orderRepository.deleteAll();
        }

        @Test
        public void shouldFindMostUsedTags() {
            Page<Tag> tagPage = tagManager.findMostUsedTags(
                    new PageRequest(DEFAULT_PAGE_NUMBER, DEFAULT_PAGE_SIZE));
            assertEquals(5, tagPage.getTotalElements());
            assertEquals(3, tagPage.getNumberOfElements());
            assertEquals(2, tagPage.getTotalPages());
            assertEquals(1, tagPage.getNumber());

            assertTrue(tagPage.hasContent());
            assertTrue(tagPage.hasNext());
            assertFalse(tagPage.hasPrevious());

            Page<Tag> tagPage2 = tagManager.findMostUsedTags(
                    tagPage.getNextPageable());
            assertEquals(5, tagPage2.getTotalElements());
            assertEquals(2, tagPage2.getNumberOfElements());
            assertEquals(2, tagPage2.getTotalPages());
            assertEquals(2, tagPage2.getNumber());

            assertTrue(tagPage2.hasContent());
            assertFalse(tagPage2.hasNext());
            assertTrue(tagPage2.hasPrevious());
        }

        @Test
        public void shouldThrowFindMostUsedTags() {
            tagRepository.deleteAll();
            String errorMessage = environment.getProperty("tag.message.allNotFound");
            TagNotFoundException ex = assertThrows(TagNotFoundException.class, () -> {
                tagManager.findMostUsedTags(new PageRequest(DEFAULT_PAGE_NUMBER, DEFAULT_PAGE_SIZE));
            });
            assertEquals(errorMessage, ex.getMessage());
        }

        @Test
        public void shouldFindTopMostUsedTags() {
            List<Tag> topTags = tagManager.findMostWidelyUsedTagsByTopLimit(2);
            assertEquals(2, topTags.size());
        }

        @Test
        public void shouldThrowExceptionWhenFindTopMostUsedTags() {
            tagRepository.deleteAll();
            String errorMessage = environment.getProperty("tag.message.allNotFound");
            TagNotFoundException ex = assertThrows(TagNotFoundException.class, ()-> {
                tagManager.findMostWidelyUsedTagsByTopLimit(2);
            });
            assertEquals(errorMessage, ex.getMessage());
        }
    }
}