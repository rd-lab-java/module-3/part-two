package com.epam.esm.restadvanced.service.impl;

import com.epam.esm.restadvanced.service.NamePropertyHandler;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
class TagNamePropertyHandlerTest {
    NamePropertyHandler namePropertyHandler;

    @BeforeEach
    public void init() {
        namePropertyHandler = new TagNamePropertyHandler();
    }

    @Test
    public void shouldRetrieveThreeCompatibleNames() {
        String[] names = {"new", "old", "big"};
        String compatibleNames = namePropertyHandler.retrieveCompatibleNames(names);
        assertEquals("'new','old','big'", compatibleNames);
    }

    @Test
    public void shouldRetrieveOneCompatibleColumnName() {
        String[] names = {"big"};
        String compatibleNames = namePropertyHandler.retrieveCompatibleNames(names);
        assertEquals("'big'", compatibleNames);
    }

    @Test
    public void shouldRetrieveEmptyCompatibleColumnName() {
        String[] names = {};
        String compatibleNames = namePropertyHandler.retrieveCompatibleNames(names);
        assertEquals("", compatibleNames);
    }
}