package com.epam.esm.restadvanced.service.impl;

import com.epam.esm.restadvanced.common.api.model.Page;
import com.epam.esm.restadvanced.common.api.model.PageRequest;
import com.epam.esm.restadvanced.config.TestH2Config;
import com.epam.esm.restadvanced.repository.GiftCertificateRepository;
import com.epam.esm.restadvanced.repository.entity.GiftCertificate;
import com.epam.esm.restadvanced.repository.entity.Tag;
import com.epam.esm.restadvanced.service.GiftCertificateManager;
import com.epam.esm.restadvanced.service.exception.GiftCertificateDuplicateException;
import com.epam.esm.restadvanced.service.exception.GiftCertificateNotFoundException;
import com.epam.esm.restadvanced.service.exception.GiftCertificateNotModifiedException;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.internal.verification.VerificationModeFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.nio.file.OpenOption;
import java.util.Arrays;
import java.util.Collections;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
@ActiveProfiles("test-h2")
@ContextConfiguration(classes =  {TestH2Config.class})
@PropertySource(name="stringValues", value = "classpath:string-values.properties")
class GiftCertificateManagerTest {
    private final static int DEFAULT_PAGE_NUMBER = 1;
    private final static int DEFAULT_PAGE_SIZE = 3;
    GiftCertificateManager giftCertificateManager;
    @Autowired
    GiftCertificateRepository giftCertificateRepository;
    @Autowired
    Environment environment;
    @Mock
    GiftCertificateSort giftCertificateSort;
    @Mock
    TagNamePropertyHandler tagNamePropertyHandler;

    @BeforeEach
    public void init() {
        MockitoAnnotations.openMocks(this);
        giftCertificateManager = new GiftCertificateManagerImpl(giftCertificateSort, giftCertificateRepository,
                tagNamePropertyHandler, environment);

        GiftCertificate gc1 = new GiftCertificate();
        gc1.setName("GC-one");
        gc1.setDescription("gift certificate one for integration test");
        gc1.setPrice(10.5);
        gc1.setDuration(10);
        Tag tag1 = new Tag();
        tag1.setName("dell");
        gc1.setTags(Collections.singletonList(tag1));

        GiftCertificate gc2 = new GiftCertificate();
        gc2.setName("GC-two");
        gc2.setDescription("gift certificate two for integration test");
        gc2.setPrice(12.5);
        gc2.setDuration(20);
        Tag tag2 = new Tag();
        tag2.setName("lenovo");
        gc2.setTags(Arrays.asList(tag1, tag2));

        GiftCertificate gc3 = new GiftCertificate();
        gc3.setName("GC-three");
        gc3.setDescription("gift certificate three for integration test");
        gc3.setPrice(33.5);
        gc3.setDuration(30);
        gc3.setTags(Arrays.asList(tag1, tag2));

        GiftCertificate gc4 = new GiftCertificate();
        gc4.setName("GC-four");
        gc4.setDescription("gift certificate four for integration test");
        gc4.setPrice(4.49);
        gc4.setDuration(40);
        gc4.setTags(Arrays.asList(tag1, tag2));

        GiftCertificate gc5 = new GiftCertificate();
        gc5.setName("GC-five");
        gc5.setDescription("gift certificate five for integration test");
        gc5.setPrice(5.555);
        gc5.setDuration(5);
        gc5.setTags(Arrays.asList(tag1, tag2));

        GiftCertificate gc6 = new GiftCertificate();
        gc6.setName("GC-six");
        gc6.setDescription("gift certificate six for integration test");
        gc6.setPrice(6.66);
        gc6.setDuration(6);
        gc6.setTags(Arrays.asList(tag1, tag2));

        GiftCertificate gc7 = new GiftCertificate();
        gc7.setName("GC-seven");
        gc7.setDescription("gift certificate seven for integration test");
        gc7.setPrice(77.7);
        gc7.setDuration(7);
        Tag tag3 = new Tag();
        tag3.setName("asus");
        gc7.setTags(Arrays.asList(tag1, tag3));

        GiftCertificate gc8 = new GiftCertificate();
        gc8.setName("GC-eight");
        gc8.setDescription("gift certificate eight for integration test");
        gc8.setPrice(80.8);
        gc8.setDuration(8);
        gc8.setTags(Arrays.asList(tag1, tag2, tag3));

        GiftCertificate gc9 = new GiftCertificate();
        gc9.setName("GC-nine");
        gc9.setDescription("gift certificate nine for integration test");
        gc9.setPrice(90.5);
        gc9.setDuration(9);
        gc9.setTags(Arrays.asList(tag1, tag2, tag3));

        GiftCertificate gc10 = new GiftCertificate();
        gc10.setName("GC-ten");
        gc10.setDescription("gift certificate ten for integration test");
        gc10.setPrice(100.5);
        gc10.setDuration(10);
        gc10.setTags(Arrays.asList(tag2, tag3));

        giftCertificateRepository.saveGiftCertificate(gc1);
        giftCertificateRepository.saveGiftCertificate(gc2);
        giftCertificateRepository.saveGiftCertificate(gc3);
        giftCertificateRepository.saveGiftCertificate(gc4);
        giftCertificateRepository.saveGiftCertificate(gc5);
        giftCertificateRepository.saveGiftCertificate(gc6);
        giftCertificateRepository.saveGiftCertificate(gc7);
        giftCertificateRepository.saveGiftCertificate(gc8);
        giftCertificateRepository.saveGiftCertificate(gc9);
        giftCertificateRepository.saveGiftCertificate(gc10);
    }

    @AfterEach
    public void tearDown() {
        giftCertificateRepository.deleteAll();
    }

    @Test
    public void shouldFindGiftCertificatesAndTagsWithDateSortByTagName() {
        when(tagNamePropertyHandler.retrieveCompatibleNames(new String[] {"lenovo", "dell"})).thenReturn("'lenovo', 'dell'");
        when(giftCertificateSort.convert(new String[] {"date"})).thenReturn("gift_certificate.create_date");
        Optional<String[]> tagNames = Optional.of(new String[]{"lenovo","dell"});
        Optional<String[]> sortBy =Optional.of(new String[]{"date"});
        Page<GiftCertificate> giftCertificatePage1 = giftCertificateManager.findByTagNames(
                        tagNames, sortBy, new PageRequest(DEFAULT_PAGE_NUMBER, DEFAULT_PAGE_SIZE));
        assertNotNull(giftCertificatePage1);
        assertEquals(1, giftCertificatePage1.getNumber());
        assertEquals(7, giftCertificatePage1.getTotalElements());
        assertEquals(3, giftCertificatePage1.getTotalPages());
        assertTrue(giftCertificatePage1.hasNext());
        assertFalse(giftCertificatePage1.hasPrevious());
        assertTrue(giftCertificatePage1.hasContent());
        assertEquals(3, giftCertificatePage1.getNumberOfElements());

        Page<GiftCertificate> giftCertificatePage2 = giftCertificateManager.findByTagNames(
                tagNames, sortBy, giftCertificatePage1.getNextPageable());
        assertEquals(2, giftCertificatePage2.getNumber());
        assertEquals(7, giftCertificatePage2.getTotalElements());
        assertEquals(3, giftCertificatePage2.getTotalPages());

        assertTrue(giftCertificatePage2.hasNext());
        assertTrue(giftCertificatePage2.hasPrevious());
        assertTrue(giftCertificatePage2.hasContent());
        assertEquals(3, giftCertificatePage2.getNumberOfElements());

        Page<GiftCertificate> giftCertificatePage3 = giftCertificateManager.findByTagNames(
                tagNames, sortBy, giftCertificatePage2.getNextPageable());
        assertEquals(3, giftCertificatePage3.getNumber());
        assertEquals(7, giftCertificatePage3.getTotalElements());
        assertEquals(3, giftCertificatePage3.getTotalPages());
        assertEquals(1, giftCertificatePage3.getNumberOfElements());

        assertFalse(giftCertificatePage3.hasNext());
        assertTrue(giftCertificatePage3.hasPrevious());
        assertTrue(giftCertificatePage3.hasContent());

        Page<GiftCertificate> giftCertificatePage4 = giftCertificateManager.findByTagNames(
                tagNames, sortBy, giftCertificatePage3.getPreviousPageable());
        assertEquals(2, giftCertificatePage4.getNumber());
        assertEquals(7, giftCertificatePage4.getTotalElements());
        assertEquals(3, giftCertificatePage4.getTotalPages());
        assertEquals(3, giftCertificatePage4.getNumberOfElements());

        assertTrue(giftCertificatePage4.hasNext());
        assertTrue(giftCertificatePage4.hasPrevious());
        assertTrue(giftCertificatePage4.hasContent());

        verify(giftCertificateSort, VerificationModeFactory.times(4)).convert(sortBy.get());
        verify(tagNamePropertyHandler, VerificationModeFactory.times(4)).
                retrieveCompatibleNames(tagNames.get());
    }

    @Test
    public void shouldFindGiftCertificatesAndTagsWithNameASCSortByTagName() {
        Optional<String[]> tagNames = Optional.of(new String[]{"asus", "lenovo", "dell"});
        Optional<String[]> sortBy = Optional.of(new String[]{"name;ASC"});
        when(giftCertificateSort.convert(sortBy.get())).thenReturn("gift_certificate.name ASC");
        when(tagNamePropertyHandler.retrieveCompatibleNames(tagNames.get())).thenReturn("'asus','lenovo','dell'");
        Page<GiftCertificate> giftCertificates = giftCertificateManager.findByTagNames(
                        tagNames, sortBy, new PageRequest(DEFAULT_PAGE_NUMBER, DEFAULT_PAGE_SIZE));
        assertNotNull(giftCertificates);
        assertEquals(2, giftCertificates.getTotalElements());
        assertEquals(1, giftCertificates.getNumber());
        assertEquals(1, giftCertificates.getTotalPages());
        assertEquals(2,giftCertificates.getNumberOfElements());

        assertFalse(giftCertificates.hasNext());
        assertFalse(giftCertificates.hasPrevious());
        assertTrue(giftCertificates.hasContent());

        verify(giftCertificateSort, VerificationModeFactory.times(1)).convert(sortBy.get());
        verify(tagNamePropertyHandler, VerificationModeFactory.times(1)).
                retrieveCompatibleNames(tagNames.get());

        assertEquals("GC-eight", giftCertificates.getContent().get(0).getName());
        assertEquals("GC-nine", giftCertificates.getContent().get(1).getName());
    }

    @Test
    public void shouldFindGiftCertificatesAndTagsWithNameDESCSortByTagName() {
        Optional<String[]> tagNames = Optional.of(new String[]{"asus", "lenovo", "dell"});
        Optional<String[]> sortBy = Optional.of(new String[]{"name;DESC"});
        when(giftCertificateSort.convert(sortBy.get())).thenReturn("gift_certificate.name DESC");
        when(tagNamePropertyHandler.retrieveCompatibleNames(tagNames.get())).thenReturn("'asus','lenovo','dell'");
        Page<GiftCertificate> giftCertificates = giftCertificateManager.findByTagNames(
                tagNames, sortBy, new PageRequest(DEFAULT_PAGE_NUMBER, DEFAULT_PAGE_SIZE));
        assertNotNull(giftCertificates);
        assertEquals(2, giftCertificates.getTotalElements());
        assertEquals(1, giftCertificates.getNumber());
        assertEquals(1, giftCertificates.getTotalPages());
        assertEquals(2,giftCertificates.getNumberOfElements());

        assertFalse(giftCertificates.hasNext());
        assertFalse(giftCertificates.hasPrevious());
        assertTrue(giftCertificates.hasContent());

        verify(giftCertificateSort, VerificationModeFactory.times(1)).convert(sortBy.get());
        verify(tagNamePropertyHandler, VerificationModeFactory.times(1)).
                retrieveCompatibleNames(tagNames.get());

        assertEquals("GC-nine", giftCertificates.getContent().get(0).getName());
        assertEquals("GC-eight", giftCertificates.getContent().get(1).getName());
    }

    @Test
    public void shouldFindGiftCertificatesAndTagsWithDateAndNameDESCSortByTagName() {
        Optional<String[]> tagNames = Optional.of(new String[]{"asus", "lenovo", "dell"});
        Optional<String[]> sortBy = Optional.of(new String[]{"date","name;ASC"});
        when(giftCertificateSort.convert(sortBy.get())).thenReturn("gift_certificate.create_date, gift_certificate.name ASC");
        when(tagNamePropertyHandler.retrieveCompatibleNames(tagNames.get())).thenReturn("'asus','lenovo','dell'");
        Page<GiftCertificate> giftCertificates = giftCertificateManager.findByTagNames(
                tagNames, sortBy, new PageRequest(DEFAULT_PAGE_NUMBER, DEFAULT_PAGE_SIZE));
        assertNotNull(giftCertificates);
        assertEquals(2, giftCertificates.getTotalElements());
        assertEquals(1, giftCertificates.getNumber());
        assertEquals(1, giftCertificates.getTotalPages());
        assertEquals(2,giftCertificates.getNumberOfElements());

        assertFalse(giftCertificates.hasNext());
        assertFalse(giftCertificates.hasPrevious());
        assertTrue(giftCertificates.hasContent());

        verify(giftCertificateSort, VerificationModeFactory.times(1)).convert(sortBy.get());
        verify(tagNamePropertyHandler, VerificationModeFactory.times(1)).
                retrieveCompatibleNames(tagNames.get());

        assertEquals("GC-eight", giftCertificates.getContent().get(0).getName());
        assertEquals("GC-nine", giftCertificates.getContent().get(1).getName());
    }

    @Test
    public void shouldFindGiftCertificatesAndTagsWithDateAndNameASCSortByTagName() {
        Optional<String[]> tagNames = Optional.of(new String[]{"asus", "lenovo", "dell"});
        Optional<String[]> sortBy = Optional.of(new String[]{"date", "name;ASC"});
        when(giftCertificateSort.convert(sortBy.get())).thenReturn("gift_certificate.name ASC");
        when(tagNamePropertyHandler.retrieveCompatibleNames(tagNames.get())).thenReturn("'asus','lenovo','dell'");
        Page<GiftCertificate> giftCertificates = giftCertificateManager.findByTagNames(
                tagNames, sortBy, new PageRequest(DEFAULT_PAGE_NUMBER, DEFAULT_PAGE_SIZE));
        assertNotNull(giftCertificates);
        assertEquals(2, giftCertificates.getTotalElements());
        assertEquals(1, giftCertificates.getNumber());
        assertEquals(1, giftCertificates.getTotalPages());
        assertEquals(2,giftCertificates.getNumberOfElements());

        assertFalse(giftCertificates.hasNext());
        assertFalse(giftCertificates.hasPrevious());
        assertTrue(giftCertificates.hasContent());

        verify(giftCertificateSort, VerificationModeFactory.times(1)).convert(sortBy.get());
        verify(tagNamePropertyHandler, VerificationModeFactory.times(1)).
                retrieveCompatibleNames(tagNames.get());

        assertEquals("GC-eight", giftCertificates.getContent().get(0).getName());
        assertEquals("GC-nine", giftCertificates.getContent().get(1).getName());
    }

    @Test
    public void shouldThrowExceptionWhenFindGiftCertificatesAndTagsWithDateAndNameDESCSortByTagName() {
        Optional<String[]> tagNames = Optional.of(new String[]{"nelly, old-phone"});
        Optional<String[]> sortBy = Optional.of(new String[]{"date", "name;DESC"});
        when(giftCertificateSort.convert(sortBy.get())).
                thenReturn("gift_certificate.create_date, gift_certificate.name DESC");
        when(tagNamePropertyHandler.retrieveCompatibleNames(tagNames.get())).thenReturn("'nelly','old-phone'");
        String errorMessage = environment.getProperty("giftCertificate.message.searchNotFound");
        GiftCertificateNotFoundException ex = assertThrows(GiftCertificateNotFoundException.class, () -> {
            giftCertificateManager.findByTagNames(tagNames, sortBy,
                    new PageRequest(DEFAULT_PAGE_NUMBER, DEFAULT_PAGE_SIZE));
        });
        assertEquals(errorMessage, ex.getMessage());
        verify(giftCertificateSort, VerificationModeFactory.times(1)).convert(sortBy.get());
        verify(tagNamePropertyHandler, VerificationModeFactory.times(1)).
                retrieveCompatibleNames(tagNames.get());
    }

    @Test
    public void shouldFindGiftCertificatesAndTagsByPartOfTagNames() {
        when(tagNamePropertyHandler.retrieveCompatibleNames(new String[] {"lenovo", "dell"})).
                thenReturn("'lenovo', 'dell'");
        Optional<String[]> tagNames = Optional.of(new String[] {"lenovo","dell"});
        Optional<String[]> sortBy = Optional.empty();
        Page<GiftCertificate> giftCertificatePage1 = giftCertificateManager.findByTagNames(
                tagNames, sortBy, new PageRequest(DEFAULT_PAGE_NUMBER, DEFAULT_PAGE_SIZE));
        assertNotNull(giftCertificatePage1);
        assertEquals(1, giftCertificatePage1.getNumber());
        assertEquals(7, giftCertificatePage1.getTotalElements());
        assertEquals(3, giftCertificatePage1.getTotalPages());
        assertTrue(giftCertificatePage1.hasNext());
        assertFalse(giftCertificatePage1.hasPrevious());
        assertTrue(giftCertificatePage1.hasContent());
        assertEquals(3, giftCertificatePage1.getNumberOfElements());

        Page<GiftCertificate> giftCertificatePage2 = giftCertificateManager.findByTagNames(
                tagNames, sortBy,giftCertificatePage1.getNextPageable());
        assertEquals(2, giftCertificatePage2.getNumber());
        assertEquals(7, giftCertificatePage2.getTotalElements());
        assertEquals(3, giftCertificatePage2.getTotalPages());

        assertTrue(giftCertificatePage2.hasNext());
        assertTrue(giftCertificatePage2.hasPrevious());
        assertTrue(giftCertificatePage2.hasContent());
        assertEquals(3, giftCertificatePage2.getNumberOfElements());

        Page<GiftCertificate> giftCertificatePage3 = giftCertificateManager.findByTagNames(
                tagNames, sortBy, giftCertificatePage2.getNextPageable());
        assertEquals(3, giftCertificatePage3.getNumber());
        assertEquals(7, giftCertificatePage3.getTotalElements());
        assertEquals(3, giftCertificatePage3.getTotalPages());
        assertEquals(1, giftCertificatePage3.getNumberOfElements());

        assertFalse(giftCertificatePage3.hasNext());
        assertTrue(giftCertificatePage3.hasPrevious());
        assertTrue(giftCertificatePage3.hasContent());

        Page<GiftCertificate> giftCertificatePage4 = giftCertificateManager.findByTagNames(
                tagNames, sortBy, giftCertificatePage3.getPreviousPageable());
        assertEquals(2, giftCertificatePage4.getNumber());
        assertEquals(7, giftCertificatePage4.getTotalElements());
        assertEquals(3, giftCertificatePage4.getTotalPages());
        assertEquals(3, giftCertificatePage4.getNumberOfElements());

        assertTrue(giftCertificatePage4.hasNext());
        assertTrue(giftCertificatePage4.hasPrevious());
        assertTrue(giftCertificatePage4.hasContent());

        verify(tagNamePropertyHandler, VerificationModeFactory.times(4)).
                retrieveCompatibleNames(tagNames.get());
    }

    @Test
    public void shouldThrowExceptionWhenFindGiftCertificatesAndTagsByTagNames() {
        Optional<String[]> tagNames = Optional.of(new String[]{"nelly, old-phone"});
        when(tagNamePropertyHandler.retrieveCompatibleNames(tagNames.get())).thenReturn("'nelly','old-phone'");
        String errorMessage = environment.getProperty("giftCertificate.message.searchNotFound");
        GiftCertificateNotFoundException ex = assertThrows(GiftCertificateNotFoundException.class, () -> {
            giftCertificateManager.findByTagNames(tagNames, Optional.empty(),
                    new PageRequest(DEFAULT_PAGE_NUMBER, DEFAULT_PAGE_SIZE));
        });
        assertEquals(errorMessage, ex.getMessage());
        verify(tagNamePropertyHandler, VerificationModeFactory.times(1)).
                retrieveCompatibleNames(tagNames.get());
    }

    @Test
    public void shouldFindGiftCertificatesAndTagsWithDateSortByPartOfName() {
        when(giftCertificateSort.convert(new String[] {"date"})).thenReturn("gift_certificate.create_date");
        Optional<String> partOfName = Optional.of("GC-s");
        Optional<String[]> sortBy = Optional.of(new String[]{"date"});
        Page<GiftCertificate> giftCertificates = giftCertificateManager.findByNameContaining(
                partOfName, sortBy, new PageRequest(DEFAULT_PAGE_NUMBER, DEFAULT_PAGE_SIZE));
        assertNotNull(giftCertificates);
        assertEquals(2, giftCertificates.getTotalElements());
        assertEquals(1, giftCertificates.getTotalPages());
        assertEquals(1, giftCertificates.getNumber());
        assertEquals(2, giftCertificates.getNumberOfElements());

        assertTrue(giftCertificates.hasContent());
        assertFalse(giftCertificates.hasNext());
        assertFalse(giftCertificates.hasPrevious());

        assertEquals("GC-six", giftCertificates.getContent().get(0).getName());
        assertEquals("GC-seven", giftCertificates.getContent().get(1).getName());
        verify(giftCertificateSort, VerificationModeFactory.times(1)).convert(sortBy.get());
    }

    @Test
    public void shouldFindGiftCertificatesAndTagsWithNameASCSortByPartOfName() {
        Optional<String> partOfName = Optional.of("GC-s");
        Optional<String[]> sortBy = Optional.of(new String[]{"name;ASC"});
        when(giftCertificateSort.convert(sortBy.get())).thenReturn("gift_certificate.name ASC");
        Page<GiftCertificate> giftCertificates = giftCertificateManager.findByNameContaining(
                partOfName, sortBy, new PageRequest(DEFAULT_PAGE_NUMBER, DEFAULT_PAGE_SIZE));

        assertNotNull(giftCertificates);
        assertEquals(2, giftCertificates.getTotalElements());
        assertEquals(1, giftCertificates.getTotalPages());
        assertEquals(1, giftCertificates.getNumber());
        assertEquals(2, giftCertificates.getNumberOfElements());

        assertTrue(giftCertificates.hasContent());
        assertFalse(giftCertificates.hasNext());
        assertFalse(giftCertificates.hasPrevious());

        assertEquals("GC-seven", giftCertificates.getContent().get(0).getName());
        assertEquals("GC-six", giftCertificates.getContent().get(1).getName());
        verify(giftCertificateSort, VerificationModeFactory.times(1)).convert(sortBy.get());
    }

    @Test
    public void shouldFindGiftCertificatesAndTagsWithNameDESCSortByPartOfName() {
        Optional<String> partOfName = Optional.of("GC-s");
        Optional<String[]> sortBy = Optional.of(new String[]{"name;DESC"});
        when(giftCertificateSort.convert(sortBy.get())).thenReturn("gift_certificate.name DESC");
        Page<GiftCertificate> giftCertificates = giftCertificateManager.findByNameContaining(
                partOfName, sortBy, new PageRequest(DEFAULT_PAGE_NUMBER, DEFAULT_PAGE_SIZE));
        assertNotNull(giftCertificates);
        assertEquals(2, giftCertificates.getTotalElements());
        assertEquals(1, giftCertificates.getTotalPages());
        assertEquals(1, giftCertificates.getNumber());
        assertEquals(2, giftCertificates.getNumberOfElements());

        assertTrue(giftCertificates.hasContent());
        assertFalse(giftCertificates.hasNext());
        assertFalse(giftCertificates.hasPrevious());

        assertEquals("GC-six", giftCertificates.getContent().get(0).getName());
        assertEquals("GC-seven", giftCertificates.getContent().get(1).getName());
        verify(giftCertificateSort, VerificationModeFactory.times(1)).convert(sortBy.get());
    }

    @Test
    public void shouldFindGiftCertificatesAndTagsWithDateAndNameDESCSortByPartOfName() {
        Optional<String> partOfName = Optional.of("GC-s");
        Optional<String[]> sortBy = Optional.of(new String[]{"date", "name;DESC"});
        when(giftCertificateSort.convert(sortBy.get())).
                thenReturn("gift_certificate.create_date, gift_certificate.name DESC");
        Page<GiftCertificate> giftCertificates = giftCertificateManager.findByNameContaining(
                partOfName, sortBy, new PageRequest(DEFAULT_PAGE_NUMBER, DEFAULT_PAGE_SIZE));
        assertNotNull(giftCertificates);
        assertEquals(2, giftCertificates.getTotalElements());
        assertEquals(1, giftCertificates.getTotalPages());
        assertEquals(1, giftCertificates.getNumber());
        assertEquals(2, giftCertificates.getNumberOfElements());

        assertTrue(giftCertificates.hasContent());
        assertFalse(giftCertificates.hasNext());
        assertFalse(giftCertificates.hasPrevious());

        assertEquals("GC-six", giftCertificates.getContent().get(0).getName());
        assertEquals("GC-seven", giftCertificates.getContent().get(1).getName());
        verify(giftCertificateSort, VerificationModeFactory.times(1)).convert(sortBy.get());
    }

    @Test
    public void shouldFindGiftCertificatesAndTagsWithDateAndNameASCSortByPartOfName() {
        Optional<String> partOfName = Optional.of("GC-s");
        Optional<String[]> sortBy = Optional.of(new String[] {"date", "name;ASC"});
        when(giftCertificateSort.convert(sortBy.get())).
                thenReturn("gift_certificate.create_date, gift_certificate.name ASC");
        Page<GiftCertificate> giftCertificates = giftCertificateManager.findByNameContaining(
                partOfName, sortBy, new PageRequest(DEFAULT_PAGE_NUMBER, DEFAULT_PAGE_SIZE));
        assertNotNull(giftCertificates);
        assertEquals(2, giftCertificates.getTotalElements());
        assertEquals(1, giftCertificates.getTotalPages());
        assertEquals(1, giftCertificates.getNumber());
        assertEquals(2, giftCertificates.getNumberOfElements());

        assertTrue(giftCertificates.hasContent());
        assertFalse(giftCertificates.hasNext());
        assertFalse(giftCertificates.hasPrevious());
        assertEquals("GC-six", giftCertificates.getContent().get(0).getName());
        assertEquals("GC-seven", giftCertificates.getContent().get(1).getName());
        verify(giftCertificateSort, VerificationModeFactory.times(1)).convert(sortBy.get());
    }

    @Test
    public void shouldThrowExceptionWhenFindGiftCertificatesAndTagsWithDateAndNameDESCSortByPartOfName() {
        Optional<String> partOfName = Optional.of("darken");
        Optional<String[]> sortBy = Optional.of(new String[]{"date", "name;DESC"});
        when(giftCertificateSort.convert(sortBy.get())).
                thenReturn("gift_certificate.create_date, gift_certificate.name DESC");
        String errorMessage = environment.getProperty("giftCertificate.message.searchNotFound");
        GiftCertificateNotFoundException ex = assertThrows(GiftCertificateNotFoundException.class, () ->{
            giftCertificateManager.findByNameContaining(
                    partOfName, sortBy, new PageRequest(DEFAULT_PAGE_NUMBER, DEFAULT_PAGE_SIZE));
        });
        assertEquals(errorMessage, ex.getMessage());
        verify(giftCertificateSort, VerificationModeFactory.times(1)).convert(sortBy.get());
    }

    @Test
    public void shouldFindGiftCertificatesAndTagsByPartOfName() {
        Optional<String> partOfName = Optional.of("GC-");
        Page<GiftCertificate> giftCertificatePage1 = giftCertificateManager.findByNameContaining(
                partOfName, Optional.empty(), new PageRequest(DEFAULT_PAGE_NUMBER, DEFAULT_PAGE_SIZE));

        assertNotNull(giftCertificatePage1);
        assertEquals(10, giftCertificatePage1.getTotalElements());
        assertEquals(4, giftCertificatePage1.getTotalPages());
        assertEquals(1, giftCertificatePage1.getNumber());
        assertEquals(3, giftCertificatePage1.getNumberOfElements());

        assertTrue(giftCertificatePage1.hasContent());
        assertTrue(giftCertificatePage1.hasNext());
        assertFalse(giftCertificatePage1.hasPrevious());

        Page<GiftCertificate> giftCertificatePage2 = giftCertificateManager.findByNameContaining(
                partOfName, Optional.empty(), giftCertificatePage1.getNextPageable());
        assertNotNull(giftCertificatePage2);
        assertEquals(10, giftCertificatePage2.getTotalElements());
        assertEquals(4, giftCertificatePage2.getTotalPages());
        assertEquals(2, giftCertificatePage2.getNumber());
        assertEquals(3, giftCertificatePage2.getNumberOfElements());

        assertTrue(giftCertificatePage2.hasContent());
        assertTrue(giftCertificatePage2.hasNext());
        assertTrue(giftCertificatePage2.hasPrevious());

        Page<GiftCertificate> giftCertificatePage3 = giftCertificateManager.findByNameContaining(
                partOfName, Optional.empty(), giftCertificatePage2.getNextPageable());
        assertNotNull(giftCertificatePage3);
        assertEquals(10, giftCertificatePage3.getTotalElements());
        assertEquals(4, giftCertificatePage3.getTotalPages());
        assertEquals(3, giftCertificatePage3.getNumber());
        assertEquals(3, giftCertificatePage3.getNumberOfElements());

        assertTrue(giftCertificatePage3.hasContent());
        assertTrue(giftCertificatePage3.hasNext());
        assertTrue(giftCertificatePage3.hasPrevious());

        Page<GiftCertificate> giftCertificatePage4 = giftCertificateManager.findByNameContaining(
                partOfName, Optional.empty(), giftCertificatePage3.getNextPageable());
        assertNotNull(giftCertificatePage4);
        assertEquals(10, giftCertificatePage4.getTotalElements());
        assertEquals(4, giftCertificatePage4.getTotalPages());
        assertEquals(4, giftCertificatePage4.getNumber());
        assertEquals(1, giftCertificatePage4.getNumberOfElements());

        assertTrue(giftCertificatePage4.hasContent());
        assertFalse(giftCertificatePage4.hasNext());
        assertTrue(giftCertificatePage4.hasPrevious());

        Page<GiftCertificate> giftCertificatePage5 = giftCertificateManager.findByNameContaining(
                partOfName, Optional.empty(), giftCertificatePage4.getPreviousPageable());
        assertNotNull(giftCertificatePage5);
        assertEquals(10, giftCertificatePage5.getTotalElements());
        assertEquals(4, giftCertificatePage5.getTotalPages());
        assertEquals(3, giftCertificatePage5.getNumber());
        assertEquals(3, giftCertificatePage5.getNumberOfElements());

        assertTrue(giftCertificatePage5.hasContent());
        assertTrue(giftCertificatePage5.hasNext());
        assertTrue(giftCertificatePage5.hasPrevious());
    }

    @Test
    public void shouldThrowExceptionWhenFindGiftCertificatesAndTagsByPartOfName() {
        Optional<String> partOfName = Optional.of("gtn");
        String errorMessage = environment.getProperty("giftCertificate.message.searchNotFound");
        GiftCertificateNotFoundException ex = assertThrows(GiftCertificateNotFoundException.class, () ->{
            giftCertificateManager.findByNameContaining(
                    partOfName, Optional.empty(), new PageRequest(DEFAULT_PAGE_NUMBER, DEFAULT_PAGE_SIZE));
        });
        assertEquals(errorMessage, ex.getMessage());
    }

    @Test
    public void shouldFindGiftCertificatesAndTagsWithDateSortByPartOfDesc() {
        Optional<String[]> sortBy = Optional.of(new String[]{"date"});
        when(giftCertificateSort.convert(sortBy.get())).thenReturn("gift_certificate.create_date");
        Optional<String> partOfDesc = Optional.of("certificate");
        Page<GiftCertificate> giftCertificatePage1 = giftCertificateManager.
                findByDescriptionContaining(partOfDesc, sortBy,
                        new PageRequest(DEFAULT_PAGE_NUMBER, DEFAULT_PAGE_SIZE));
        assertNotNull(giftCertificatePage1);
        assertEquals(10, giftCertificatePage1.getTotalElements());
        assertEquals(4, giftCertificatePage1.getTotalPages());
        assertEquals(1, giftCertificatePage1.getNumber());
        assertEquals(3, giftCertificatePage1.getNumberOfElements());

        assertTrue(giftCertificatePage1.hasContent());
        assertTrue(giftCertificatePage1.hasNext());
        assertFalse(giftCertificatePage1.hasPrevious());
        assertEquals("GC-one", giftCertificatePage1.getContent().get(0).getName());
        assertEquals("GC-two", giftCertificatePage1.getContent().get(1).getName());
        assertEquals("GC-three", giftCertificatePage1.getContent().get(2).getName());

        Page<GiftCertificate> giftCertificatePage2 = giftCertificateManager.
                findByDescriptionContaining(partOfDesc, sortBy,
                        giftCertificatePage1.getNextPageable());
        assertNotNull(giftCertificatePage2);
        assertEquals(10, giftCertificatePage2.getTotalElements());
        assertEquals(4, giftCertificatePage2.getTotalPages());
        assertEquals(2, giftCertificatePage2.getNumber());
        assertEquals(3, giftCertificatePage2.getNumberOfElements());

        assertTrue(giftCertificatePage2.hasContent());
        assertTrue(giftCertificatePage2.hasNext());
        assertTrue(giftCertificatePage2.hasPrevious());
        assertEquals("GC-four", giftCertificatePage2.getContent().get(0).getName());
        assertEquals("GC-five", giftCertificatePage2.getContent().get(1).getName());
        assertEquals("GC-six", giftCertificatePage2.getContent().get(2).getName());

        Page<GiftCertificate> giftCertificatePage3 = giftCertificateManager.
                findByDescriptionContaining(partOfDesc, sortBy,
                        giftCertificatePage2.getNextPageable());
        assertNotNull(giftCertificatePage3);
        assertEquals(10, giftCertificatePage3.getTotalElements());
        assertEquals(4, giftCertificatePage3.getTotalPages());
        assertEquals(3, giftCertificatePage3.getNumber());
        assertEquals(3, giftCertificatePage3.getNumberOfElements());

        assertTrue(giftCertificatePage3.hasContent());
        assertTrue(giftCertificatePage3.hasNext());
        assertTrue(giftCertificatePage3.hasPrevious());
        assertEquals("GC-seven", giftCertificatePage3.getContent().get(0).getName());
        assertEquals("GC-eight", giftCertificatePage3.getContent().get(1).getName());
        assertEquals("GC-nine", giftCertificatePage3.getContent().get(2).getName());
        Page<GiftCertificate> giftCertificatePage4 = giftCertificateManager.
                findByDescriptionContaining(partOfDesc, sortBy,
                        giftCertificatePage3.getNextPageable());
        assertNotNull(giftCertificatePage4);
        assertEquals(10, giftCertificatePage4.getTotalElements());
        assertEquals(4, giftCertificatePage4.getTotalPages());
        assertEquals(4, giftCertificatePage4.getNumber());
        assertEquals(1, giftCertificatePage4.getNumberOfElements());

        assertTrue(giftCertificatePage4.hasContent());
        assertFalse(giftCertificatePage4.hasNext());
        assertTrue(giftCertificatePage4.hasPrevious());
        assertEquals("GC-ten", giftCertificatePage4.getContent().get(0).getName());

        Page<GiftCertificate> giftCertificatePage5 = giftCertificateManager.
                findByDescriptionContaining(partOfDesc, sortBy,
                        giftCertificatePage4.getPreviousPageable());
        assertNotNull(giftCertificatePage5);
        assertEquals(10, giftCertificatePage5.getTotalElements());
        assertEquals(4, giftCertificatePage5.getTotalPages());
        assertEquals(3, giftCertificatePage5.getNumber());
        assertEquals(3, giftCertificatePage5.getNumberOfElements());

        assertTrue(giftCertificatePage5.hasContent());
        assertTrue(giftCertificatePage5.hasNext());
        assertTrue(giftCertificatePage5.hasPrevious());
        assertEquals("GC-seven", giftCertificatePage5.getContent().get(0).getName());
        assertEquals("GC-eight", giftCertificatePage5.getContent().get(1).getName());
        assertEquals("GC-nine", giftCertificatePage5.getContent().get(2).getName());
        verify(giftCertificateSort, VerificationModeFactory.times(5)).convert(sortBy.get());
    }

    @Test
    public void shouldFindGiftCertificatesAndTagsWithNameASCSortByPartOfDesc() {
        Optional<String> partOfDesc = Optional.of("certificate s");
        Optional<String[]> sortBy = Optional.of(new String[]{"name;ASC"});
        when(giftCertificateSort.convert(sortBy.get())).thenReturn("gift_certificate.name ASC");
        Page<GiftCertificate> giftCertificates = giftCertificateManager.findByDescriptionContaining(
                partOfDesc, sortBy, new PageRequest(DEFAULT_PAGE_NUMBER, DEFAULT_PAGE_SIZE));
        assertNotNull(giftCertificates);
        assertEquals(2, giftCertificates.getTotalElements());
        assertEquals(1, giftCertificates.getTotalPages());
        assertEquals(1, giftCertificates.getNumber());
        assertEquals(2, giftCertificates.getNumberOfElements());

        assertTrue(giftCertificates.hasContent());
        assertFalse(giftCertificates.hasNext());
        assertFalse(giftCertificates.hasPrevious());
        assertEquals("GC-seven", giftCertificates.getContent().get(0).getName());
        assertEquals("GC-six", giftCertificates.getContent().get(1).getName());
        verify(giftCertificateSort, VerificationModeFactory.times(1)).convert(sortBy.get());
    }

    @Test
    public void shouldFindGiftCertificatesAndTagsWithNameDESCSortByPartOfDesc() {
        Optional<String> partOfDesc = Optional.of("certificate s");
        Optional<String[]> sortBy = Optional.of(new String[]{"name;DESC"});
        when(giftCertificateSort.convert(sortBy.get())).thenReturn("gift_certificate.name DESC");
        Page<GiftCertificate> giftCertificates = giftCertificateManager.findByDescriptionContaining(
                partOfDesc, sortBy, new PageRequest(DEFAULT_PAGE_NUMBER, DEFAULT_PAGE_SIZE));
        assertNotNull(giftCertificates);
        assertEquals(2, giftCertificates.getTotalElements());
        assertEquals(1, giftCertificates.getTotalPages());
        assertEquals(1, giftCertificates.getNumber());
        assertEquals(2, giftCertificates.getNumberOfElements());

        assertTrue(giftCertificates.hasContent());
        assertFalse(giftCertificates.hasNext());
        assertFalse(giftCertificates.hasPrevious());
        assertEquals("GC-six", giftCertificates.getContent().get(0).getName());
        assertEquals("GC-seven", giftCertificates.getContent().get(1).getName());
        verify(giftCertificateSort, VerificationModeFactory.times(1)).convert(sortBy.get());
    }

    @Test
    public void shouldFindGiftCertificatesAndTagsWithDateAndNameDESCSortByPartOfDesc() {
        Optional<String> partOfDesc = Optional.of("certificate s");
        Optional<String[]> sortBy = Optional.of(new String[]{"date", "name;DESC"});
        when(giftCertificateSort.convert(sortBy.get())).
                thenReturn("gift_certificate.create_date, gift_certificate.name DESC");
        Page<GiftCertificate> giftCertificates = giftCertificateManager.findByDescriptionContaining(
                partOfDesc, sortBy, new PageRequest(DEFAULT_PAGE_NUMBER, DEFAULT_PAGE_SIZE));
        assertNotNull(giftCertificates);
        assertEquals(2, giftCertificates.getTotalElements());
        assertEquals(1, giftCertificates.getTotalPages());
        assertEquals(1, giftCertificates.getNumber());
        assertEquals(2, giftCertificates.getNumberOfElements());

        assertTrue(giftCertificates.hasContent());
        assertFalse(giftCertificates.hasNext());
        assertFalse(giftCertificates.hasPrevious());
        assertEquals("GC-six", giftCertificates.getContent().get(0).getName());
        assertEquals("GC-seven", giftCertificates.getContent().get(1).getName());
        verify(giftCertificateSort, VerificationModeFactory.times(1)).convert(sortBy.get());
    }

    @Test
    public void shouldFindGiftCertificatesAndTagsWithDateAndNameASCSortByPartOfDesc() {
        Optional<String> partOfDesc = Optional.of("certificate s");
        Optional<String[]> sortBy = Optional.of(new String[]{"date", "name;ASC"});
        when(giftCertificateSort.convert(sortBy.get())).
                thenReturn("gift_certificate.create_date, gift_certificate.name ASC");
        Page<GiftCertificate> giftCertificates = giftCertificateManager.findByDescriptionContaining(
                partOfDesc, sortBy, new PageRequest(DEFAULT_PAGE_NUMBER, DEFAULT_PAGE_SIZE));
        assertNotNull(giftCertificates);
        assertEquals(2, giftCertificates.getTotalElements());
        assertEquals(1, giftCertificates.getTotalPages());
        assertEquals(1, giftCertificates.getNumber());
        assertEquals(2, giftCertificates.getNumberOfElements());

        assertTrue(giftCertificates.hasContent());
        assertFalse(giftCertificates.hasNext());
        assertFalse(giftCertificates.hasPrevious());
        assertEquals("GC-six", giftCertificates.getContent().get(0).getName());
        assertEquals("GC-seven", giftCertificates.getContent().get(1).getName());
        verify(giftCertificateSort, VerificationModeFactory.times(1)).convert(sortBy.get());
    }

    @Test
    public void shouldThrowExceptionWhenFindGiftCertificatesAndTagsWithDateAndNameDESCSortByPartOfDesc() {
        Optional<String> partOfDesc = Optional.of("bottle");
        Optional<String[]> sortBy = Optional.of(new String[]{"date", "name;DESC"});
        when(giftCertificateSort.convert(sortBy.get())).thenReturn("gift_certificate.create_date, gift_certificate.name DESC");
        String errorMessage = environment.getProperty("giftCertificate.message.searchNotFound");
        GiftCertificateNotFoundException ex = assertThrows(GiftCertificateNotFoundException.class, () ->{
            giftCertificateManager.findByDescriptionContaining(
                    partOfDesc, sortBy, new PageRequest(DEFAULT_PAGE_NUMBER, DEFAULT_PAGE_SIZE));
        });
        assertEquals(errorMessage, ex.getMessage());
        verify(giftCertificateSort, VerificationModeFactory.times(1)).convert(sortBy.get());
    }

    @Test
    public void shouldFindGiftCertificatesAndTagsByPartOfDesc() {
        Optional<String> partOfDesc = Optional.of("gift");
        Page<GiftCertificate> giftCertificatePage1 = giftCertificateManager.findByDescriptionContaining(
                partOfDesc, Optional.empty(), new PageRequest(DEFAULT_PAGE_NUMBER, DEFAULT_PAGE_SIZE));

        assertNotNull(giftCertificatePage1);
        assertEquals(10, giftCertificatePage1.getTotalElements());
        assertEquals(4, giftCertificatePage1.getTotalPages());
        assertEquals(1, giftCertificatePage1.getNumber());
        assertEquals(3, giftCertificatePage1.getNumberOfElements());

        assertTrue(giftCertificatePage1.hasContent());
        assertTrue(giftCertificatePage1.hasNext());
        assertFalse(giftCertificatePage1.hasPrevious());

        Page<GiftCertificate> giftCertificatePage2 = giftCertificateManager.findByDescriptionContaining(
                partOfDesc, Optional.empty(), giftCertificatePage1.getNextPageable());
        assertNotNull(giftCertificatePage2);
        assertEquals(10, giftCertificatePage2.getTotalElements());
        assertEquals(4, giftCertificatePage2.getTotalPages());
        assertEquals(2, giftCertificatePage2.getNumber());
        assertEquals(3, giftCertificatePage2.getNumberOfElements());

        assertTrue(giftCertificatePage2.hasContent());
        assertTrue(giftCertificatePage2.hasNext());
        assertTrue(giftCertificatePage2.hasPrevious());

        Page<GiftCertificate> giftCertificatePage3 = giftCertificateManager.findByDescriptionContaining(
                partOfDesc, Optional.empty(), giftCertificatePage2.getNextPageable());
        assertNotNull(giftCertificatePage3);
        assertEquals(10, giftCertificatePage3.getTotalElements());
        assertEquals(4, giftCertificatePage3.getTotalPages());
        assertEquals(3, giftCertificatePage3.getNumber());
        assertEquals(3, giftCertificatePage3.getNumberOfElements());

        assertTrue(giftCertificatePage3.hasContent());
        assertTrue(giftCertificatePage3.hasNext());
        assertTrue(giftCertificatePage3.hasPrevious());

        Page<GiftCertificate> giftCertificatePage4 = giftCertificateManager.findByDescriptionContaining(
                partOfDesc, Optional.empty(), giftCertificatePage3.getNextPageable());
        assertNotNull(giftCertificatePage4);
        assertEquals(10, giftCertificatePage4.getTotalElements());
        assertEquals(4, giftCertificatePage4.getTotalPages());
        assertEquals(4, giftCertificatePage4.getNumber());
        assertEquals(1, giftCertificatePage4.getNumberOfElements());

        assertTrue(giftCertificatePage4.hasContent());
        assertFalse(giftCertificatePage4.hasNext());
        assertTrue(giftCertificatePage4.hasPrevious());

        Page<GiftCertificate> giftCertificatePage5 = giftCertificateManager.findByDescriptionContaining(
                partOfDesc, Optional.empty(), giftCertificatePage4.getPreviousPageable());
        assertNotNull(giftCertificatePage5);
        assertEquals(10, giftCertificatePage5.getTotalElements());
        assertEquals(4, giftCertificatePage5.getTotalPages());
        assertEquals(3, giftCertificatePage5.getNumber());
        assertEquals(3, giftCertificatePage5.getNumberOfElements());

        assertTrue(giftCertificatePage5.hasContent());
        assertTrue(giftCertificatePage5.hasNext());
        assertTrue(giftCertificatePage5.hasPrevious());
    }

    @Test
    public void shouldThrowExceptionWhenFindGiftCertificatesAndTagsByPartOfDesc() {
        Optional<String> partOfDesc = Optional.of("bottle");
        String errorMessage = environment.getProperty("giftCertificate.message.searchNotFound");
        GiftCertificateNotFoundException ex = assertThrows(GiftCertificateNotFoundException.class, () ->{
            giftCertificateManager.findByDescriptionContaining(
                    partOfDesc, Optional.empty(), new PageRequest(DEFAULT_PAGE_NUMBER, DEFAULT_PAGE_SIZE));
        });
        assertEquals(errorMessage, ex.getMessage());
    }

    @Test
    public void shouldDeleteGiftCertificateById() {
        long giftCertificateId = 1L;
        GiftCertificate giftCertificate = giftCertificateManager.deleteGiftCertificateById(giftCertificateId);
        assertNotNull(giftCertificate);
        assertEquals(giftCertificateId, giftCertificate.getId());

        String errorMessage = String.format(
                environment.getProperty("giftCertificate.message.notFound"), giftCertificateId);
        GiftCertificateNotFoundException ex = assertThrows(GiftCertificateNotFoundException.class, () -> {
            giftCertificateManager.deleteGiftCertificateById(giftCertificateId);
        });
        assertEquals(errorMessage, ex.getMessage());
    }

    @Test
    public void shouldUpdateGiftCertificateById() {
        GiftCertificate giftCertificate = new GiftCertificate();
        giftCertificate.setName("updated gc-one"); // update
        giftCertificate.setDescription("gift certificate one for integration test"); // same
        giftCertificate.setPrice(10.5);
        giftCertificate.setDuration(20);
        Tag tag = new Tag();
        tag.setName("zero");
        giftCertificate.setTags(Collections.singletonList(tag)); // added new tag

        long giftCertificateId = 1L;
        GiftCertificate updatedGiftCertificate = giftCertificateManager.updateGiftCertificateById(giftCertificateId, giftCertificate);
        assertNotNull(updatedGiftCertificate);
        assertEquals(giftCertificateId, updatedGiftCertificate.getId());
        assertEquals(giftCertificate.getName(), updatedGiftCertificate.getName());
        assertEquals(1, updatedGiftCertificate.getTags().size());
        assertEquals(giftCertificate.getDuration(), updatedGiftCertificate.getDuration());

        String errorMessage = String.format(
                environment.getProperty("giftCertificate.message.notModified"), giftCertificateId);
        GiftCertificateNotModifiedException ex = assertThrows(GiftCertificateNotModifiedException.class, () -> {
            giftCertificateManager.updateGiftCertificateById(giftCertificateId, updatedGiftCertificate);
        });

        assertEquals(errorMessage, ex.getMessage());
    }

    @Test
    public void shouldThrowExceptionWhenWhenUpdateGiftCertificateById() {
        GiftCertificate giftCertificate = new GiftCertificate();
        giftCertificate.setName("updated gc-one"); // update
        giftCertificate.setDescription("gift certificate one for integration test"); // same
        giftCertificate.setPrice(10.5);
        giftCertificate.setDuration(20);
        Tag tag = new Tag();
        tag.setName("zero");
        giftCertificate.setTags(Collections.singletonList(tag)); // added new tag

        long giftCertificateId = 15L;
        String errorMessage = String.format(
                environment.getProperty("giftCertificate.message.notFound"), giftCertificateId);
        GiftCertificateNotFoundException ex = assertThrows(GiftCertificateNotFoundException.class, () -> {
            giftCertificateManager.updateGiftCertificateById(giftCertificateId, giftCertificate);
        });
        assertEquals(errorMessage, ex.getMessage());
    }

    @Test
    public void shouldFindGiftCertificateById() {
        long giftCertificateId = 2L;
        GiftCertificate giftCertificate = giftCertificateManager.findGiftCertificateById(giftCertificateId);
        assertNotNull(giftCertificate);
        assertEquals(giftCertificateId, giftCertificate.getId());
        assertEquals("GC-two", giftCertificate.getName());
        assertEquals("gift certificate two for integration test", giftCertificate.getDescription());
        assertEquals(12.5, giftCertificate.getPrice());
        assertEquals(20, giftCertificate.getDuration());
    }

    @Test
    public void shouldThrowExceptionWhenFindGiftCertificateById() {
        long giftCertificateId = 11L;
        String errorMessage = String.format(
                environment.getProperty("giftCertificate.message.notFound"), giftCertificateId);
        GiftCertificateNotFoundException ex = assertThrows(GiftCertificateNotFoundException.class, ()-> {
            giftCertificateManager.findGiftCertificateById(giftCertificateId);
        });
        assertEquals(errorMessage, ex.getMessage());
    }

    @Test
    public void shouldFindAllGiftCertificates() {
        Page<GiftCertificate> giftCertificatePage1 = giftCertificateManager.findAll(
                new PageRequest(DEFAULT_PAGE_NUMBER, DEFAULT_PAGE_SIZE));
        assertNotNull(giftCertificatePage1);
        assertEquals(10, giftCertificatePage1.getTotalElements());
        assertEquals(1, giftCertificatePage1.getNumber());
        assertEquals(3, giftCertificatePage1.getNumberOfElements());
        assertEquals(4, giftCertificatePage1.getTotalPages());

        assertTrue(giftCertificatePage1.hasContent());
        assertTrue(giftCertificatePage1.hasNext());
        assertFalse(giftCertificatePage1.hasPrevious());

        Page<GiftCertificate> giftCertificatePage2 = giftCertificateManager.findAll(
                giftCertificatePage1.getNextPageable());
        assertNotNull(giftCertificatePage2);
        assertEquals(10, giftCertificatePage2.getTotalElements());
        assertEquals(2, giftCertificatePage2.getNumber());
        assertEquals(3, giftCertificatePage2.getNumberOfElements());
        assertEquals(4, giftCertificatePage2.getTotalPages());

        assertTrue(giftCertificatePage2.hasContent());
        assertTrue(giftCertificatePage2.hasNext());
        assertTrue(giftCertificatePage2.hasPrevious());

        Page<GiftCertificate> giftCertificatePage3 = giftCertificateManager.findAll(
                giftCertificatePage2.getNextPageable());
        assertNotNull(giftCertificatePage3);
        assertEquals(10, giftCertificatePage3.getTotalElements());
        assertEquals(3, giftCertificatePage3.getNumber());
        assertEquals(3, giftCertificatePage3.getNumberOfElements());
        assertEquals(4, giftCertificatePage3.getTotalPages());

        assertTrue(giftCertificatePage3.hasContent());
        assertTrue(giftCertificatePage3.hasNext());
        assertTrue(giftCertificatePage3.hasPrevious());

        Page<GiftCertificate> giftCertificatePage4 = giftCertificateManager.findAll(
                giftCertificatePage3.getNextPageable());
        assertNotNull(giftCertificatePage4);
        assertEquals(10, giftCertificatePage4.getTotalElements());
        assertEquals(4, giftCertificatePage4.getNumber());
        assertEquals(1, giftCertificatePage4.getNumberOfElements());
        assertEquals(4, giftCertificatePage4.getTotalPages());

        assertTrue(giftCertificatePage4.hasContent());
        assertFalse(giftCertificatePage4.hasNext());
        assertTrue(giftCertificatePage4.hasPrevious());

        Page<GiftCertificate> giftCertificatePage5 = giftCertificateManager.findAll(
                giftCertificatePage4.getPreviousPageable());
        assertNotNull(giftCertificatePage5);
        assertEquals(10, giftCertificatePage5.getTotalElements());
        assertEquals(3, giftCertificatePage5.getNumber());
        assertEquals(3, giftCertificatePage5.getNumberOfElements());
        assertEquals(4, giftCertificatePage5.getTotalPages());

        assertTrue(giftCertificatePage5.hasContent());
        assertTrue(giftCertificatePage5.hasNext());
        assertTrue(giftCertificatePage5.hasPrevious());
    }

    @Nested
    public class FindAll {
        @BeforeEach
        public void init() {
            giftCertificateRepository.deleteAll();
        }

        @Test
        public void shouldThrowWhenFindAllGiftCertificates() {
            String errorMessage = environment.getProperty("giftCertificate.message.allNotFound");
            GiftCertificateNotFoundException ex = assertThrows(GiftCertificateNotFoundException.class, () -> {
                giftCertificateManager.findAll(new PageRequest(DEFAULT_PAGE_NUMBER, DEFAULT_PAGE_SIZE));
            });
            assertEquals(errorMessage, ex.getMessage());
        }
    }

    @Test
    public void shouldSaveGiftCertificate() {
        GiftCertificate giftCertificate = new GiftCertificate();
        giftCertificate.setName("GC-eleven");
        giftCertificate.setDescription("gift certificate eleven for test");
        giftCertificate.setPrice(111.11);
        giftCertificate.setDuration(11);
        Tag tag = new Tag();
        tag.setName("dell");
        giftCertificate.setTags(Collections.singletonList(tag));

        GiftCertificate savedGiftCertificate = giftCertificateManager.saveGiftCertificate(giftCertificate);
        assertNotNull(savedGiftCertificate);
        assertEquals(11, giftCertificate.getId());
        assertEquals(giftCertificate.getName(), savedGiftCertificate.getName());
        assertEquals(giftCertificate.getDescription(), savedGiftCertificate.getDescription());
        assertEquals(giftCertificate.getPrice(), savedGiftCertificate.getPrice());
        assertEquals(giftCertificate.getDuration(), savedGiftCertificate.getDuration());
        assertEquals(1, giftCertificate.getTags().size());
    }

    @Test
    public void shouldThrowExceptionWhenSaveGiftCertificate() {
        GiftCertificate giftCertificate = new GiftCertificate();
        giftCertificate.setName("GC-one");
        giftCertificate.setDescription("gift certificate eleven for test");
        giftCertificate.setPrice(111.11);
        giftCertificate.setDuration(11);
        Tag tag = new Tag();
        tag.setName("dell");
        giftCertificate.setTags(Collections.singletonList(tag));
        String errorMessage = String.format(environment.getProperty("giftCertificate.message.duplicate"),
                giftCertificate.getName());
        GiftCertificateDuplicateException ex = assertThrows(GiftCertificateDuplicateException.class, () -> {
            giftCertificateManager.saveGiftCertificate(giftCertificate);
        });
        assertEquals(errorMessage, ex.getMessage());
    }

    @Test
    public void shouldFindGiftCertificatesByTagId() {
        long tagId = 13L;
        Page<GiftCertificate> giftCertificatePage1 = giftCertificateManager.findGiftCertificatesByTagId(
                tagId, new PageRequest(DEFAULT_PAGE_NUMBER, DEFAULT_PAGE_SIZE));
        assertNotNull(giftCertificatePage1);
        assertEquals(4, giftCertificatePage1.getTotalElements());
        assertEquals(2, giftCertificatePage1.getTotalPages());
        assertEquals(3, giftCertificatePage1.getNumberOfElements());
        assertEquals(1, giftCertificatePage1.getNumber());

        assertTrue(giftCertificatePage1.hasContent());
        assertTrue(giftCertificatePage1.hasNext());
        assertFalse(giftCertificatePage1.hasPrevious());

        Page<GiftCertificate> giftCertificatePage2 = giftCertificateManager.findGiftCertificatesByTagId(
                tagId, giftCertificatePage1.getNextPageable());
        assertNotNull(giftCertificatePage2);
        assertEquals(4, giftCertificatePage2.getTotalElements());
        assertEquals(2, giftCertificatePage2.getTotalPages());
        assertEquals(1, giftCertificatePage2.getNumberOfElements());
        assertEquals(2, giftCertificatePage2.getNumber());

        assertTrue(giftCertificatePage2.hasContent());
        assertFalse(giftCertificatePage2.hasNext());
        assertTrue(giftCertificatePage2.hasPrevious());

        Page<GiftCertificate> giftCertificatePage3 = giftCertificateManager.findGiftCertificatesByTagId(
                tagId, giftCertificatePage2.getPreviousPageable());
        assertNotNull(giftCertificatePage2);
        assertEquals(4, giftCertificatePage3.getTotalElements());
        assertEquals(2, giftCertificatePage3.getTotalPages());
        assertEquals(3, giftCertificatePage3.getNumberOfElements());
        assertEquals(1, giftCertificatePage3.getNumber());

        assertTrue(giftCertificatePage3.hasContent());
        assertTrue(giftCertificatePage3.hasNext());
        assertFalse(giftCertificatePage3.hasPrevious());
    }

    @Test
    public void shouldThrowExceptionWhenGiftCertificatesByTagId() {
        long tagId = 5L;
        String errorMessage = String.format(environment.getProperty("giftCertificate.message.notFoundByTagId"), tagId);
        GiftCertificateNotFoundException ex = assertThrows(GiftCertificateNotFoundException.class, () -> {
            giftCertificateManager.findGiftCertificatesByTagId(
                    tagId, new PageRequest(DEFAULT_PAGE_NUMBER, DEFAULT_PAGE_SIZE));
        });
        assertEquals(errorMessage, ex.getMessage());
    }
}