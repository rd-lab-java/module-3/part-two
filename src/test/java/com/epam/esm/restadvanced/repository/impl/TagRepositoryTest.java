package com.epam.esm.restadvanced.repository.impl;

import com.epam.esm.restadvanced.common.api.model.Page;
import com.epam.esm.restadvanced.common.api.model.PageRequest;
import com.epam.esm.restadvanced.config.TestH2Config;
import com.epam.esm.restadvanced.repository.GiftCertificateRepository;
import com.epam.esm.restadvanced.repository.OrderRepository;
import com.epam.esm.restadvanced.repository.TagRepository;
import com.epam.esm.restadvanced.repository.UserRepository;
import com.epam.esm.restadvanced.repository.entity.GiftCertificate;
import com.epam.esm.restadvanced.repository.entity.Order;
import com.epam.esm.restadvanced.repository.entity.User;
import com.epam.esm.restadvanced.service.exception.TagDuplicateException;
import com.epam.esm.restadvanced.service.exception.TagNotFoundException;
import com.epam.esm.restadvanced.repository.entity.Tag;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes =  {TestH2Config.class})
@ActiveProfiles("test-h2")
class TagRepositoryTest {
    private final static int DEFAULT_PAGE_NUMBER = 1;
    private final static int DEFAULT_PAGE_SIZE = 3;
    @Autowired
    GiftCertificateRepository giftCertificateRepository;
    @Autowired
    TagRepository tagRepository;
    @Autowired
    Environment environment;

    @AfterEach
    public void tearDown() {
        giftCertificateRepository.deleteAll();
    }

    @BeforeEach
    public void init() {
        Tag tagOne = new Tag();
        tagOne.setName("adorable");

        Tag tagTwo = new Tag();
        tagTwo.setName("rainy");

        Tag tagThree = new Tag();
        tagThree.setName("sunny");

        Tag tagFour = new Tag();
        tagFour.setName("haze");

        tagRepository.saveTag(tagTwo);
        tagRepository.saveTag(tagThree);
        tagRepository.saveTag(tagOne);
        tagRepository.saveTag(tagFour);
    }

    @Test
    public void shouldSaveTag() {
        Tag tagFour = new Tag();
        tagFour.setName("snowy");

        Tag savedTagFour = tagRepository.saveTag(tagFour);
        assertNotNull(savedTagFour);
        assertEquals(tagFour.getName(), savedTagFour.getName());
    }

    @Test
    public void shouldThrowExceptionWhenSaveTag() {
        Tag tag = new Tag();
        tag.setName("rainy");
        String errorMessage = String.format(environment.getProperty("tag.message.duplicate"), tag.getName());
        TagDuplicateException ex = assertThrows(TagDuplicateException.class, () -> {
            tagRepository.saveTag(tag);
        });

        assertEquals(errorMessage, ex.getMessage());
    }

    @Test
    public void shouldFindTagByName() {
        Tag foundTag = tagRepository.findTagByName("rainy");
        assertNotNull(foundTag);
        assertEquals("rainy", foundTag.getName());
    }

    @Test
    public void shouldThrowNotFoundExceptionWhenFindTagByName() {
        String tagName = "geeks";
        String errorMessage = String.format(environment.getProperty("tag.message.notFoundByName"), tagName);
        TagNotFoundException ex = assertThrows(TagNotFoundException.class, () -> {
            tagRepository.findTagByName(tagName);
        });
        assertEquals(errorMessage, ex.getMessage());
    }

    @Test
    public void shouldFindTagsByGiftCertificateId() {
        GiftCertificate gcOne = new GiftCertificate();
        gcOne.setName("GC-one");
        gcOne.setDescription("gift certificate one for integration test");
        gcOne.setPrice(10.5);
        gcOne.setDuration(10);

        Tag tagOne = tagRepository.findTagByName("rainy");
        Tag tagTwo = tagRepository.findTagByName("adorable");
        Tag tagThree = tagRepository.findTagByName("sunny");
        Tag tagFour = tagRepository.findTagByName("haze");
        gcOne.setTags(Arrays.asList(tagOne, tagTwo, tagThree, tagFour));
        GiftCertificate savedGiftCertificate = giftCertificateRepository.saveGiftCertificate(gcOne);
        Page<Tag> tagPage1 = tagRepository.findTagsByGiftCertificateId(
                savedGiftCertificate.getId(), new PageRequest(DEFAULT_PAGE_NUMBER,DEFAULT_PAGE_SIZE));
        assertEquals(4, tagPage1.getTotalElements());
        assertEquals(2, tagPage1.getTotalPages());
        assertEquals(1, tagPage1.getNumber());
        assertEquals(3, tagPage1.getNumberOfElements());

        assertTrue(tagPage1.hasContent());
        assertTrue(tagPage1.hasNext());
        assertFalse(tagPage1.hasPrevious());

        Page<Tag> tagPage2 = tagRepository.findTagsByGiftCertificateId(
                savedGiftCertificate.getId(), tagPage1.getNextPageable());
        assertEquals(4, tagPage2.getTotalElements());
        assertEquals(2, tagPage2.getTotalPages());
        assertEquals(2, tagPage2.getNumber());
        assertEquals(1, tagPage2.getNumberOfElements());

        assertTrue(tagPage2.hasContent());
        assertFalse(tagPage2.hasNext());
        assertTrue(tagPage2.hasPrevious());

        Page<Tag> tagPage3 = tagRepository.findTagsByGiftCertificateId(
                savedGiftCertificate.getId(), tagPage2.getPreviousPageable());
        assertEquals(4, tagPage3.getTotalElements());
        assertEquals(2, tagPage3.getTotalPages());
        assertEquals(1, tagPage3.getNumber());
        assertEquals(3, tagPage3.getNumberOfElements());

        assertTrue(tagPage3.hasContent());
        assertTrue(tagPage3.hasNext());
        assertFalse(tagPage3.hasPrevious());
    }

    @Test
    public void throwExceptionWhenFindTagsByGiftCertificateId() {
        long giftCertificateId = 1L;
        String errorMessage = String.format(
                environment.getProperty("tag.message.notFoundByGiftCertificateId"), giftCertificateId);
        TagNotFoundException ex = assertThrows(TagNotFoundException.class, () -> {
            tagRepository.findTagsByGiftCertificateId(
                    giftCertificateId, new PageRequest(DEFAULT_PAGE_NUMBER, DEFAULT_PAGE_SIZE));
        });
        assertEquals(errorMessage, ex.getMessage());
    }

    @Test
    public void shouldFindAllTags() {
        Page<Tag> tagPage1 = tagRepository.findAll(new PageRequest(DEFAULT_PAGE_NUMBER, DEFAULT_PAGE_SIZE));
        assertEquals(4, tagPage1.getTotalElements());
        assertEquals(2, tagPage1.getTotalPages());
        assertEquals(1, tagPage1.getNumber());
        assertEquals(3, tagPage1.getNumberOfElements());

        assertTrue(tagPage1.hasContent());
        assertTrue(tagPage1.hasNext());
        assertFalse(tagPage1.hasPrevious());

        Page<Tag> tagPage2 = tagRepository.findAll(tagPage1.getNextPageable());
        assertEquals(4, tagPage2.getTotalElements());
        assertEquals(2, tagPage2.getTotalPages());
        assertEquals(2, tagPage2.getNumber());
        assertEquals(1, tagPage2.getNumberOfElements());

        assertTrue(tagPage2.hasContent());
        assertFalse(tagPage2.hasNext());
        assertTrue(tagPage2.hasPrevious());

        Page<Tag> tagPage3 = tagRepository.findAll(tagPage2.getPreviousPageable());
        assertEquals(4, tagPage3.getTotalElements());
        assertEquals(2, tagPage3.getTotalPages());
        assertEquals(1, tagPage3.getNumber());
        assertEquals(3, tagPage3.getNumberOfElements());

        assertTrue(tagPage3.hasContent());
        assertTrue(tagPage3.hasNext());
        assertFalse(tagPage3.hasPrevious());
    }

    @Test
    public void shouldThrowNotFoundExceptionWhenFindAll() {
        tagRepository.deleteAll();
        String errorMessage = environment.getProperty("tag.message.allNotFound");
        TagNotFoundException ex = assertThrows(TagNotFoundException.class, () -> {
            tagRepository.findAll(new PageRequest(DEFAULT_PAGE_NUMBER, DEFAULT_PAGE_SIZE));
        });

        assertEquals(errorMessage, ex.getMessage());
    }


    @Test
    public void shouldDeleteTagById() {
        long tagId = 2L;
        Tag deletedTag = tagRepository.deleteTagById(tagId);
        assertNotNull(deletedTag);
        assertEquals(tagId, deletedTag.getId());

        String errorMessage = String.format(environment.getProperty("tag.message.notFoundById"), tagId);
        TagNotFoundException ex = assertThrows(TagNotFoundException.class, ()-> {
            tagRepository.deleteTagById(deletedTag.getId());
        });
        assertEquals(errorMessage, ex.getMessage());
    }

    @Test
    public void shouldThrowNotFoundException() {
        long tagId = 5L;
        String errorMessage = String.format(environment.getProperty("tag.message.notFoundById"), tagId);
        TagNotFoundException ex = assertThrows(TagNotFoundException.class, () -> {
            tagRepository.deleteTagById(tagId);
        });
        assertEquals(errorMessage, ex.getMessage());
    }

    @Test
    public void shouldFindTagById() {
        long tagId = 3L;
        Tag tag = tagRepository.findTagById(tagId);
        assertNotNull(tag);
        assertEquals(tagId, tag.getId());
        assertEquals("adorable", tag.getName());
    }

    @Test
    public void shouldThrowNotFoundExceptionWhenFindTagById() {
        long tagId = 10L;
        String errorMessage = String.format(environment.getProperty("tag.message.notFoundById"), tagId);
        TagNotFoundException ex = assertThrows(TagNotFoundException.class, () -> {
            tagRepository.findTagById(tagId);
        });
        assertEquals(errorMessage, ex.getMessage());
    }

    @Nested
    public class TagAndGiftCertificateAndOrderAndUser {
        @Autowired
        OrderRepository orderRepository;
        @Autowired
        UserRepository userRepository;

        @BeforeEach
        public void init() {

            User user1 = new User();
            user1.setFullName("User one");
            user1.setEmail("user_one@email.com");
            user1.setPassword("user_one");
            userRepository.saveUser(user1);

            User user2 = new User();
            user2.setFullName("User two");
            user2.setEmail("user_two@email.com");
            user2.setPassword("user_two");
            userRepository.saveUser(user2);

            GiftCertificate giftCertificate1 = new GiftCertificate();
            giftCertificate1.setName("GC-one");
            giftCertificate1.setDescription("gift certificate one");
            giftCertificate1.setPrice(10.5);
            giftCertificate1.setDuration(12);
            Tag tag1 = new Tag();
            tag1.setName("new");

            Tag tag2 = new Tag();
            tag2.setName("old");

            List<Tag> tags = Arrays.asList(tag1, tag2);
            giftCertificate1.setTags(tags);

            giftCertificateRepository.saveGiftCertificate(giftCertificate1);

            GiftCertificate giftCertificate2 = new GiftCertificate();
            giftCertificate2.setName("GC-two");
            giftCertificate2.setDescription("gift certificate two");
            giftCertificate2.setPrice(12.5);
            giftCertificate2.setDuration(9);
            Tag tag4 = new Tag();
            tag4.setName("lonely");

            Tag tag5 = new Tag();
            tag5.setName("annoying");
            giftCertificate2.setTags(Arrays.asList(tag4, tag5));

            giftCertificateRepository.saveGiftCertificate(giftCertificate2);

            GiftCertificate giftCertificate3 = new GiftCertificate();
            giftCertificate3.setName("GC-three");
            giftCertificate3.setDescription("gift certificate three");
            giftCertificate3.setPrice(25.5);
            giftCertificate3.setDuration(5);
            Tag tag6 = new Tag();
            tag6.setName("disgusting");

            giftCertificate3.setTags(Collections.singletonList(tag6));

            giftCertificateRepository.saveGiftCertificate(giftCertificate3);

            Order order1 = new com.epam.esm.restadvanced.repository.entity.Order(3, user1, giftCertificate1,
                    "unpaid");
            orderRepository.saveOrder(order1);

            Order order2 = new com.epam.esm.restadvanced.repository.entity.Order(4, user2, giftCertificate2,
                    "unpaid");
            orderRepository.saveOrder(order2);

            Order order3 = new Order(5, user1, giftCertificate3, "unpaid");
            orderRepository.saveOrder(order3);
        }

        @AfterEach
        public void tearDown() {
            giftCertificateRepository.deleteAll();
            userRepository.deleteAll();
            orderRepository.deleteAll();
        }

        @Test
        public void shouldFindMostUsedTags() {
            Page<Tag> tagPage = tagRepository.findMostWidelyUsedTags(
                    new PageRequest(DEFAULT_PAGE_NUMBER, DEFAULT_PAGE_SIZE));
            assertEquals(5, tagPage.getTotalElements());
            assertEquals(3, tagPage.getNumberOfElements());
            assertEquals(2, tagPage.getTotalPages());
            assertEquals(1, tagPage.getNumber());

            assertTrue(tagPage.hasContent());
            assertTrue(tagPage.hasNext());
            assertFalse(tagPage.hasPrevious());

            Page<Tag> tagPage2 = tagRepository.findMostWidelyUsedTags(
                    tagPage.getNextPageable());
            assertEquals(5, tagPage2.getTotalElements());
            assertEquals(2, tagPage2.getNumberOfElements());
            assertEquals(2, tagPage2.getTotalPages());
            assertEquals(2, tagPage2.getNumber());

            assertTrue(tagPage2.hasContent());
            assertFalse(tagPage2.hasNext());
            assertTrue(tagPage2.hasPrevious());
        }

        @Test
        public void shouldThrowFindMostUsedTags() {
            tagRepository.deleteAll();
            String errorMessage = environment.getProperty("tag.message.allNotFound");
            TagNotFoundException ex = assertThrows(TagNotFoundException.class, () -> {
                tagRepository.findMostWidelyUsedTags(new PageRequest(DEFAULT_PAGE_NUMBER, DEFAULT_PAGE_SIZE));
            });
            assertEquals(errorMessage, ex.getMessage());
        }

        @Test
        public void shouldFindTopMostUsedTags() {
            List<Tag> topTags = tagRepository.findTopMostWidelyUsedTags(2);
            assertEquals(2, topTags.size());
        }

        @Test
        public void shouldThrowExceptionWhenFindTopMostUsedTags() {
            tagRepository.deleteAll();
            String errorMessage = environment.getProperty("tag.message.allNotFound");
            TagNotFoundException ex = assertThrows(TagNotFoundException.class, ()-> {
                tagRepository.findTopMostWidelyUsedTags(2);
            });
            assertEquals(errorMessage, ex.getMessage());
        }
    }
}