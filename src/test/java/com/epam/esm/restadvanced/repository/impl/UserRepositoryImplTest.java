package com.epam.esm.restadvanced.repository.impl;

import com.epam.esm.restadvanced.common.api.model.Page;
import com.epam.esm.restadvanced.common.api.model.PageRequest;
import com.epam.esm.restadvanced.config.TestH2Config;
import com.epam.esm.restadvanced.repository.UserRepository;
import com.epam.esm.restadvanced.repository.entity.User;
import com.epam.esm.restadvanced.service.exception.UserDuplicateException;
import com.epam.esm.restadvanced.service.exception.UserNotFoundException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes =  {TestH2Config.class})
@ActiveProfiles("test-h2")
class UserRepositoryImplTest {
    private final static int DEFAULT_PAGE_NUMBER = 1;
    private final static int DEFAULT_PAGE_SIZE = 3;
    @Autowired
    Environment environment;
    @Autowired
    UserRepository userRepository;

    @AfterEach
    public void tearDown() {
        userRepository.deleteAll();
    }

    @BeforeEach
    public void init() {

        User user1 = new User();
        user1.setFullName("User one");
        user1.setEmail("user_one@email.com");
        user1.setPassword("user_one");
        userRepository.saveUser(user1);

        User user2 = new User();
        user2.setFullName("User two");
        user2.setEmail("user_two@email.com");
        user2.setPassword("user_two");
        userRepository.saveUser(user2);

        User user3 = new User();
        user3.setFullName("User three");
        user3.setEmail("user_three@email.com");
        user3.setPassword("user_three");
        userRepository.saveUser(user3);

        User user4 = new User();
        user4.setFullName("User four");
        user4.setEmail("user_four@email.com");
        user4.setPassword("user_four");
        userRepository.saveUser(user4);
    }

    @Test
    public void shouldSaveUser() {
        User user = new User();
        user.setFullName("User five");
        user.setEmail("user_five@email.com");
        user.setPassword("user_five");
        User saveUser = userRepository.saveUser(user);
        assertEquals(user.getFullName(), saveUser.getFullName());
        assertEquals(user.getEmail(), saveUser.getEmail());
        assertEquals(5, saveUser.getId());
    }

    @Test
    public void shouldThrowExceptionWhenSaveUser() {
        User user = new User();
        user.setFullName("User four");
        user.setEmail("user_four@email.com");
        user.setPassword("user_four");
        String errorMessage = String.format(environment.getProperty("user.message.duplicate"), user.getFullName());
        UserDuplicateException ex = assertThrows(UserDuplicateException.class, ()-> {
            userRepository.saveUser(user);
        });
        assertEquals(errorMessage, ex.getMessage());
    }

    @Test
    @DisplayName("find all")
    public void shouldFindAll() {
        Page<User> userPage = userRepository.findAll(new PageRequest(DEFAULT_PAGE_NUMBER, DEFAULT_PAGE_SIZE));
        assertEquals(4, userPage.getTotalElements());
        assertEquals(2, userPage.getTotalPages());
        assertEquals(3, userPage.getNumberOfElements());
        assertEquals(1, userPage.getNumber());

        assertTrue(userPage.hasContent());
        assertTrue(userPage.hasNext());
        assertFalse(userPage.hasPrevious());

        Page<User> userPage2 = userRepository.findAll(userPage.getNextPageable());
        assertEquals(4, userPage2.getTotalElements());
        assertEquals(2, userPage2.getTotalPages());
        assertEquals(1, userPage2.getNumberOfElements());
        assertEquals(2, userPage2.getNumber());

        assertTrue(userPage2.hasContent());
        assertFalse(userPage2.hasNext());
        assertTrue(userPage2.hasPrevious());

        Page<User> userPage3 = userRepository.findAll(userPage2.getPreviousPageable());
        assertEquals(4, userPage3.getTotalElements());
        assertEquals(2, userPage3.getTotalPages());
        assertEquals(3, userPage3.getNumberOfElements());
        assertEquals(1, userPage3.getNumber());

        assertTrue(userPage3.hasContent());
        assertTrue(userPage3.hasNext());
        assertFalse(userPage3.hasPrevious());
    }

    @Test
    public void shouldThrowExceptionWhenFindAll() {
        userRepository.deleteAll();
        String errorMessage = environment.getProperty("user.message.allNotFound");
        UserNotFoundException ex = assertThrows(UserNotFoundException.class, ()-> {
            userRepository.findAll(new PageRequest(DEFAULT_PAGE_NUMBER, DEFAULT_PAGE_SIZE));
        });
        assertEquals(errorMessage, ex.getMessage());
    }

    @Test
    public void shouldFindUserById() {
        long userId = 2L;
        User user = userRepository.findUserById(userId);
        assertEquals(userId, user.getId());
        assertEquals("User two", user.getFullName());
        assertEquals("user_two@email.com", user.getEmail());
    }

    @Test
    public void shouldThrowExceptionWhenFindUserById() {
        long userId = 5L;
        String errorMessage = String.format(environment.getProperty("user.message.notFound"), userId);
        UserNotFoundException ex = assertThrows(UserNotFoundException.class, ()-> {
            userRepository.findUserById(userId);
        });
        assertEquals(errorMessage, ex.getMessage());
    }

    @Test
    public void shouldDeleteUserById() {
        long userId = 1L;
        User user = userRepository.deleteUserById(userId);
        assertEquals(userId, user.getId());

        String errorMessage = String.format(environment.getProperty("user.message.notFound"), userId);
        UserNotFoundException ex = assertThrows(UserNotFoundException.class, ()->{
            userRepository.deleteUserById(userId);
        });
        assertEquals(errorMessage, ex.getMessage());
    }

    @Test
    public void shouldThrowExceptionWhenDeleteUser() {
        long userId = 6L;
        String errorMessage = String.format(environment.getProperty("user.message.notFound"), userId);
        UserNotFoundException ex = assertThrows(UserNotFoundException.class, ()->{
            userRepository.deleteUserById(userId);
        });
        assertEquals(errorMessage, ex.getMessage());
    }
}