package com.epam.esm.restadvanced.repository.impl;

import com.epam.esm.restadvanced.common.api.model.Page;
import com.epam.esm.restadvanced.common.api.model.PageRequest;
import com.epam.esm.restadvanced.config.TestH2Config;
import com.epam.esm.restadvanced.repository.GiftCertificateRepository;
import com.epam.esm.restadvanced.repository.OrderRepository;
import com.epam.esm.restadvanced.repository.UserRepository;
import com.epam.esm.restadvanced.repository.entity.GiftCertificate;
import com.epam.esm.restadvanced.repository.entity.Order;
import com.epam.esm.restadvanced.repository.entity.Tag;
import com.epam.esm.restadvanced.repository.entity.User;
import com.epam.esm.restadvanced.service.exception.OrderDuplicateException;
import com.epam.esm.restadvanced.service.exception.OrderNotFoundException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.verify;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes =  {TestH2Config.class})
@ActiveProfiles("test-h2")
class OrderRepositoryImplTest {
    private final static int DEFAULT_PAGE_NUMBER = 1;
    private final static int DEFAULT_PAGE_SIZE = 3;
    @Autowired
    Environment environment;
    @Autowired
    OrderRepository orderRepository;
    @Autowired
    UserRepository userRepository;
    @Autowired
    GiftCertificateRepository giftCertificateRepository;

    @BeforeEach
    public void init() {
        User user1 = new User();
        user1.setFullName("User one");
        user1.setEmail("user_one@email.com");
        user1.setPassword("user_one");
        userRepository.saveUser(user1);

        User user2 = new User();
        user2.setFullName("User two");
        user2.setEmail("user_two@email.com");
        user2.setPassword("user_two");
        userRepository.saveUser(user2);

        GiftCertificate giftCertificate1 = new GiftCertificate();
        giftCertificate1.setName("GC-one");
        giftCertificate1.setDescription("gift certificate one");
        giftCertificate1.setPrice(10.5);
        giftCertificate1.setDuration(12);
        Tag tag1 = new Tag();
        tag1.setName("new");

        Tag tag2 = new Tag();
        tag2.setName("old");

        List<Tag> tags = Arrays.asList(tag1, tag2);
        giftCertificate1.setTags(tags);

        giftCertificateRepository.saveGiftCertificate(giftCertificate1);

        GiftCertificate giftCertificate2 = new GiftCertificate();
        giftCertificate2.setName("GC-two");
        giftCertificate2.setDescription("gift certificate two");
        giftCertificate2.setPrice(12.5);
        giftCertificate2.setDuration(9);
        Tag tag3 = new Tag();
        tag3.setName("lonely");

        Tag tag4 = new Tag();
        tag4.setName("annoying");
        giftCertificate2.setTags(Arrays.asList(tag3, tag4));

        giftCertificateRepository.saveGiftCertificate(giftCertificate2);

        GiftCertificate giftCertificate3 = new GiftCertificate();
        giftCertificate3.setName("GC-three");
        giftCertificate3.setDescription("gift certificate three");
        giftCertificate3.setPrice(25.5);
        giftCertificate3.setDuration(5);
        Tag tag5 = new Tag();
        tag5.setName("disgusting");

        giftCertificate3.setTags(Collections.singletonList(tag5));

        giftCertificateRepository.saveGiftCertificate(giftCertificate3);

        Order order1 = new Order(3, user1, giftCertificate1, "unpaid");
        order1.setTotalCost(order1.getUnitPrice() * order1.getQuantity());
        orderRepository.saveOrder(order1);

        Order order2 = new Order(4, user2, giftCertificate2, "unpaid");
        order2.setTotalCost(order2.getUnitPrice() * order2.getQuantity());
        orderRepository.saveOrder(order2);

        Order order3 = new Order(5, user1, giftCertificate3, "unpaid");
        order3.setTotalCost(order3.getUnitPrice() * order3.getQuantity());
        orderRepository.saveOrder(order3);
    }

    @AfterEach
    public void tearDown() {
        giftCertificateRepository.deleteAll();
        userRepository.deleteAll();
        orderRepository.deleteAll();
    }

    @Test
    public void shouldSaveOrder() {
        long giftCertificateId = 2L;
        GiftCertificate giftCertificate = giftCertificateRepository.findGiftCertificateById(giftCertificateId);
        long userId = 1L;
        User user = userRepository.findUserById(userId);

        int quantity = 10;
        Order order = new Order(quantity, user, giftCertificate, "unpaid");
        order.setTotalCost(order.getUnitPrice() * order.getQuantity());
        Order saveOrder = orderRepository.saveOrder(order);

        assertEquals(giftCertificateId, saveOrder.getGiftCertificate().getId());
        assertEquals(userId, saveOrder.getOwner().getId());
        assertEquals(quantity, order.getQuantity());
        assertEquals(quantity * giftCertificate.getPrice(), order.getTotalCost());
    }

    @Test
    public void shouldThrowExceptionWhenSaveOrder() {
        long giftCertificateId = 2L;
        GiftCertificate giftCertificate = giftCertificateRepository.findGiftCertificateById(giftCertificateId);
        long userId = 2L;
        User user = userRepository.findUserById(userId);

        int quantity = 10;

        String errorMessage = String.format(
                environment.getProperty("order.message.duplicate"), userId, giftCertificateId);
        Order order = new Order(quantity, user, giftCertificate, "unpaid");
        order.setTotalCost(order.getUnitPrice() * order.getQuantity());
        OrderDuplicateException ex = assertThrows(OrderDuplicateException.class, ()-> {
            orderRepository.saveOrder(order);
        });

        assertEquals(errorMessage, ex.getMessage());
    }

    @Test
    public void shouldFindOrderById() {
        long orderId = 3L;
        Order order = orderRepository.findOrderById(orderId);
        assertEquals(orderId, order.getId());
        assertEquals(1L, order.getOwner().getId());
        assertEquals(3L, order.getGiftCertificate().getId());
    }

    @Test
    public void shouldThrowExceptionWhenFindOrderById() {
        long orderId = 4L;
        String errorMessage = String.format(environment.getProperty("order.message.notFoundById"), orderId);
        OrderNotFoundException exception = assertThrows(OrderNotFoundException.class, () -> {
            orderRepository.findOrderById(orderId);
        });
        assertEquals(errorMessage, exception.getMessage());
    }

    @Test
    public void findOrdersByGiftCertificateId() {
        long giftCertificateId = 1L;
        GiftCertificate giftCertificate = giftCertificateRepository.findGiftCertificateById(giftCertificateId);
        long userId = 2L;
        User user = userRepository.findUserById(userId);
        int quantity = 10;
        Order order = new Order(quantity, user, giftCertificate, "unpaid");
        orderRepository.saveOrder(order);

        Page<Order> orderPage = orderRepository.findOrdersByGiftCertificateId(
                giftCertificateId, new PageRequest(DEFAULT_PAGE_NUMBER, DEFAULT_PAGE_SIZE));

        assertEquals(2, orderPage.getTotalElements());
        assertEquals(1, orderPage.getNumber());
        assertEquals(1, orderPage.getTotalPages());
        assertEquals(2, orderPage.getNumberOfElements());

        assertTrue(orderPage.hasContent());
        assertFalse(orderPage.hasNext());
        assertFalse(orderPage.hasPrevious());
    }

    @Test
    public void shouldThrowExceptionWhenFindGiftCertificateId() {
        long giftCertificateId = 4L;
        String errorMessage = String.format(
                environment.getProperty("order.message.notFoundByGiftCertificateId"), giftCertificateId);
        OrderNotFoundException ex = assertThrows(OrderNotFoundException.class, () -> {
            orderRepository.findOrdersByGiftCertificateId(
                    giftCertificateId, new PageRequest(DEFAULT_PAGE_NUMBER, DEFAULT_PAGE_SIZE));
        });
        assertEquals(errorMessage, ex.getMessage());
    }

    @Test
    public void shouldFindOrdersByOwnerId() {
        long userId = 1L;
        Page<Order> orderPage = orderRepository.findOrdersByOwnerId(
                userId, new PageRequest(DEFAULT_PAGE_NUMBER, DEFAULT_PAGE_SIZE));

        assertEquals(2, orderPage.getTotalElements());
        assertEquals(1, orderPage.getNumber());
        assertEquals(1, orderPage.getTotalPages());
        assertEquals(2, orderPage.getNumberOfElements());

        assertTrue(orderPage.hasContent());
        assertFalse(orderPage.hasNext());
        assertFalse(orderPage.hasPrevious());
    }

    @Test
    public void shouldThrowExceptionWhenFindOwnerId() {
        long ownerId = 3L;
        String errorMessage = String.format(environment.getProperty("order.message.notFoundByOwnerId"), ownerId);
        OrderNotFoundException ex = assertThrows(OrderNotFoundException.class, () -> {
            orderRepository.findOrdersByOwnerId(ownerId, new PageRequest(DEFAULT_PAGE_NUMBER, DEFAULT_PAGE_SIZE));
        });

        assertEquals(errorMessage, ex.getMessage());
    }
}