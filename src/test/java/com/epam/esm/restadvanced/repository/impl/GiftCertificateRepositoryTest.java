package com.epam.esm.restadvanced.repository.impl;

import com.epam.esm.restadvanced.common.api.model.Page;
import com.epam.esm.restadvanced.common.api.model.PageRequest;
import com.epam.esm.restadvanced.config.TestH2Config;
import com.epam.esm.restadvanced.repository.GiftCertificateRepository;
import com.epam.esm.restadvanced.repository.TagRepository;
import com.epam.esm.restadvanced.repository.entity.GiftCertificate;
import com.epam.esm.restadvanced.service.exception.GiftCertificateDuplicateException;
import com.epam.esm.restadvanced.repository.entity.Tag;
import com.epam.esm.restadvanced.service.exception.GiftCertificateNotFoundException;
import com.epam.esm.restadvanced.service.exception.GiftCertificateNotModifiedException;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes =  {TestH2Config.class})
@ActiveProfiles("test-h2")
class GiftCertificateRepositoryTest {
    private final static int DEFAULT_PAGE_NUMBER = 1;
    private final static int DEFAULT_PAGE_SIZE = 3;
    @Autowired
    GiftCertificateRepository giftCertificateRepository;
    @Autowired
    TagRepository tagRepository;
    @Autowired
    Environment environment;

    @BeforeEach
    public void init() {
        GiftCertificate gc1 = new GiftCertificate();
        gc1.setName("GC-one");
        gc1.setDescription("gift certificate one for integration test with tag1");
        gc1.setPrice(10.5);
        gc1.setDuration(10);
        Tag tag1 = new Tag();
        tag1.setName("dell");
        gc1.setTags(Collections.singletonList(tag1));


        GiftCertificate gc2 = new GiftCertificate();
        gc2.setName("GC-two");
        gc2.setDescription("gift certificate two for integration test with tag1 and tag2");
        gc2.setPrice(12.5);
        gc2.setDuration(20);
        Tag tag2 = new Tag();
        tag2.setName("lenovo");
        gc2.setTags(Arrays.asList(tag1, tag2));

        GiftCertificate gc3 = new GiftCertificate();
        gc3.setName("GC-three");
        gc3.setDescription("gift certificate three for integration test with tag1, tag2 and tag3");
        gc3.setPrice(13.5);
        gc3.setDuration(30);
        Tag tag3 = new Tag();
        tag3.setName("asus");
        gc3.setTags(Arrays.asList(tag1, tag2, tag3));

        GiftCertificate gc5 = new GiftCertificate();
        gc5.setName("GC-five");
        gc5.setDescription("gift certificate five for integration test with tag2 and tag3");
        gc5.setPrice(15.5);
        gc5.setDuration(5);
        gc5.setTags(Arrays.asList(tag2, tag3));

        GiftCertificate gc6 = new GiftCertificate();
        gc6.setName("GC-six");
        gc6.setDescription("gift certificate six for integration test with tag2");
        gc6.setPrice(26.5);
        gc6.setDuration(16);
        gc6.setTags(Collections.singletonList(tag2));

        GiftCertificate gc7 = new GiftCertificate();
        gc7.setName("GC-seven");
        gc7.setDescription("gift certificate seven for integration test with tag3");
        gc7.setPrice(7.5);
        gc7.setDuration(7);
        gc7.setTags(Collections.singletonList(tag3));

        GiftCertificate gc8 = new GiftCertificate();
        gc8.setName("GC-eight");
        gc8.setDescription("gift certificate eight for integration test  with tag2 and tag3");
        gc8.setPrice(18.5);
        gc8.setDuration(8);
        gc8.setTags(Arrays.asList(tag2, tag3));

        GiftCertificate gc9 = new GiftCertificate();
        gc9.setName("GC-nine");
        gc9.setDescription("gift certificate nine for integration test with tag2 and tag3");
        gc9.setPrice(9.5);
        gc9.setDuration(19);
        gc9.setTags(Arrays.asList(tag2, tag3));

        giftCertificateRepository.saveGiftCertificate(gc1);
        giftCertificateRepository.saveGiftCertificate(gc2);
        giftCertificateRepository.saveGiftCertificate(gc3);
        giftCertificateRepository.saveGiftCertificate(gc5);
        giftCertificateRepository.saveGiftCertificate(gc6);
        giftCertificateRepository.saveGiftCertificate(gc7);
        giftCertificateRepository.saveGiftCertificate(gc8);
        giftCertificateRepository.saveGiftCertificate(gc9);
    }

    @AfterEach
    public void tearDown() {
        giftCertificateRepository.deleteAll();
    }

    @Test
    @DisplayName("Save gift certificate")
    public void shouldSaveGiftCertificate() {
        GiftCertificate newGC = new GiftCertificate();
        newGC.setName("GC-four");
        newGC.setDescription("gift certificate four for integration test");
        newGC.setPrice(14.5);
        newGC.setDuration(40);
        Tag tag1 = new Tag();
        tag1.setName("apple");
        newGC.setTags(Collections.singletonList(tag1));

        GiftCertificate saveGC1 = giftCertificateRepository.saveGiftCertificate(newGC);
        assertNotNull(saveGC1);
        assertEquals(1, saveGC1.getTags().size());

    }

    @Test
    @DisplayName("throw duplicate exception when inserting a data with the same name")
    public void shouldThrowExceptionWhenSaveGiftCertificate() {
        String errorMessage = "Gift Certificate with 'GC-one' name already existed.";

        GiftCertificate giftCertificate = new GiftCertificate();
        giftCertificate.setName("GC-one");
        giftCertificate.setDescription("gift certificate one for integration test");
        giftCertificate.setPrice(10.5);
        giftCertificate.setDuration(10);
        Tag tag = new Tag();
        tag.setName("lenovo");
        giftCertificate.setTags(Collections.singletonList(tag));

        GiftCertificateDuplicateException ex = assertThrows(GiftCertificateDuplicateException.class, () -> {
            giftCertificateRepository.saveGiftCertificate(giftCertificate);
        });
        assertEquals(errorMessage, ex.getMessage());
    }

    @Test
    @DisplayName("Find all gift certificates")
    public void shouldFindAllGiftCertificates() {
        Page<GiftCertificate> gcPage1 = giftCertificateRepository.
                findAll(new PageRequest(DEFAULT_PAGE_NUMBER, DEFAULT_PAGE_SIZE));
        assertEquals(8, gcPage1.getTotalElements());
        assertEquals(3, gcPage1.getTotalPages());
        assertEquals(1, gcPage1.getNumber());
        assertEquals(3, gcPage1.getNumberOfElements());

        assertTrue(gcPage1.hasContent());
        assertTrue(gcPage1.hasNext());
        assertFalse(gcPage1.hasPrevious());

        Page<GiftCertificate> gcPage2 = giftCertificateRepository.
                findAll(gcPage1.getNextPageable());
        assertEquals(8, gcPage2.getTotalElements());
        assertEquals(3, gcPage2.getTotalPages());
        assertEquals(2, gcPage2.getNumber());
        assertEquals(3, gcPage2.getNumberOfElements());

        assertTrue(gcPage2.hasContent());
        assertTrue(gcPage2.hasNext());
        assertTrue(gcPage2.hasPrevious());

        Page<GiftCertificate> gcPage3 = giftCertificateRepository.
                findAll(gcPage2.getNextPageable());
        assertEquals(8, gcPage3.getTotalElements());
        assertEquals(3, gcPage3.getTotalPages());
        assertEquals(3, gcPage3.getNumber());
        assertEquals(2, gcPage3.getNumberOfElements());

        assertTrue(gcPage3.hasContent());
        assertFalse(gcPage3.hasNext());
        assertTrue(gcPage3.hasPrevious());

        Page<GiftCertificate> gcPage4 = giftCertificateRepository.
                findAll(gcPage3.getPreviousPageable());
        assertEquals(8, gcPage4.getTotalElements());
        assertEquals(3, gcPage4.getTotalPages());
        assertEquals(2, gcPage4.getNumber());
        assertEquals(3, gcPage4.getNumberOfElements());

        assertTrue(gcPage4.hasContent());
        assertTrue(gcPage4.hasNext());
        assertTrue(gcPage4.hasPrevious());
    }

    @Test
    @DisplayName("Should throw exception when find all")
    public void shouldThrowExceptionWhenFindAll() {
        giftCertificateRepository.deleteAll();
        String errorMessage = environment.getProperty("giftCertificate.message.allNotFound");
        GiftCertificateNotFoundException ex = assertThrows(GiftCertificateNotFoundException.class, () -> {
            giftCertificateRepository.findAll(new PageRequest(DEFAULT_PAGE_NUMBER, DEFAULT_PAGE_SIZE));
        });

        assertEquals(errorMessage, ex.getMessage());
    }


    @Test
    @DisplayName("Find gift certificate by id")
    public void shouldFindGiftCertificateById() {
        GiftCertificate giftCertificate = giftCertificateRepository.findGiftCertificateById(2L);
        assertNotNull(giftCertificate, "GiftCertificate should not be null");
        assertEquals(2L, giftCertificate.getId());
        assertEquals("GC-two", giftCertificate.getName());
        assertEquals("gift certificate two for integration test with tag1 and tag2",
                giftCertificate.getDescription());
        assertEquals(12.5, giftCertificate.getPrice());
        assertEquals(20, giftCertificate.getDuration());
    }

    @Test
    @DisplayName("Throw Exception when find gift certificate by id")
    public void shouldThrowExceptionWhenGiftCertificateById() {
        long giftCertificateId = 19L;
        String errorMessage = String.format(
                environment.getProperty("giftCertificate.message.notFound"), giftCertificateId);
        GiftCertificateNotFoundException ex = assertThrows(GiftCertificateNotFoundException.class, () -> {
            giftCertificateRepository.findGiftCertificateById(giftCertificateId);
        });
        assertEquals(errorMessage, ex.getMessage());
    }

    @Test
    @DisplayName("Update gift certificate by id")
    public void shouldUpdateGiftCertificateById() {
        GiftCertificate gc1 = giftCertificateRepository.findGiftCertificateById(1);
        gc1.setName("updated gc1");
        gc1.setDescription("updated gift certificate by id in integration test");
        Tag tag = new Tag();
        tag.setName("acer");
        gc1.setTags(Collections.singletonList(tag));

        GiftCertificate updatedGC = giftCertificateRepository.updateGiftCertificateById(1, gc1);
        assertNotNull(updatedGC, "Updated gift certificate should be not null");
        assertEquals(gc1.getName(), updatedGC.getName());
        assertEquals(gc1.getDescription(), updatedGC.getDescription());
        assertEquals(tag.getName(), ((List<Tag>) updatedGC.getTags()).get(0).getName());
    }

    @Test
    @DisplayName("Update gift certificate by adding tag by id")
    public void shouldUpdateGiftCertificateWithTagById() {
        GiftCertificate gc1 = giftCertificateRepository.findGiftCertificateById(1);
        Tag tag = new Tag();
        tag.setName("asus");
        gc1.setTags(Collections.singletonList(tag));

        GiftCertificate updatedGC = giftCertificateRepository.updateGiftCertificateById(1, gc1);
        assertNotNull(updatedGC, "Updated gift certificate should be not null");
        assertEquals(tag.getName(), ((List<Tag>) updatedGC.getTags()).get(0).getName());
    }

    @Test
    @DisplayName("Throw not modified exception when update gift certificate by id")
    public void shouldThrowGiftCertificateNotModifiedException() {
        long giftCertificateId = 1L;
        String errorMessage = String.format(
                environment.getProperty("giftCertificate.message.notModified"), giftCertificateId);
        GiftCertificate gc1 = giftCertificateRepository.findGiftCertificateById(giftCertificateId);
        GiftCertificateNotModifiedException ex = Assertions.assertThrows(GiftCertificateNotModifiedException.class, () -> {
            giftCertificateRepository.updateGiftCertificateById(giftCertificateId,  gc1);
        });

        assertEquals(errorMessage, ex.getMessage());
    }

    @Test
    @DisplayName("Throw not found exception when update gift certificate by id")
    public void shouldThrowGiftCertificateNotFoundException() {
        String errorMessage = String.format(environment.getProperty("giftCertificate.message.notFound"), 9L);
        GiftCertificate gc1 = giftCertificateRepository.findGiftCertificateById(1L);
        gc1.setName("new name for gc1");
        GiftCertificateNotFoundException ex = Assertions.assertThrows(GiftCertificateNotFoundException.class, () -> {
            giftCertificateRepository.updateGiftCertificateById(9L,  gc1);
        });

        assertEquals(errorMessage, ex.getMessage());
    }

    @Test
    @DisplayName("Delete gift certificate by id")
    public void shouldDeleteGiftCertificateById() {
        long giftCertificateId = 2L;
        GiftCertificate gc = giftCertificateRepository.deleteGiftCertificateById(giftCertificateId);
        assertEquals(giftCertificateId, gc.getId());

        String errorMessage = String.format(
                environment.getProperty("giftCertificate.message.notFound"), giftCertificateId);
        GiftCertificateNotFoundException ex = Assertions.assertThrows(GiftCertificateNotFoundException.class, () -> {
            giftCertificateRepository.findGiftCertificateById(gc.getId());
        });

        assertEquals(errorMessage, ex.getMessage());
    }

    @Test
    @DisplayName("Throw exception when delete gift certificate by id")
    public void shouldThrowExceptionWhenDeleteGiftCertificateById() {
        long giftCertificateId = 1L;
        String errorMessage = String.format(
                environment.getProperty("giftCertificate.message.notFound"), giftCertificateId);
        giftCertificateRepository.deleteGiftCertificateById(giftCertificateId);
        GiftCertificateNotFoundException ex = Assertions.assertThrows(GiftCertificateNotFoundException.class, () -> {
            giftCertificateRepository.deleteGiftCertificateById(giftCertificateId);
        });
        assertEquals(errorMessage, ex.getMessage());
    }

    @Test
    @DisplayName("Save gift certificate and tag")
    public void shouldSaveGiftCertificateAndTag() {
        GiftCertificate giftCertificate = new GiftCertificate();
        giftCertificate.setName("gc-four");
        giftCertificate.setDescription("Gift certificate number 4");
        giftCertificate.setPrice(14.5);
        giftCertificate.setDuration(40);

        GiftCertificate savedGiftCertificate = giftCertificateRepository.saveGiftCertificate(giftCertificate);
        assertNotNull(savedGiftCertificate, "Saved gift repository should not be null");
        assertEquals(giftCertificate.getName(), savedGiftCertificate.getName());

        Tag foundTag = tagRepository.findTagByName("lenovo");
        assertNotNull(foundTag, "Saved tag should not be null");
        Assertions.assertTrue(giftCertificateRepository.saveGiftCertificateAndTag(savedGiftCertificate, foundTag));
    }

    @Test
    @DisplayName("Find gift certificates and tags by tag name and sorted with date")
    public void shouldFindGiftCertificatesAndTagsByTagNameAndSortByDate() {
        String sort = "gift_certificate.create_date";
        String tagName = "'lenovo', 'asus'";

        Page<GiftCertificate> giftCertificates = giftCertificateRepository.
                findSortedGiftCertificatesAndTagsByTagNames(
                        tagName, 2, sort, new PageRequest(DEFAULT_PAGE_NUMBER,DEFAULT_PAGE_SIZE));

        assertNotNull(giftCertificates);
        assertEquals(4, giftCertificates.getTotalElements());
        assertEquals(3, giftCertificates.getNumberOfElements());
        assertEquals(1, giftCertificates.getNumber());
        assertEquals(2, giftCertificates.getTotalPages());
        assertTrue(giftCertificates.hasContent());
        assertTrue(giftCertificates.hasNext());
        assertFalse(giftCertificates.hasPrevious());

        assertEquals("GC-three", giftCertificates.getContent().get(0).getName());
        assertEquals("GC-five", giftCertificates.getContent().get(1).getName());
        assertEquals("GC-eight", giftCertificates.getContent().get(2).getName());

        Page<GiftCertificate> giftCertificatePage2 = giftCertificateRepository.
                findSortedGiftCertificatesAndTagsByTagNames(
                        tagName, 2, sort, giftCertificates.getNextPageable());

        assertNotNull(giftCertificatePage2);
        assertEquals(4, giftCertificatePage2.getTotalElements());
        assertEquals(1, giftCertificatePage2.getNumberOfElements());
        assertEquals(2, giftCertificatePage2.getNumber());
        assertEquals(2, giftCertificatePage2.getTotalPages());
        assertTrue(giftCertificatePage2.hasContent());
        assertFalse(giftCertificatePage2.hasNext());
        assertTrue(giftCertificatePage2.hasPrevious());

        assertEquals("GC-nine", giftCertificatePage2.getContent().get(0).getName());

        Page<GiftCertificate> giftCertificatePage3 = giftCertificateRepository.
                findSortedGiftCertificatesAndTagsByTagNames(
                        tagName, 2, sort, giftCertificatePage2.getPreviousPageable());

        assertNotNull(giftCertificatePage3);
        assertEquals(4, giftCertificatePage3.getTotalElements());
        assertEquals(3, giftCertificatePage3.getNumberOfElements());
        assertEquals(1, giftCertificatePage3.getNumber());
        assertEquals(2, giftCertificatePage3.getTotalPages());
        assertTrue(giftCertificatePage3.hasContent());
        assertTrue(giftCertificatePage3.hasNext());
        assertFalse(giftCertificatePage3.hasPrevious());

        assertEquals("GC-three", giftCertificatePage3.getContent().get(0).getName());
        assertEquals("GC-five", giftCertificatePage3.getContent().get(1).getName());
        assertEquals("GC-eight", giftCertificatePage3.getContent().get(2).getName());
    }

    @Test
    @DisplayName("Find gift certificates and tags by tag name and sorted with name ASC")
    public void shouldFindGiftCertificatesAndTagsByTagNameAndSortByName() {
        String sort = "gift_certificate.name ASC";
        String tagNames = "'lenovo', 'asus'";

        Page<GiftCertificate> giftCertificates = giftCertificateRepository.findSortedGiftCertificatesAndTagsByTagNames(
                tagNames,2, sort, new PageRequest(DEFAULT_PAGE_NUMBER, DEFAULT_PAGE_SIZE));

        assertNotNull(giftCertificates);
        assertEquals(4, giftCertificates.getTotalElements());
        assertEquals(3, giftCertificates.getNumberOfElements());
        assertEquals(1, giftCertificates.getNumber());
        assertEquals(2, giftCertificates.getTotalPages());
        assertTrue(giftCertificates.hasContent());
        assertTrue(giftCertificates.hasNext());
        assertFalse(giftCertificates.hasPrevious());

        assertEquals("GC-eight", giftCertificates.getContent().get(0).getName());
        assertEquals("GC-five", giftCertificates.getContent().get(1).getName());
        assertEquals("GC-nine", giftCertificates.getContent().get(2).getName());

        Page<GiftCertificate> giftCertificatePage2 = giftCertificateRepository.
                findSortedGiftCertificatesAndTagsByTagNames(
                        tagNames, 2, sort, giftCertificates.getNextPageable());

        assertNotNull(giftCertificatePage2);
        assertEquals(4, giftCertificatePage2.getTotalElements());
        assertEquals(1, giftCertificatePage2.getNumberOfElements());
        assertEquals(2, giftCertificatePage2.getNumber());
        assertEquals(2, giftCertificatePage2.getTotalPages());
        assertTrue(giftCertificatePage2.hasContent());
        assertFalse(giftCertificatePage2.hasNext());
        assertTrue(giftCertificatePage2.hasPrevious());

        assertEquals("GC-three", giftCertificatePage2.getContent().get(0).getName());

        Page<GiftCertificate> giftCertificatePage3 = giftCertificateRepository.
                findSortedGiftCertificatesAndTagsByTagNames(
                        tagNames, 2, sort, giftCertificatePage2.getPreviousPageable());

        assertNotNull(giftCertificatePage3);
        assertEquals(4, giftCertificatePage3.getTotalElements());
        assertEquals(3, giftCertificatePage3.getNumberOfElements());
        assertEquals(1, giftCertificatePage3.getNumber());
        assertEquals(2, giftCertificatePage3.getTotalPages());
        assertTrue(giftCertificatePage3.hasContent());
        assertTrue(giftCertificatePage3.hasNext());
        assertFalse(giftCertificatePage3.hasPrevious());

        assertEquals("GC-eight", giftCertificatePage3.getContent().get(0).getName());
        assertEquals("GC-five", giftCertificatePage3.getContent().get(1).getName());
        assertEquals("GC-nine", giftCertificatePage3.getContent().get(2).getName());
    }

    @Test
    @DisplayName("Find gift certificates and tags by tag name and sorted with date and name DESC")
    public void shouldFindGiftCertificatesAndTagsByTagNameAndSortByDateAndName() {
        String sort = "gift_certificate.create_date, gift_certificate.name DESC";
        String tagName = "'lenovo'";

        int tagNamesCount = 1;
        Page<GiftCertificate> giftCertificatePage1 = giftCertificateRepository.findSortedGiftCertificatesAndTagsByTagNames(
                tagName, tagNamesCount, sort, new PageRequest(DEFAULT_PAGE_NUMBER, DEFAULT_PAGE_SIZE));

        assertEquals(6, giftCertificatePage1.getTotalElements());
        assertEquals(3, giftCertificatePage1.getNumberOfElements());
        assertEquals(1, giftCertificatePage1.getNumber());
        assertEquals(2, giftCertificatePage1.getTotalPages());
        assertTrue(giftCertificatePage1.hasContent());
        assertTrue(giftCertificatePage1.hasNext());
        assertFalse(giftCertificatePage1.hasPrevious());

        assertEquals("GC-two", giftCertificatePage1.getContent().get(0).getName());
        assertEquals("GC-three", giftCertificatePage1.getContent().get(1).getName());
        assertEquals("GC-five", giftCertificatePage1.getContent().get(2).getName());

        Page<GiftCertificate> giftCertificatePage2 = giftCertificateRepository.findSortedGiftCertificatesAndTagsByTagNames(
                tagName, tagNamesCount, sort, giftCertificatePage1.getNextPageable());

        assertEquals(6, giftCertificatePage2.getTotalElements());
        assertEquals(3, giftCertificatePage2.getNumberOfElements());
        assertEquals(2, giftCertificatePage2.getNumber());
        assertEquals(2, giftCertificatePage2.getTotalPages());
        assertTrue(giftCertificatePage2.hasContent());
        assertFalse(giftCertificatePage2.hasNext());
        assertTrue(giftCertificatePage2.hasPrevious());

        assertEquals("GC-six", giftCertificatePage2.getContent().get(0).getName());
        assertEquals("GC-eight", giftCertificatePage2.getContent().get(1).getName());
        assertEquals("GC-nine", giftCertificatePage2.getContent().get(2).getName());
    }

    @Test
    @DisplayName("Throw exception when find sorted gift certificates and tags by tagName")
    public void shouldThrowExceptionWhenFindGiftCertificatesAndTagsByTagName() {
        String sort = "gift_certificate.create_date, gift_certificate.name DESC";
        String tagName = "'woody'";

        String errorMessage = environment.getProperty("giftCertificate.message.searchNotFound");
        GiftCertificateNotFoundException ex = Assertions.assertThrows(GiftCertificateNotFoundException.class, () -> {
            giftCertificateRepository.findSortedGiftCertificatesAndTagsByTagNames(
                    tagName, 1, sort, new PageRequest(DEFAULT_PAGE_NUMBER, DEFAULT_PAGE_SIZE));
        });

        assertEquals(errorMessage, ex.getMessage());
    }

    @Test
    @DisplayName("Find GiftCertificates and tags by tagName")
    public void shouldFindGiftCertificatesAndTagsByTagName() {
        String tagName = "'asus'";

        Page<GiftCertificate> giftCertificatePage1 = giftCertificateRepository.findGiftCertificatesAndTagsByTagNames(
                tagName, 1, new PageRequest(DEFAULT_PAGE_NUMBER, DEFAULT_PAGE_SIZE));
        assertEquals(5, giftCertificatePage1.getTotalElements());
        assertEquals(3, giftCertificatePage1.getNumberOfElements());
        assertEquals(1, giftCertificatePage1.getNumber());
        assertEquals(2, giftCertificatePage1.getTotalPages());
        assertTrue(giftCertificatePage1.hasContent());
        assertTrue(giftCertificatePage1.hasNext());
        assertFalse(giftCertificatePage1.hasPrevious());

        assertEquals("GC-three", giftCertificatePage1.getContent().get(0).getName());
        assertEquals("GC-five", giftCertificatePage1.getContent().get(1).getName());
        assertEquals("GC-seven", giftCertificatePage1.getContent().get(2).getName());

        Page<GiftCertificate> giftCertificatePage2 = giftCertificateRepository.findGiftCertificatesAndTagsByTagNames(
                tagName, 1, giftCertificatePage1.getNextPageable());
        assertEquals(5, giftCertificatePage2.getTotalElements());
        assertEquals(2, giftCertificatePage2.getNumberOfElements());
        assertEquals(2, giftCertificatePage2.getNumber());
        assertEquals(2, giftCertificatePage2.getTotalPages());
        assertTrue(giftCertificatePage2.hasContent());
        assertFalse(giftCertificatePage2.hasNext());
        assertTrue(giftCertificatePage2.hasPrevious());

        assertEquals("GC-eight", giftCertificatePage2.getContent().get(0).getName());
        assertEquals("GC-nine", giftCertificatePage2.getContent().get(1).getName());

        Page<GiftCertificate> giftCertificatePage3 = giftCertificateRepository.findGiftCertificatesAndTagsByTagNames(
                tagName, 1, giftCertificatePage2.getPreviousPageable());
        assertEquals(5, giftCertificatePage3.getTotalElements());
        assertEquals(3, giftCertificatePage3.getNumberOfElements());
        assertEquals(1, giftCertificatePage3.getNumber());
        assertEquals(2, giftCertificatePage3.getTotalPages());
        assertTrue(giftCertificatePage3.hasContent());
        assertTrue(giftCertificatePage3.hasNext());
        assertFalse(giftCertificatePage3.hasPrevious());

        assertEquals("GC-three", giftCertificatePage3.getContent().get(0).getName());
        assertEquals("GC-five", giftCertificatePage3.getContent().get(1).getName());
        assertEquals("GC-seven", giftCertificatePage3.getContent().get(2).getName());
    }

    @Test
    @DisplayName("Throw exception when find gift certificates and tags")
    public void shouldThrowExeptionWhenFindGiftCertificatesByTagName() {
        String tagName = "'woody'";
        String errorMessage = environment.getProperty("giftCertificate.message.searchNotFound");
        GiftCertificateNotFoundException ex = Assertions.assertThrows(GiftCertificateNotFoundException.class, () -> {
            giftCertificateRepository.findGiftCertificatesAndTagsByTagNames(
                    tagName, 1, new PageRequest(DEFAULT_PAGE_NUMBER, DEFAULT_PAGE_SIZE));
        });

        assertEquals(errorMessage, ex.getMessage());
    }

    @Test
    @DisplayName("Find sorted gift certificates by part of name sorted by date")
    public void shouldFindGiftCertificatesAndTagsByPartOfNameAndSortByDate() {
        String sort = "gift_certificate.create_date";
        String partOfName = "GC";

        Page<GiftCertificate> giftCertificatePage1 = giftCertificateRepository.findSortedGiftCertificatesAndTagsByPartOfName(
                partOfName, sort, new PageRequest(DEFAULT_PAGE_NUMBER, DEFAULT_PAGE_SIZE));
        assertEquals(8, giftCertificatePage1.getTotalElements());
        assertEquals(3, giftCertificatePage1.getNumberOfElements());
        assertEquals(1, giftCertificatePage1.getNumber());
        assertEquals(3, giftCertificatePage1.getTotalPages());
        assertTrue(giftCertificatePage1.hasContent());
        assertTrue(giftCertificatePage1.hasNext());
        assertFalse(giftCertificatePage1.hasPrevious());

        assertEquals("GC-one", giftCertificatePage1.getContent().get(0).getName());
        assertEquals("GC-two", giftCertificatePage1.getContent().get(1).getName());
        assertEquals("GC-three", giftCertificatePage1.getContent().get(2).getName());

        Page<GiftCertificate> giftCertificatePage2 = giftCertificateRepository.findSortedGiftCertificatesAndTagsByPartOfName(
                partOfName, sort, giftCertificatePage1.getNextPageable());
        assertEquals(8, giftCertificatePage2.getTotalElements());
        assertEquals(3, giftCertificatePage2.getNumberOfElements());
        assertEquals(2, giftCertificatePage2.getNumber());
        assertEquals(3, giftCertificatePage2.getTotalPages());
        assertTrue(giftCertificatePage2.hasContent());
        assertTrue(giftCertificatePage2.hasNext());
        assertTrue(giftCertificatePage2.hasPrevious());

        assertEquals("GC-five", giftCertificatePage2.getContent().get(0).getName());
        assertEquals("GC-six", giftCertificatePage2.getContent().get(1).getName());
        assertEquals("GC-seven", giftCertificatePage2.getContent().get(2).getName());

        Page<GiftCertificate> giftCertificatePage3 = giftCertificateRepository.findSortedGiftCertificatesAndTagsByPartOfName(
                partOfName, sort, giftCertificatePage2.getNextPageable());
        assertEquals(8, giftCertificatePage3.getTotalElements());
        assertEquals(2, giftCertificatePage3.getNumberOfElements());
        assertEquals(3, giftCertificatePage3.getNumber());
        assertEquals(3, giftCertificatePage3.getTotalPages());
        assertTrue(giftCertificatePage3.hasContent());
        assertFalse(giftCertificatePage3.hasNext());
        assertTrue(giftCertificatePage3.hasPrevious());

        assertEquals("GC-eight", giftCertificatePage3.getContent().get(0).getName());
        assertEquals("GC-nine", giftCertificatePage3.getContent().get(1).getName());

        Page<GiftCertificate> giftCertificatePage4 = giftCertificateRepository.findSortedGiftCertificatesAndTagsByPartOfName(
                partOfName, sort, giftCertificatePage3.getPreviousPageable());
        assertEquals(8, giftCertificatePage4.getTotalElements());
        assertEquals(3, giftCertificatePage4.getNumberOfElements());
        assertEquals(2, giftCertificatePage4.getNumber());
        assertEquals(3, giftCertificatePage4.getTotalPages());
        assertTrue(giftCertificatePage4.hasContent());
        assertTrue(giftCertificatePage4.hasNext());
        assertTrue(giftCertificatePage4.hasPrevious());

        assertEquals("GC-five", giftCertificatePage4.getContent().get(0).getName());
        assertEquals("GC-six", giftCertificatePage4.getContent().get(1).getName());
        assertEquals("GC-seven", giftCertificatePage4.getContent().get(2).getName());
    }

    @Test
    @DisplayName("Find gift certificates and tags by part of name and sorted with name ASC")
    public void shouldFindGiftCertificatesAndTagsByPartOfNameAndSortByName() {
        String sort = "gift_certificate.name ASC";
        String partOfName = "GC";

        Page<GiftCertificate> giftCertificatePage1 = giftCertificateRepository.
                findSortedGiftCertificatesAndTagsByPartOfName(partOfName, sort,
                        new PageRequest(DEFAULT_PAGE_NUMBER, DEFAULT_PAGE_SIZE));
        assertEquals(8, giftCertificatePage1.getTotalElements());
        assertEquals(3, giftCertificatePage1.getNumberOfElements());
        assertEquals(1, giftCertificatePage1.getNumber());
        assertEquals(3, giftCertificatePage1.getTotalPages());
        assertTrue(giftCertificatePage1.hasContent());
        assertTrue(giftCertificatePage1.hasNext());
        assertFalse(giftCertificatePage1.hasPrevious());

        assertEquals("GC-eight", giftCertificatePage1.getContent().get(0).getName());
        assertEquals("GC-five", giftCertificatePage1.getContent().get(1).getName());
        assertEquals("GC-nine", giftCertificatePage1.getContent().get(2).getName());

        Page<GiftCertificate> giftCertificatePage2 = giftCertificateRepository.findSortedGiftCertificatesAndTagsByPartOfName(
                partOfName, sort, giftCertificatePage1.getNextPageable());
        assertEquals(8, giftCertificatePage2.getTotalElements());
        assertEquals(3, giftCertificatePage2.getNumberOfElements());
        assertEquals(2, giftCertificatePage2.getNumber());
        assertEquals(3, giftCertificatePage2.getTotalPages());
        assertTrue(giftCertificatePage2.hasContent());
        assertTrue(giftCertificatePage2.hasNext());
        assertTrue(giftCertificatePage2.hasPrevious());

        assertEquals("GC-one", giftCertificatePage2.getContent().get(0).getName());
        assertEquals("GC-seven", giftCertificatePage2.getContent().get(1).getName());
        assertEquals("GC-six", giftCertificatePage2.getContent().get(2).getName());

        Page<GiftCertificate> giftCertificatePage3 = giftCertificateRepository.findSortedGiftCertificatesAndTagsByPartOfName(
                partOfName, sort, giftCertificatePage2.getNextPageable());
        assertEquals(8, giftCertificatePage3.getTotalElements());
        assertEquals(2, giftCertificatePage3.getNumberOfElements());
        assertEquals(3, giftCertificatePage3.getNumber());
        assertEquals(3, giftCertificatePage3.getTotalPages());
        assertTrue(giftCertificatePage3.hasContent());
        assertFalse(giftCertificatePage3.hasNext());
        assertTrue(giftCertificatePage3.hasPrevious());

        assertEquals("GC-three", giftCertificatePage3.getContent().get(0).getName());
        assertEquals("GC-two", giftCertificatePage3.getContent().get(1).getName());

    }

    @Test
    @DisplayName("Throw exception when find sorted gift certificates and tags by part of name")
    public void shouldThrowExceptionWhenFindGiftCertificatesAndTagsByPartOfName() {
        String sort = "gift_certificate.create_date, gift_certificate.name DESC";
        String partOfName = "moon";

        String errorMessage = environment.getProperty("giftCertificate.message.searchNotFound");
        GiftCertificateNotFoundException ex = Assertions.assertThrows(GiftCertificateNotFoundException.class, () -> {
            giftCertificateRepository.findSortedGiftCertificatesAndTagsByPartOfName(
                    partOfName, sort, new PageRequest(DEFAULT_PAGE_NUMBER, DEFAULT_PAGE_SIZE));
        });

        assertEquals(errorMessage, ex.getMessage());
    }

    @Test
    @DisplayName("Find GiftCertificates and tags by part of name")
    public void shouldFindGiftCertificatesAndTagsByPartOfName() {
        String partOfName = "-";

        Page<GiftCertificate> giftCertificatePage1 = giftCertificateRepository.findGiftCertificatesAndTagsByPartOfName(
                        partOfName, new PageRequest(DEFAULT_PAGE_NUMBER, DEFAULT_PAGE_SIZE));
        assertEquals(8, giftCertificatePage1.getTotalElements());
        assertEquals(3, giftCertificatePage1.getNumberOfElements());
        assertEquals(1, giftCertificatePage1.getNumber());
        assertEquals(3, giftCertificatePage1.getTotalPages());
        assertTrue(giftCertificatePage1.hasContent());
        assertTrue(giftCertificatePage1.hasNext());
        assertFalse(giftCertificatePage1.hasPrevious());

        Page<GiftCertificate> giftCertificatePage2 = giftCertificateRepository.findGiftCertificatesAndTagsByPartOfName(
                partOfName, giftCertificatePage1.getNextPageable());
        assertEquals(8, giftCertificatePage2.getTotalElements());
        assertEquals(3, giftCertificatePage2.getNumberOfElements());
        assertEquals(2, giftCertificatePage2.getNumber());
        assertEquals(3, giftCertificatePage2.getTotalPages());
        assertTrue(giftCertificatePage2.hasContent());
        assertTrue(giftCertificatePage2.hasNext());
        assertTrue(giftCertificatePage2.hasPrevious());

        Page<GiftCertificate> giftCertificatePage3 = giftCertificateRepository.findGiftCertificatesAndTagsByPartOfName(
                partOfName, giftCertificatePage2.getNextPageable());
        assertEquals(8, giftCertificatePage3.getTotalElements());
        assertEquals(2, giftCertificatePage3.getNumberOfElements());
        assertEquals(3, giftCertificatePage3.getNumber());
        assertEquals(3, giftCertificatePage3.getTotalPages());
        assertTrue(giftCertificatePage3.hasContent());
        assertFalse(giftCertificatePage3.hasNext());
        assertTrue(giftCertificatePage3.hasPrevious());

    }

    @Test
    @DisplayName("Throw exception when find gift certificates and tags by part of name")
    public void shouldThrowExceptionWhenFindGiftCertificatesByPartOfName() {
        String partOfName = "&?";
        String errorMessage = environment.getProperty("giftCertificate.message.searchNotFound");
        GiftCertificateNotFoundException ex = Assertions.assertThrows(GiftCertificateNotFoundException.class, () -> {
            giftCertificateRepository.findGiftCertificatesAndTagsByPartOfName(
                    partOfName, new PageRequest(DEFAULT_PAGE_NUMBER, DEFAULT_PAGE_SIZE));
        });

        assertEquals(errorMessage, ex.getMessage());
    }

    @Test
    @DisplayName("Find sorted gift certificates by part of desc sorted by date")
    public void shouldFindGiftCertificatesAndTagsByPartOfDescAndSortByDate() {
        String sort = "gift_certificate.create_date";
        String partOfDesc = "with tag2 and tag3";

        Page<GiftCertificate> giftCertificatePage1 = giftCertificateRepository.
                findSortedGiftCertificatesAndTagsByPartOfDesc(partOfDesc, sort,
                        new PageRequest(DEFAULT_PAGE_NUMBER, DEFAULT_PAGE_SIZE));
        assertEquals(3, giftCertificatePage1.getTotalElements());
        assertEquals(3, giftCertificatePage1.getNumberOfElements());
        assertEquals(1, giftCertificatePage1.getNumber());
        assertEquals(1, giftCertificatePage1.getTotalPages());
        assertTrue(giftCertificatePage1.hasContent());
        assertFalse(giftCertificatePage1.hasNext());
        assertFalse(giftCertificatePage1.hasPrevious());
    }

    @Test
    @DisplayName("Find gift certificates and tags by part of desc and sorted with name ASC")
    public void shouldFindGiftCertificatesAndTagsByPartOfDescAndSortByName() {
        String sort = "gift_certificate.name ASC";
        String partOfDesc = "with tag2 and tag3";

        Page<GiftCertificate> giftCertificatePage = giftCertificateRepository.
                findSortedGiftCertificatesAndTagsByPartOfDesc(partOfDesc, sort,
                        new PageRequest(DEFAULT_PAGE_NUMBER, DEFAULT_PAGE_SIZE));

        assertEquals("GC-eight", giftCertificatePage.getContent().get(0).getName());
        assertEquals("GC-five", giftCertificatePage.getContent().get(1).getName());
        assertEquals("GC-nine", giftCertificatePage.getContent().get(2).getName());
    }

    @Test
    @DisplayName("Throw exception when find sorted gift certificates and tags by part of desc")
    public void shouldThrowExceptionWhenFindGiftCertificatesAndTagsByPartOfDesc() {
        String sort = "gift_certificate.create_date, gift_certificate.name DESC";
        String partOfDesc = "electric adapter";

        String errorMessage = environment.getProperty("giftCertificate.message.searchNotFound");
        GiftCertificateNotFoundException ex = Assertions.assertThrows(GiftCertificateNotFoundException.class, () -> {
            giftCertificateRepository.findSortedGiftCertificatesAndTagsByPartOfDesc(partOfDesc, sort,
                    new PageRequest(DEFAULT_PAGE_NUMBER, DEFAULT_PAGE_SIZE));
        });

        assertEquals(errorMessage, ex.getMessage());
    }

    @Test
    @DisplayName("Find GiftCertificates and tags by part of desc")
    public void shouldFindGiftCertificatesAndTagsByPartOfDesc() {
        String partOfDesc = "with tag2";

        Page<GiftCertificate> giftCertificatePage1 = giftCertificateRepository.findGiftCertificatesAndTagsByPartOfDesc(
                partOfDesc, new PageRequest(DEFAULT_PAGE_NUMBER, DEFAULT_PAGE_SIZE));
        assertEquals(4, giftCertificatePage1.getTotalElements());
        assertEquals(3, giftCertificatePage1.getNumberOfElements());
        assertEquals(1, giftCertificatePage1.getNumber());
        assertEquals(2, giftCertificatePage1.getTotalPages());
        assertTrue(giftCertificatePage1.hasContent());
        assertTrue(giftCertificatePage1.hasNext());
        assertFalse(giftCertificatePage1.hasPrevious());

        assertEquals("GC-five", giftCertificatePage1.getContent().get(0).getName());
        assertEquals("GC-six", giftCertificatePage1.getContent().get(1).getName());
        assertEquals("GC-eight", giftCertificatePage1.getContent().get(2).getName());
    }

    @Test
    @DisplayName("Throw exception when find gift certificates and tags by part of desc")
    public void shouldThrowExceptionWhenFindGiftCertificatesByPartOfDesc() {
        String partOfDesc = "trump and clinton";
        String errorMessage = environment.getProperty("giftCertificate.message.searchNotFound");
        GiftCertificateNotFoundException ex = Assertions.assertThrows(GiftCertificateNotFoundException.class, () -> {
            giftCertificateRepository.findGiftCertificatesAndTagsByPartOfDesc(
                    partOfDesc, new PageRequest(DEFAULT_PAGE_NUMBER, DEFAULT_PAGE_SIZE));
        });

        assertEquals(errorMessage, ex.getMessage());
    }

    @Test
    @DisplayName("Find gift certificates by tag id")
    public void shouldFindGiftCertificatesByTagId() {
        long tagId = 3L;
        Page<GiftCertificate> giftCertificatePage1 = giftCertificateRepository.findGiftCertificatesByTagId(
                        tagId, new PageRequest(DEFAULT_PAGE_NUMBER, DEFAULT_PAGE_SIZE));
        assertEquals(6, giftCertificatePage1.getTotalElements());
        assertEquals(3, giftCertificatePage1.getNumberOfElements());
        assertEquals(1, giftCertificatePage1.getNumber());
        assertEquals(2, giftCertificatePage1.getTotalPages());
        assertTrue(giftCertificatePage1.hasContent());
        assertTrue(giftCertificatePage1.hasNext());
        assertFalse(giftCertificatePage1.hasPrevious());

        Page<GiftCertificate> giftCertificatePage2 = giftCertificateRepository.findGiftCertificatesByTagId(
                tagId, giftCertificatePage1.getNextPageable());
        assertEquals(6, giftCertificatePage2.getTotalElements());
        assertEquals(3, giftCertificatePage2.getNumberOfElements());
        assertEquals(2, giftCertificatePage2.getNumber());
        assertEquals(2, giftCertificatePage2.getTotalPages());
        assertTrue(giftCertificatePage2.hasContent());
        assertFalse(giftCertificatePage2.hasNext());
        assertTrue(giftCertificatePage2.hasPrevious());

        Page<GiftCertificate> giftCertificatePage3 = giftCertificateRepository.findGiftCertificatesByTagId(
                tagId, giftCertificatePage2.getPreviousPageable());
        assertEquals(6, giftCertificatePage3.getTotalElements());
        assertEquals(3, giftCertificatePage3.getNumberOfElements());
        assertEquals(1, giftCertificatePage3.getNumber());
        assertEquals(2, giftCertificatePage3.getTotalPages());
        assertTrue(giftCertificatePage3.hasContent());
        assertTrue(giftCertificatePage3.hasNext());
        assertFalse(giftCertificatePage3.hasPrevious());
    }

    @Test
    @DisplayName("Throw exception when find gift certificates by id")
    public void shouldThrowExceptionWhenFindGiftCertificateById() {
        long tagId = 7L;
        String errorMessage = String.format(environment.getProperty("giftCertificate.message.notFoundByTagId"), tagId);
        GiftCertificateNotFoundException ex = assertThrows(GiftCertificateNotFoundException.class, ()-> {
            giftCertificateRepository.findGiftCertificatesByTagId(
                    tagId, new PageRequest(DEFAULT_PAGE_NUMBER, DEFAULT_PAGE_SIZE));
        });
        assertEquals(errorMessage, ex.getMessage());
    }

    @Test
    @DisplayName("Throw exception when save gift certificate and tag")
    public void shouldThrowExceptionWhenSaveGiftCertificateAndTag() {
        long giftCertificateId = 3L;
        long tagId = 3L;
        GiftCertificate giftCertificate = giftCertificateRepository.findGiftCertificateById(giftCertificateId);
        Tag tag = tagRepository.findTagById(tagId);

        assertFalse(giftCertificateRepository.saveGiftCertificateAndTag(giftCertificate, tag));
    }
}