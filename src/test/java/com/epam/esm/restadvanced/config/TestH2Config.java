package com.epam.esm.restadvanced.config;

import com.epam.esm.restadvanced.repository.*;
import com.epam.esm.restadvanced.repository.entity.GiftCertificate;
import com.epam.esm.restadvanced.repository.entity.Order;
import com.epam.esm.restadvanced.repository.entity.Tag;
import com.epam.esm.restadvanced.repository.entity.User;
import com.epam.esm.restadvanced.repository.impl.*;
import com.epam.esm.restadvanced.repository.mapper.GiftCertificateFieldMapper;
import com.epam.esm.restadvanced.repository.mapper.GiftCertificateFieldMapperImpl;
import org.apache.commons.dbcp2.BasicDataSource;
import org.springframework.context.annotation.*;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
import org.springframework.transaction.PlatformTransactionManager;

import javax.sql.DataSource;

@Configuration
@PropertySources({
        @PropertySource(name="sqlStatements", value = "classpath:sql-statements.properties"),
        @PropertySource(name="stringValues", value = "classpath:string-values.properties"),
        @PropertySource(name="db_test.properties", value = "classpath:application-test.properties")
})
public class TestH2Config {

    @Bean
    @Profile("test-h2")
    public DataSource getDevDataSource() {
        return new EmbeddedDatabaseBuilder()
            .setType(EmbeddedDatabaseType.H2)
            .addScript("classpath:schema.sql")
            .build();
    }

    @Bean(name = "prodTxManager")
    @Profile("test-h2")
    public PlatformTransactionManager txManager(DataSource dataSource) {
        return new DataSourceTransactionManager(dataSource);
    }


    @Bean
    @Profile("test-h2")
    public JdbcTemplate getJdbcTemplate(DataSource dataSource) {
        return new JdbcTemplate(dataSource);
    }

    @Bean
    @Profile("test-h2")
    public GiftCertificateFieldMapper getGiftCertificateFieldMapper() {
        return new GiftCertificateFieldMapperImpl();
    }

    @Bean
    @Profile("test-h2")
    public TagRepository getTagRepository(Environment environment, JdbcTemplate jdbcTemplate) {
        return new TagRepositoryImpl(jdbcTemplate, environment, getTagPageRepository(jdbcTemplate));
    }

    @Bean
    @Profile("test-h2")
    public PageRepository<GiftCertificate> getGiftCertificatePageRepository(JdbcTemplate jdbcTemplate) {
        return new PageRepositoryImpl<>(jdbcTemplate);
    }

    @Bean
    @Profile("test-h2")
    public PageRepository<Tag> getTagPageRepository(JdbcTemplate jdbcTemplate) {
        return new PageRepositoryImpl<>(jdbcTemplate);
    }

    @Bean
    @Profile("test-h2")
    public PageRepository<Order> getOrderPageRepository(JdbcTemplate jdbcTemplate) {
        return new PageRepositoryImpl<>(jdbcTemplate);
    }

    @Bean
    @Profile("test-h2")
    public UserRepository getUserRepository(Environment environment, JdbcTemplate jdbcTemplate) {
        return new UserRepositoryImpl(jdbcTemplate, environment, getUserPageRepository(jdbcTemplate));
    }

    @Bean
    @Profile("test-h2")
    public PageRepository<User> getUserPageRepository(JdbcTemplate jdbcTemplate) {
        return new PageRepositoryImpl<>(jdbcTemplate);
    }

    @Bean
    @Profile("test-h2")
    public OrderRepository getOrderRepository(Environment environment, JdbcTemplate jdbcTemplate,
                                              GiftCertificateRepository giftCertificateRepository) {
        return new OrderRepositoryImpl(jdbcTemplate, environment, getUserRepository(environment, jdbcTemplate),
                giftCertificateRepository, getOrderPageRepository(jdbcTemplate));
    }
    @Bean
    @Profile("test-h2")
    public GiftCertificateRepository getGiftCertRepository(Environment environment, JdbcTemplate jdbcTemplate,
                                                           GiftCertificateFieldMapper giftCertificateFieldMapper) {
        return new GiftCertificateRepositoryImpl(environment, jdbcTemplate, getTagRepository(environment, jdbcTemplate),
                getGiftCertificatePageRepository(jdbcTemplate), giftCertificateFieldMapper);
    }
}
