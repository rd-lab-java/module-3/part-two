package com.epam.esm.restadvanced.config;

import com.epam.esm.restadvanced.repository.GiftCertificateRepository;
import com.epam.esm.restadvanced.repository.PageRepository;
import com.epam.esm.restadvanced.repository.TagRepository;
import com.epam.esm.restadvanced.repository.entity.GiftCertificate;
import com.epam.esm.restadvanced.repository.entity.Tag;
import com.epam.esm.restadvanced.repository.impl.GiftCertificateRepositoryImpl;
import com.epam.esm.restadvanced.repository.impl.PageRepositoryImpl;
import com.epam.esm.restadvanced.repository.impl.TagRepositoryImpl;
import com.epam.esm.restadvanced.repository.mapper.GiftCertificateFieldMapper;
import com.epam.esm.restadvanced.repository.mapper.GiftCertificateFieldMapperImpl;
import org.apache.commons.dbcp2.BasicDataSource;
import org.springframework.context.annotation.*;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;

import javax.sql.DataSource;

@Configuration
@PropertySources({
        @PropertySource(name="sqlStatements", value = "classpath:sql-statements.properties"),
        @PropertySource(name="stringValues", value = "classpath:string-values.properties"),
        @PropertySource(name="db_test.properties", value = "classpath:application-dev.properties")
})
public class TestMysqlConfig {
    @Bean
    @Profile("test-mysql")
    public DataSource getDataSource(Environment environment) {
        BasicDataSource basicDataSource = new BasicDataSource();
        basicDataSource.setDriverClassName(environment.getProperty("spring.datasource.driverClassName"));
        basicDataSource.setUrl(environment.getProperty("spring.datasource.url"));
        basicDataSource.setMaxIdle(environment.getProperty("spring.datasource.maxIdle", Integer.class));
        basicDataSource.setMaxWaitMillis(environment.getProperty("spring.datasource.maxWait", Long.class));
        basicDataSource.setMaxTotal(environment.getProperty("spring.datasource.maxActive", Integer.class));
        basicDataSource.setUsername(environment.getProperty("spring.datasource.user"));
        basicDataSource.setPassword(environment.getProperty("spring.datasource.password"));
        return basicDataSource;
    }

    @Bean(name = "prodTxManager")
    @Profile("test-mysql")
    public PlatformTransactionManager txManager(DataSource dataSource) {
        return new DataSourceTransactionManager(dataSource);
    }


    @Bean
    @Profile("test-mysql")
    public JdbcTemplate getJdbcTemplate(DataSource dataSource) {
        return new JdbcTemplate(dataSource);
    }

    @Bean
    @Profile("test-mysql")
    public GiftCertificateFieldMapper getGiftCertificateFieldMapper() {
        return new GiftCertificateFieldMapperImpl();
    }

    @Bean
    @Profile("test-mysql")
    public TagRepository getTagRepository(Environment environment, JdbcTemplate jdbcTemplate) {
        return new TagRepositoryImpl(jdbcTemplate, environment, getTagPageRepository(jdbcTemplate));
    }

    @Bean
    @Profile("test-mysql")
    public PageRepository<GiftCertificate> getGiftCertificatePageRepository(JdbcTemplate jdbcTemplate) {
        return new PageRepositoryImpl<>(jdbcTemplate);
    }

    @Bean
    @Profile("test-mysql")
    public PageRepository<Tag> getTagPageRepository(JdbcTemplate jdbcTemplate) {
        return new PageRepositoryImpl<>(jdbcTemplate);
    }

    @Bean
    @Profile("test-mysql")
    public GiftCertificateRepository getGiftCertRepository(Environment environment, JdbcTemplate jdbcTemplate,
                                                           GiftCertificateFieldMapper giftCertificateFieldMapper) {
        return new GiftCertificateRepositoryImpl(environment, jdbcTemplate, getTagRepository(environment, jdbcTemplate),
                getGiftCertificatePageRepository(jdbcTemplate), giftCertificateFieldMapper);
    }
}
